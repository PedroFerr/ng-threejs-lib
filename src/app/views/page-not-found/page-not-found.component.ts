import { Component, OnInit, AfterViewInit, Renderer2 } from '@angular/core';

@Component({
    selector: 'app-page-not-found',
    templateUrl: './page-not-found.component.html',
    styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit, AfterViewInit {

    menuTriggerButton: HTMLElement;

    constructor(private renderer: Renderer2) { }

    ngOnInit() {
        this.menuTriggerButton = document.querySelector('.menu-trigger');
    }

    ngAfterViewInit() {
        // As we have a blak bacground, on this page, maybe the button of the side Menu trigger, stay better in $secondary:
        if (this.menuTriggerButton) {
            this.renderer.setStyle(this.menuTriggerButton, 'color', 'var(--secondary)');
        }
        // REMEMBER we need to set it back to $primary color on leaving => use Angular's Router 'CanDeactivate' over a Guard condition.
    }

}
