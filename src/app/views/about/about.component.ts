import { Component, OnInit, AfterViewInit, Renderer2 } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit, AfterViewInit {

    menuTriggerButton: HTMLElement;

    constructor(
        private browserTitleService: Title,
        private renderer: Renderer2
    ) { }

    ngOnInit() {
        this.browserTitleService.setTitle('NG3D About');
        this.menuTriggerButton = document.querySelector('.menu-trigger');
    }

    ngAfterViewInit() {
        // As we have a black background, on this page, maybe the button of the side Menu trigger, stay better in $secondary:
        if (this.menuTriggerButton) {
            this.renderer.setStyle(this.menuTriggerButton, 'color', 'var(--secondary)');
        }
        // REMEMBER we need to set it back to $primary color on leaving => use Angular's Router 'CanDeactivate' over a Guard condition.
    }

}
