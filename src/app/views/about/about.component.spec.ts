/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AboutComponent } from './about.component';

describe('AboutComponent', () => {
    let component: AboutComponent;
    let fixture: ComponentFixture<AboutComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule
            ],
            declarations: [AboutComponent]
        })
        .compileComponents();
    }));

    it('should create About component', () => {
        fixture = TestBed.createComponent(AboutComponent);
        component = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });
});
