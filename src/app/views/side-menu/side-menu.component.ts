import { Component, OnInit, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'app-side-menu',
    templateUrl: './side-menu.component.html',
    styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit, AfterViewInit, OnDestroy {

    componentDOM: HTMLElement;
    collectionOfLinksToDemos: NodeListOf<Element>;
    closeMenuBtn: HTMLElement;

    constructor(
        private hostElement: ElementRef
    ) { }

    ngOnInit() {
        this.componentDOM = this.hostElement.nativeElement;
    }

    ngAfterViewInit() {
        this.collectionOfLinksToDemos = this.componentDOM.querySelectorAll('li');

        this.collectionOfLinksToDemos.forEach((link: HTMLElement) => link.addEventListener('click', this.onLinkClick));
    }

    ngOnDestroy() {
        this.collectionOfLinksToDemos.forEach((link: HTMLElement) => link.removeEventListener('click', this.onLinkClick));
    }

    private onLinkClick(evt: MouseEvent) {
        const closeMenuBtn: HTMLElement = document.querySelector('.close-menu');

        // Interesting to see (uncomment below console.logs)
        // the time the browser takes to "react", most probably beacuse it is EXTREMELY busy with the 3D Rendering!
        //
        // TODO:
        // On each 3D Rendering/Animation, make sure you run it OUTSIDE the angular zones
        //     - I think it's triggering heavy 'changeDetection' cycles.
        // Or, on each 3D Component, control the 'changeDetection's to be OnPush - only re-render if an @Input() as change it's value...
        // @Component({ ... , changeDetection: ChangeDetectionStrategy.OnPush })
        // Or my machine is awful slooooow... ;-)
        //
        // console.warn(evt.target, 'clicked!');
        // console.log(closeMenuBtn);

        evt.stopImmediatePropagation();
        closeMenuBtn.click();
    }

}
