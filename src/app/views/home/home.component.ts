import { Component, OnInit, AfterViewInit, ElementRef, OnDestroy, Renderer2 } from '@angular/core';
import { Title } from '@angular/platform-browser';

import * as THREE from 'three';
// Some used JS Library of animations - check it at https://github.com/juliangarnier/anime/
import anime from 'animejs/lib/anime.es.js';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {

    title = 'NG3D Home';

    // This WebGL cool example, for the home page of this Application (DEMO of the library of 3D Components)
    // was taken from https://codepen.io/Staak/pen/XowdeE
    //
    //  -----------------------------
    //
    // This is a perfect example, for our Home page, since it's exactly what we'll keep doing on the library:
    // provide an "environment" to render 3D Objects.
    //
    // This environment is precisely given to us through ThreeJS library, that is the one operating the browser's WebGL.
    // As so, we, in fact, don't need to worry about "nothing" concerning WebGL
    //     - just use properly, and to the far of its extent, the ThreeJS library, according to your App's/Project's needs!
    //
    // This environemnt is basically formed of a Camera(s), filming a Scene, with some sort of Light(s),
    // that has to Render "an" Objects (3D Component) that we will be developing and adding up to the 3D library of Components.
    //
    // Each Object, then, by its turns, will have some Geometry, Materials, Textures, Colors, Shapes, etc., etc.
    // and can also have a 3D axys Movement, associated with it, that we code with an Animation of the Scene/Object!
    //
    // That's exactly what we do on our Library of 3D Components.
    //
    //  -----------------------------
    //
    // Some more ThreeJS most cool examples can be borrowed from:
    // https://uicookies.com/threejs-examples/
    // https://www.jotform.com/blog/20-exceptional-three-js-experiments-98740/

    randFrom: any;
    easing: any;

    componentDOM: HTMLElement;

    randFromText: HTMLElement;
    randEasingText: HTMLElement;

    // ThreeJS Components/Methods:
    camera: any;
    orbitControls: any;
    scene: any;
    renderer: any;
    // And about our paralelipedic elements (they are, in fact, stretched Cubes)
    geometry: any;
    // which we show in a 3D Grid:
    nRows: number;
    nCols: number;
    staggerArray: Array<THREE.Vector3>;
    // and will animate with random 'easing' patterns:
    anime: any;

    // tslint:disable: constiable-name
    // tslint:disable: prefer-for-of
    // tslint:disable: no-bitwise
    // tslint:disable: one-variable-per-declaration
    // tslint:disable: max-line-length

    constructor(
        private browserTitleService: Title,
        private hostElement: ElementRef,
        private ngRrenderer: Renderer2
    ) { }

    ngOnInit() {

        this.browserTitleService.setTitle( this.title );

        this.randFrom = ['first', 'last', 'center'];

        this.easing = [
            'linear',
            'easeInOutQuad',
            'easeInOutCubic',
            'easeInOutQuart',
            'easeInOutQuint',
            'easeInOutSine',
            'easeInOutExpo',
            'easeInOutCirc',
            'easeInOutBack',
            'cubicBezier(.5, .05, .1, .3)',
            'spring(1, 80, 10, 0)',
            'steps(10)'
        ];

        this.componentDOM = this.hostElement.nativeElement;
    }

    ngOnDestroy() {
        window.removeEventListener('resize', (e) => this.onWindowResize(e));
    }

    ngAfterViewInit() {

        this.randFromText = this.componentDOM.querySelector('#randFrom');
        this.randEasingText = this.componentDOM.querySelector('#randEasing');

        this.init();
    }

    private init() {
        this.camera = new THREE.PerspectiveCamera(25, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.camera.position.x = -45;
        this.camera.position.y = 30;
        this.camera.position.z = -45;

        // this.orbitControls = new THREE.OrbitControls(this.camera);    // See it in the end, whenever an 'update()' is requested.

        // Next will define the point to where camera is looking at => can "center" to different positions, the center of the sticks animation:
        // -----------------------------------------------
        // this.camera.lookAt(new THREE.Vector3(5, -5, 5));
        // this.camera.lookAt(new THREE.Vector3(0, 0, 0));
        // this.camera.lookAt(new THREE.Vector3(0, -3, 0));
        this.camera.lookAt(new THREE.Vector3(5, -6, 5));
        // -----------------------------------------------

        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color('#ffffff'); // <= SCSS '$secondary'

        // this.resizeListener = (e) => this.onWindowResize(e);
        // window.addEventListener('resize', this.resizeListener, false);
        // this.onWindowResize();
        window.addEventListener('resize', (e) => this.onWindowResize(e));

        this.createBoxes();

        this.renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true }); // <= 'alpha: true' allows you to customize bg color @ this.scene.background - 'black', by default

        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth, window.innerHeight);


        // document.body.appendChild(this.renderer.domElement);
        this.ngRrenderer.appendChild(this.componentDOM, this.renderer.domElement);

        // Now we have our HTML5 <canvas /> into this component's DOM, where WebGL will do our home page cool rendering, lets place it:
        const canvas: HTMLElement = this.componentDOM.querySelector('canvas');
        this.ngRrenderer.setStyle(canvas, 'position', 'absolute');
        this.ngRrenderer.setStyle(canvas, 'top', '0');
        this.ngRrenderer.setStyle(canvas, 'left', '0');
        this.ngRrenderer.setStyle(canvas, 'bottom', '0');
        this.ngRrenderer.setStyle(canvas, 'right', '0');
        this.ngRrenderer.setStyle(canvas, 'padding', '1rem');

        // Et voilá! Prepare animation parameters and animate it:
        this.beginAnimationLoop();
        this.animate();
    }

    private createBoxes() {
        const vertexShader = `
            varying vec2 vUv;
            void main()	{
                vUv = uv;
                gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);
            }
        `;

        const fragmentShader = `
            #extension GL_OES_standard_derivatives : enable

            varying vec2 vUv;
            uniform float thickness;

            float edgeFactor(vec2 p){
                vec2 grid = abs(fract(p - 0.5) - 0.5) / fwidth(p) / thickness;
                return min(grid.x, grid.y);
            }

            void main() {

                float a = edgeFactor(vUv);

                vec3 c = mix(vec3(1), vec3(0), a);

                gl_FragColor = vec4(c, 1.0);
            }
        `;

        // So:
        this.geometry = new THREE.BoxBufferGeometry(1, 10, 1);
        // and with:
        const material = new THREE.ShaderMaterial({
            uniforms: {
                thickness: { value: 1.5 }
            },
            vertexShader,
            fragmentShader
        });
        // We have our 3D paralelipedic element:
        const cube = new THREE.Mesh(this.geometry, material);

        // Now let's do a 3D grid of them!
        const offset = 1.25;
        this.nRows = 25;
        this.nCols = 25;
        this.staggerArray = [];

        for (let column = 0; column < this.nCols; column++) {
            for (let row = 0; row < this.nRows; row++) {
                const obj = cube.clone();

                obj.position.x = row * offset - (this.nRows * 0.5 + this.geometry.parameters.width * 0.5);
                obj.position.y = -(this.geometry.parameters.height * 0.5);
                obj.position.z = column * offset - (this.nCols * 0.5 + this.geometry.parameters.width * 0.5);

                this.staggerArray.push(obj.position);
                // There we are - our grid of paralalipedic elements:
                this.scene.add(obj);
            }
        }
    }

    beginAnimationLoop() {
        // Random from array, with a random animation pattern (using 'easing'):
        const randFrom = this.randFrom[ Math.floor(Math.random() * this.randFrom.length) ];
        const easingString = this.easing[ Math.floor(Math.random() * this.easing.length)];

        this.randFromText.textContent = randFrom;
        this.randEasingText.textContent = easingString;

        anime({
            targets: this.staggerArray,
            y: [
                { value: this.geometry.parameters.height * 0.25, duration: 500 },
                { value: -(this.geometry.parameters.height * 0.25), duration: 2000 }
            ],
            delay: anime.stagger(200, {
                grid: [this.nRows, this.nCols],
                from: randFrom
            }),
            easing: easingString,
            complete: (anim) => this.beginAnimationLoop()
        });
    }

    animate() {
        requestAnimationFrame(() => this.animate());
        this.update();
        this.render();
    }

    update() {
        if (this.orbitControls) {
            this.orbitControls.update();
        }
    }

    render() {
        this.renderer.render(this.scene, this.camera);
    }

    onWindowResize(evt?: any) {
        // Shouldn't we...?
        // evt.stopImmediatePropagation();
        const width = window.innerWidth;
        const height = window.innerHeight;

        // scene & camera update
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(width, height);
    }


}
