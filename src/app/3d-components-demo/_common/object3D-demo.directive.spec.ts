/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { Object3DDemoDirective } from './object3D-demo.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from 'projects/ng-threejs-lib/src/lib/services/app-demo/logger.service';

import * as ComponentMenuType from '../../3d-component-menu/component-menu.interfaces';

describe('Directive: Object3DDemo', () => {
    // tslint:disable: prefer-const
    let ThreeJSrendererService: RendererService;
    let ThreeJSloggerService: LoggerService;

    beforeEach( async(() => {
            TestBed.configureTestingModule({
                // declarations: [ThreeJSloggerService, ThreeJSloggerService],
                // providers: [ThreeJSloggerService, ThreeJSloggerService],
                // providers: [
                //     { provide: ThreeJSrendererService, useClass: LoggerService },
                //     { provide: ThreeJSloggerService, useClass: RendererService }
                // ],
            }).compileComponents();
        })
    );

    it('should create an instance', () => {
        const directive = new Object3DDemoDirective(ThreeJSrendererService, ThreeJSloggerService);
        expect(directive).toBeTruthy();
    });
});
