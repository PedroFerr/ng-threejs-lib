import { Directive, OnDestroy } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';

import { LoggerService, RendererService, LoggEventData, SomeNgChanges, UserObj3D } from 'ng-threejs-lib';

import * as ComponentMenuType from '../../3d-component-menu/component-menu.interfaces';

/**
 * =============================================================================================
 * This is a common Angular Directive to ALL ThreeJS's 3D Objects DEMO components.
 *
 * Incorporates all COMMON:
 *     * loading of the 3D Stage params initial/reloading values
 *     * Logged Events listner and actions (i.e. loading wheel start/stop moments, for each DEMO, 'uuid' for each User new Obj 3D, etc.)
 *     * the Stage @Input / @Output values/affairs, spread on each DEMO's Template HTML
 * ==============================================================================================
 */
@Directive({
    selector: '[appMeshObject3DDemo]'
})
export class Object3DDemoDirective implements OnDestroy {

    // Our currently being rendered (extended Class) DEMO Stage's ThreeJS 3D Objects,
    // we want to manipulate on each of the 'settings' Tab's several inner Tabs:
    camera: ComponentMenuType.Camera;
    pointsOfLight = [] as Array<ComponentMenuType.PointOfLight>;
    actor3D: ComponentMenuType.Actor3D;
    // And the eventual User created 3D Object, added dynamically into the current DEMO's 3D Stage:
    createdObj3D: ComponentMenuType.New3DObject;

    // NgRxJS stream of added logg events, through LoggerService, all over the App.
    // It's the starting/stop moments of the loading wheel we are, here, interested in catching:
    loggEvent$: Observable<LoggEventData | string>;
    loggSubscription: Subscription;
    loadingWheelDirectiveCommand$ = new Subject<boolean>();

    // Let's synchronize the 3D Stage with the Angular DEMO mounting of components:
    start3Dmounting = false;

    // And we have to think about the 3D future... dynamically! ;-)
    userCreatedObj3D: UserObj3D; // ComponentMenuType.New3DObject;

    constructor(
        protected ThreeJSrendererService: RendererService,
        protected ThreeJSloggerService: LoggerService
    ) {
        this.loadingWheelDirectiveCommand$.next(false);
        this.startListening3DloggedEvents();
    }

    ngOnDestroy() {
        if (this.loggSubscription) {
            this.loggSubscription.unsubscribe();
        }

        this.ThreeJSrendererService.clear3Dstage();
        this.ThreeJSloggerService.clear3DloggedEvents();
    }

    /**
     * 'monitor-debug', as the most far nested component of this 'sphere-demo',
     * has reachd 'ngAfterViewInit()' life cycle hook.
     *
     * As so, we can assume all DEMO Angular components have mounted and are now stable
     * Meaning... we can give a direct order for the 3D Stge to start mounting its own Angular components
     * and, inside of each, the talkings with the ThreeJS API, that will instruct WebGL to force
     * the GPU rendering of 'some' 3D Stage on the screen.
     */
    onFinishLoadingOfAngular() {
        this.start3Dmounting = true;
    }

    // ----------------------------------------------------------------------------

    onAddingNewPointOfLight(userCustomPointOfLight: ComponentMenuType.PointOfLight) {
        this.userCreatedObj3D = { type: 'PointLight', operation: 'create', obj3DParams: Object.assign({}, userCustomPointOfLight) };
    }

    onRemovingPointOfLight(userCustomPointOfLightUuid: string) {
        this.userCreatedObj3D = { type: 'PointLight', operation: 'remove', obj3DParams: {uuid: userCustomPointOfLightUuid} };
    }

    // ----------------------------------------------------------------------------

    onSendingCameraAxisCoord(thisNewCoord: ComponentMenuType.Coordinate) {
        // console.warn(`Moving Camera to "${thisNewCoord.value}" @ ${thisNewCoord.axis.toUpperCase()} axis Pos.`);
        this.camera.position[thisNewCoord.axis] = Number(thisNewCoord.value);
    }

    onSendingCameraNewRotationCoord(thisNewRotationCoord: ComponentMenuType.Coordinate) {
        // console.warn(`Rotating Camera to "${thisNewRotationCoord.value}" @ ${thisNewRotationCoord.axis.toUpperCase()} axis Pos.`);
        this.camera.rotation[thisNewRotationCoord.axis] = Number(thisNewRotationCoord.value);
    }

    onSendingCameraMoveParam(thisParam: ComponentMenuType.MoveParam) {
        // console.warn(`Camera param "${thisParam.param}" should change to "${thisParam.value}"`);
        switch (thisParam.param) {
            case 'lookAtCenter':
                this.camera.movements.isLookingAtCenter = thisParam.value as boolean;
                break;

            case 'axisUp':
                this.camera.movements.axisUp = thisParam.value as ComponentMenuType.AxisType;
                break;

            default:
                console.error(`ERROR: ${thisParam.param} is NOT considered @ 'onSendingCameraMoveParam()' Method!`);
                break;
        }
    }

    onSendingCameraNewFOV(nFov: number) {
        // console.warn(`Changing Camera frustum vertical Field Of View to "${nFov}"`);
        // If you don't Number() it, will go as an string, since it's an HTML Input value. And... ThreeJS will reject it... silently!
        this.camera.frustum.fov = Number(nFov);
    }

    onSendingCameraNewNEAR(nNear: number) {
        // console.warn(`Changing Camera frustum NEAR plane to "${nNear}"`);
        this.camera.frustum.near = Number(nNear);
    }

    onSendingCameraNewFAR(nFar: number) {
        // console.warn(`Changing Camera frustum FAR plane to "${nFar}"`);
        this.camera.frustum.far = Number(nFar);
    }

    // ----------------------------------------------------------------------------

    onSendingPLNewTranslatePosCoord(thisNewCoord: ComponentMenuType.Coordinate) {
        // console.warn(
        //     `Moving Point of Light #${thisNewCoord.idx} to "${thisNewCoord.value}" @ ${thisNewCoord.axis.toUpperCase()} axis Pos.`
        // );
        if (!thisNewCoord.uuid) {
            this.pointsOfLight[thisNewCoord.idx].position[thisNewCoord.axis] = Number(thisNewCoord.value);
        } else {
            // This Point of Light was dynamically added by the User!
            // Update <ng3d-scene [newObj]="userCreatedObj3D"> and not any of the marked up <ng3d-point-light /> at DEMO's Template HTML
            this.userCreatedObj3D = { type: 'PointLight', operation: 'update',
                obj3DParams: {
                    uuid: thisNewCoord.uuid,
                    position: {
                        translateX: thisNewCoord.axis === 'x' ? thisNewCoord.value : null,
                        translateY: thisNewCoord.axis === 'y' ? thisNewCoord.value : null,
                        translateZ: thisNewCoord.axis === 'z' ? thisNewCoord.value : null,
                    }
                }
            };
        }
    }

    onSendingPLNewColor(nPLColor: ComponentMenuType.IndexedValue) {
        const incomingValue = nPLColor.value as string;
        const hexColorNumber: number = parseInt(incomingValue.replace('#', '0x'), 16);
        // console.warn(`Changing Scene\'s Point of Light #${nPLColor.idx} COLOR to "#${hexColorNumber.toString(16)}"`);

        if (!nPLColor.uuid) {
            this.pointsOfLight[nPLColor.idx].emittance.color = hexColorNumber;
        } else {
            // This Point of Light was dynamically added by the User!
            this.userCreatedObj3D = { type: 'PointLight', operation: 'update',
                obj3DParams: {
                    uuid: nPLColor.uuid,
                    emittance: { color: hexColorNumber }
                }
            };
        }
    }

    onSendingPLNewIntensity(nPLIntensity: ComponentMenuType.IndexedValue) {
        const incomingValue = nPLIntensity.value as number;
        // console.warn(`Changing Scene\'s Point of Light #${nPLIntensity.idx} INTENSITY to "${incomingValue}"`);

        if (!nPLIntensity.uuid) {
            this.pointsOfLight[nPLIntensity.idx].emittance.intensity = Number(incomingValue);
        } else {
            // This Point of Light was dynamically added by the User!
            this.userCreatedObj3D = { type: 'PointLight', operation: 'update',
                obj3DParams: {
                    uuid: nPLIntensity.uuid,
                    emittance: { intensity: incomingValue }
                }
            };
        }
    }

    onSendingPLNewDistance(nPLDistance: ComponentMenuType.IndexedValue) {
        const incomingValue = nPLDistance.value as number;
        // console.warn(`Changing Scene\'s Point of Light #${nPLDistance.idx} DISTANCE to "${incomingValue}"`);

        if (!nPLDistance.uuid) {
            this.pointsOfLight[nPLDistance.idx].emittance.distance = Number(incomingValue);
        } else {
            // This Point of Light was dynamically added by the User!
            this.userCreatedObj3D = { type: 'PointLight', operation: 'update',
                obj3DParams: {
                    uuid: nPLDistance.uuid,
                    emittance: { distance: incomingValue }
                }
            };
        }
    }

    // ----------------------------------------------------------------------------

    onSendingActorAxisCoord(thisNewCoord: ComponentMenuType.Coordinate) {
        // console.warn(`Moving Actor to "${thisNewCoord.value}" @ ${thisNewCoord.axis.toUpperCase()} axis Pos.`);
        this.actor3D.position[thisNewCoord.axis] = Number(thisNewCoord.value);
    }

    onSendingActorAxisRotationCoord(thisNewRotationCoord: ComponentMenuType.Coordinate) {
        // console.warn(`Moving Actor to "${thisNewRotationCoord.value}" @ ${thisNewRotationCoord.axis.toUpperCase()} axis Pos.`);
        this.actor3D.rotation[thisNewRotationCoord.axis] = Number(thisNewRotationCoord.value);
    }

    // ----------------------------------------------------------------------------

    /**
     * Import each currently being rendered (extended Class) DEMO component's Stage parameters/values,
     * to get going the 3D initial view of it,
     * or
     * re-write them, in the currently being rendered (extended Class) DEMO component's Template
     * @Input() vars, from extended component's method 'onResetingToInitValues()', the inital Stage params values.
     *
     * MIND YOU
     * the intensive use of Object.assign({}, [initail object]) so we don't propagate back,
     * to the initial Stage params, the current values.
     *
     * @param on - string identifying wich ThreeJS Stage Obj is to revert params from, or 'all' of them
     */
    protected loadInitSceneParameters(on: string, fromThisDemoStage: { [key: string]: ComponentMenuType.DEMOstageParams }) {
        // -------------------------------------
        // Let's decouple, with 'as', the ambiguity of Types on DEMOstageParams,
        // a composed unique Type made of a UNION of them:
        // -------------------------------------
        const cameraInitStageParams = fromThisDemoStage.CAMERA as ComponentMenuType.Camera;
        const lightingInitStageParams = fromThisDemoStage.POINTS_OF_LIGHT as Array<ComponentMenuType.PointOfLight>;
        const actorInitStageParams = fromThisDemoStage.ACTOR3D as ComponentMenuType.Actor3D;
        // We can now clearly state the type of the Stage's (each 3D Object) object of values:

        switch (on) {
            case 'camera':
                this.setStageParamsForCamera(cameraInitStageParams);
                break;

            case 'pol-0':
            case 'pol-1':
                const idx: number = Number(on.substr(-1));
                this.setStageParamsForIndexedPointOfLight(lightingInitStageParams, idx);
                break;

            case 'actor':
                this.setStageParamsForActor3D(actorInitStageParams);
                break;

            case 'all':
                this.setStageParamsForCamera(cameraInitStageParams);
                this.setStageParamsForIndexedPointOfLight(lightingInitStageParams, null);
                this.setStageParamsForActor3D(actorInitStageParams);
                break;

            default:
                console.error(`******** ERROR: this ${on} does NOT exist on DEMO's TS declaration of 3D object's vars!`);
                break;
        }
    }

    /**
     * Camera (one) will always have...
     * @param params
     */
    private setStageParamsForCamera(params: ComponentMenuType.Camera) {
        this.camera = {
            name: params.name,
            position: Object.assign( {}, params.position ),
            rotation: Object.assign( {}, params.rotation),
            // axisUp: params.axisUp,
            // isLookingAtCenter: params.isLookingAtCenter,
            movements: Object.assign( {}, params.movements ),
            frustum: Object.assign( {}, params.frustum )
        };
    }

    /**
     * Might not have Points of Ligth - on '0.0-empty-stage-demo'
     * @param params 
     * @param idx 
     */
    private setStageParamsForIndexedPointOfLight(params: Array<ComponentMenuType.PointOfLight>, idx: number) {
        if (params !== undefined) {
            
            if (idx !== null) {
                this.pointsOfLight[idx] = {
                    name: params[idx].name,
                    position: Object.assign( {}, params[idx].position ),
                    emittance: Object.assign( {}, params[idx].emittance )
                };
            } else {
                params.forEach((eachPoLparams: ComponentMenuType.PointOfLight) =>
                    this.pointsOfLight.push(
                        {
                            name: eachPoLparams.name,
                            position: Object.assign( {}, eachPoLparams.position ),
                            emittance: Object.assign( {}, eachPoLparams.emittance )
                        }
                    )
                );
            }
        }
    }

    /**
     * Might not have Actors - on '0.0-empty-stage-demo'
     * @param params 
     */
    private setStageParamsForActor3D(params: ComponentMenuType.Actor3D) {
        if (params !== undefined) {
            this.actor3D = {
                name: params.name,
                position: Object.assign( {}, params.position ),
                rotation: Object.assign( {}, params.rotation )
            };
        }
    }

    protected startListening3DloggedEvents() {
        // The rendering of the logged msgs Service:
        this.loggEvent$ = this.ThreeJSloggerService ? this.ThreeJSloggerService.newEvtLogged$ : null;

        this.loggSubscription = this.loggEvent$ ? this.loggEvent$.subscribe((loggEvtObj: LoggEventData | string) => {
            // Check we're subscribing from the beginning, and not missing any sent msgs to the Child component:
            // if (this.consoleLogService.isThisBeingConsoled('SphereDemoComponent')) {
            //     console.log('$$$$$$$$ SPHERE DEMO new LOGG evt: ', loggEvtObj);
            // }

            const logEvtObjSTR = loggEvtObj as string;
            const logEvtObjOBJ = loggEvtObj as LoggEventData;

            // =============================================
            // LOADING 3D Object
            // =============================================
            if (logEvtObjOBJ.data.hasOwnProperty('isLoading')) {
                const isWheelLoading: boolean = logEvtObjOBJ.data.isLoading;
                // The false/true/false CHANGES on upper 'isWheelLoading', will trigger the activation/deactivation of the Loading Wheel:
                // (check 'loggEvtConstructor()' @ 'lib\services\app-demo\logger.service.ts' for 'isLoading' parameter)
                this.loadingWheelDirectiveCommand$.next(isWheelLoading);
                //
                // console.error(`<ng3d-loading-wheel /> status: isLoading: ${isWheelLoading}`);
                //
                // can be "fetched", individually, by each DEMO component, subscribing to this parent 'this.loadingWheelDirectiveCommand$',
                // and, from there, trigger specific actions of each DEMO component on the exact moment of the finishing of DEMO render.
            }

            // =============================================
            // CREATING dynamically 3D Object by the User
            // =============================================
            const where: string = logEvtObjOBJ.data.where;
            const operation: string = logEvtObjOBJ.data.operation;
            if ( where === 'PointOfLightComponent' && operation === 'create') {
                // const scalarValues: Array<SomeNgChanges> = logEvtObjOBJ.data.scalarValues;
                // const uuidObj: SomeNgChanges = scalarValues.filter((scalrObj: SomeNgChanges) => scalrObj.whatProp === 'uuid')[0];
                // const uuid: string = uuidObj.toValue;
                this.createdObj3D = logEvtObjOBJ.data.onWhat;
                // tslint:disable-next-line: no-string-literal
                // const uuid: string = this.createdObj3D['uuid'];
                // console.error('uui:', uuid);
                // console.error('Created 3D Object:', this.createdObj3D);
            }


        }) :  null;
    }
}
