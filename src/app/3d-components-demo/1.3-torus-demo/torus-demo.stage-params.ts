import * as ComponentMenuType from 'src/app/3d-component-menu/component-menu.interfaces';

export const CAMERA: ComponentMenuType.Camera = {
    name: 'TORUS Camera',
    position: { x: 18, y: 22, z: 13 },
    rotation: { x: 0, y: 35, z: 0 },
    frustum: { fov: 70, near: 1, far: 1100 }
};

export const POINTS_OF_LIGHT: Array<ComponentMenuType.PointOfLight> = [
    {
        name: '1st TORUS Stage Light',
        position: { x: 50, y: 50, z: 50 },
        emittance: { color: 0xffffff, intensity: 3, distance: 100 }
    },
    {
        name: '2nd TORUS Stage Light',
        position: { x: 50, y: 50, z: -50 },
        emittance: { color: 0xffffff, intensity: 0.7, distance: -50 }
    }
];

export const ACTOR3D: ComponentMenuType.Actor3D = {
    name: 'Yellowish donut',
    position: { x: 10, y: 5, z: -0.5 },
    rotation: { x: -20, y: 0, z: 0 }
};
