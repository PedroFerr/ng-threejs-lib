/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Deg2RadPipe } from '../../util/pipes/rad-degrees/deg2rad.pipe';

import { TorusDemoComponent } from './torus-demo.component';

describe('TorusDemoComponent', () => {
    let component: TorusDemoComponent;
    let fixture: ComponentFixture<TorusDemoComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TorusDemoComponent, Deg2RadPipe]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TorusDemoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
