import * as ComponentMenuType from 'src/app/3d-component-menu/component-menu.interfaces';

export const CAMERA: ComponentMenuType.Camera = {
    name: 'Office Camera',
    position: { x: 0, y: -1500, z: 90 },
    rotation: { x: 0, y: 0, z: 90 },
    movements: {
        axisUp: 'z',                  // Imposed by the OBJ file (type of) Modeling...
        isLookingAtCenter: false,    // User can navigate freely "inside" the 3 floor Office
    },
    frustum: { fov: 22, near: 10, far: 10000 }    // <= 10.000 to see the 2nd floor terrace, going left! | fov: 160 (farest back) 22 (cool)
};

export const POINTS_OF_LIGHT: Array<ComponentMenuType.PointOfLight> = [
    {
        name: '1st Office\'s Stage Light',
        position: { x: -50, y: 50, z: 20 },
        emittance: { color: 0xffff00, intensity: 0.3, distance: 99 }
    },
    {
        name: '2nd Office\'s Stage Light',
        position: { x: 500, y: 50, z: 50 },
        emittance: { color: 0x408080, intensity: 0.9, distance: 1000 }
    }
];

export const ACTOR3D = {} as ComponentMenuType.Actor3D;
