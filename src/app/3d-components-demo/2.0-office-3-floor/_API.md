# 3D Component <ng3d-obj-loader />

## Input Directives:

* `model` `string` The URL, URI or path of the **OBJ** file to download from.
