import { Component, OnInit, AfterViewInit, Renderer2 } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { Object3DDemoDirective } from '../_common/object3D-demo.directive';

import { LoggerService, RendererService } from 'ng-threejs-lib';

import { LGG_TYPE } from './_ts-html-api-code';
import * as Stage from './obj-loader-demo.stage-params';

@Component({
    selector: 'app-obj-loader-demo',
    templateUrl: './obj-loader-demo.component.html',
    styleUrls: ['../_common/ng3d-stage-style.scss']
})
export class ObjLoaderDemoComponent extends Object3DDemoDirective implements OnInit, AfterViewInit {

    codeStringsObj = LGG_TYPE;

    menuTriggerButton: HTMLElement;
    componentTitle: HTMLElement;

    // Loading Wheel to activate when something is happening on the 3D Stage world:
    loadingWheelCommand$: Observable<boolean>;
    loadingWheelHostId: string;

    constructor(
        protected ThreeJSrendererService: RendererService,
        protected ThreeJSloggerService: LoggerService,

        private NgRenderer: Renderer2
    ) {
        super(ThreeJSrendererService, ThreeJSloggerService);
    }

    ngOnInit() {
        // Select DOM elements to change colors:
        this.menuTriggerButton = document.querySelector('.menu-trigger');    // It's on <app-root />
        this.componentTitle = document.querySelector('.component-name');    // It's on <app-component-menu />, parent of THIS.

        this.loadInitSceneParameters('all', Stage);

        this.loadingWheelHostId = 'obj-loader-demo';
        // We want to keep Observing, from now on, the 'this.loadingWheelDirectiveCommand$' Subject changes,
        // that, by its turns, is commanded by the ThreeJS Logged Events, that keep pouring in on our Object3DDemoDirective
        this.loadingWheelCommand$ = this.loadingWheelDirectiveCommand$;
    }

    ngAfterViewInit() {
        // As we have a black background, on this page, maybe the button of the side Menu trigger, looks better in $secondary:
        if (this.menuTriggerButton) {
            this.NgRenderer.setStyle(this.menuTriggerButton, 'color', 'var(--secondary)');
        }
        // REMEMBER we need to set it back to $primary color on leaving => use Angular's Router 'CanDeactivate' over a Guard condition.

        // As this is hapening on te parent, new route will clear to new loading => the component's name div will always start $primary:
        if (this.componentTitle) {
            this.NgRenderer.setStyle(this.componentTitle, 'color', 'var(--secondary)');
        }
    }

    /**
     * In case user gets "lost", and rendering goes to some 'out-of-sight' spots,
     * he/she can reset the all Stage to the initial coordinates,
     * clicking on a "reset" button @ each Stage 3D Object settings tab's template panel.
     *
     * In this case, all we have to do is to load the initial parameters, somewhere on './xxxx-demo.stage-params.ts' file,
     * for the specific 3D object's Stage values,
     * as we did on ngOnInit()'s 'loadInitSceneParameters()' method, for all of them.
     *
     * @param on - string identifying wich ThreeJS Stage Obj is to revert params from
     */
    onResetingToInitValues(on: string) {
        this.loadInitSceneParameters(on, Stage);
    }
}
