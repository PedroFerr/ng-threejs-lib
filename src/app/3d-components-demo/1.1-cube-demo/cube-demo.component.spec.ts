/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Deg2RadPipe } from '../../util/pipes/rad-degrees/deg2rad.pipe';

import { CubeDemoComponent } from './cube-demo.component';

describe('CubeDemoComponent', () => {
    let component: CubeDemoComponent;
    let fixture: ComponentFixture<CubeDemoComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CubeDemoComponent, Deg2RadPipe]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CubeDemoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
