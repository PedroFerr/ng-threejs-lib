# 3D Component <ng3d-cube-mesh />

## Input Directives:

* `width` `number` The **length** of the <ng3d-cube-mesh />'s edges parallel to the **X axis** . Optional; defaults to 1..
* `height` `number` The **length** of the <ng3d-cube-mesh />'s edges parallel to the **Y axis**. Optional; defaults to 1.
* `depth` `number` The *length* of the <ng3d-cube-mesh />'s edges parallel to the **Z axis**. Optional; defaults to 1.
* 
* `widthSegments` `number` <ng3d-cube-mesh />'s *width* of each segment.
* `heightSegments` `number` <ng3d-cube-mesh />'s *height* of each segment.
* `depthSegments` `number` <ng3d-cube-mesh />'s number of segmented rectangular **faces** along the depth of the sides. Optional; defaults to 1.
* ...

## Output Directives

 * ...
