import * as ComponentMenuType from 'src/app/3d-component-menu/component-menu.interfaces';

export const CAMERA: ComponentMenuType.Camera = {
    name: 'CUBE Camera',
    movements: {
        axisUp: 'x',                    // If not stated, it's the 'AxisTypeInit' @ 'lib\ng-threejs-lib.interfaces.ts' the default one.
        // isLookingAtCenter: true,     // It's the defaut one, anyway...
    },
    position: { x: 83, y: 63, z: 53 },
    rotation: { x: 40, y: 0, z: 0 },
    frustum: { fov: 90, near: 1, far: 1100 }
};

export const POINTS_OF_LIGHT: Array<ComponentMenuType.PointOfLight> = [
    {
        name: '1st CUBE Stage Light',
        position: { x: 50, y: 50, z: 50 },
        emittance: { color: 0xffffff, intensity: 0.9, distance: 1000 }
    },
    {
        name: '2nd CUBE Stage Light',
        position: { x: (30 + 40 + 10), y: ((-30 / 2) + (-20) + (-20)), z: -70 },
        emittance: { color: 0x00ACC1, intensity: 2.9, distance: 70 }
    }
];

export const ACTOR3D: ComponentMenuType.Actor3D = {
    name: 'Green cube',
    position: { x: 0, y: 0, z: 0 },
    rotation: { x: 0, y: 0, z: 0 }
};
