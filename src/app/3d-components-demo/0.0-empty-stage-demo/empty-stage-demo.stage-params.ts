import * as ComponentMenuType from 'src/app/3d-component-menu/component-menu.interfaces';

export const CAMERA: ComponentMenuType.Camera = {
    name: 'Empty Stage Camera',
    // position: { x: 20, y: 58, z: 50 },
    movements: { axisUp: 'z' },
    position: { x: 60, y: 83, z: -124 },
    rotation: { x: 0, y: 0, z: 240 },

    frustum: { fov: 50, near: 1, far: 1100 }
};
