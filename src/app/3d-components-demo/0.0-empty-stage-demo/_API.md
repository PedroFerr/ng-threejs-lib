# 3D Component Empty Stage

It's an **empty Stage** - there's no 3D Actors to render!

We have just a small basic set of Angular components, needed for a full **3D rendering** :
* one `Camera` directly controlled by your mouse within an `Orbit` enabling a 360º full inspection of any Component, from any angle - **Zoom** in/out with the mouse wheel; **Grab and Move around** with mouse left button
* and one `3D Stage Scene` composed of a very handy set of `helpers` a **3D-Axis** and a **3D-Grid** *ThreeJS* helpers, with **illumination** done by **2** `Light Point` components, like in the real 3D world!

And, off course, we also have **this** Component's bottom Menu, you are now reading the **API** *tab* of.
There's also the **HTML** and the **TS** *tabs* - with the corresponding code of this DEMO Component.

And, most important, the **Settings** (top) one, where you can change several settings of the 3D Component, that will influence directly its rendering on **your** Angular application.
Once achieved the **Stage Scene** you desire, you should then mount the 3D Component on **your Angular Web App** with these **settings** / **parameters** / **Input()/Output()** + any other coded **logic/markup** you can find relevant on the HTML/TS code *tabs*

--

### Useful `*.md` code Info (only on this 3D Component):
URLs can be simply made like so: [Go to Home Page](http://ng3d.miles-net.com/)
And it's possible to open it on a new Tab: [Github's Markdown language Tutorial](https://guides.github.com/features/mastering-markdown/" target="_blank)

If you want to embed images, this is how you do it:
![Image of happyness](http://ng3d.miles-net.com/assets/model/smiley/Smiley_Sphere001_5.jpg)

API files should ALWAYS finish wih an empty `space` / `enter` - sometimes, they don't *translate* , if not.

An horizontal rule is coded with 2 (or more) dashes.
-------------------
It's sometimes handy for breaking, or ending :-), things up.
