import { Component, OnInit, OnDestroy } from '@angular/core';

import { Object3DDemoDirective } from '../_common/object3D-demo.directive';

import { LoggerService, RendererService } from 'ng-threejs-lib';

import { LGG_TYPE } from './_ts-html-api-code';
import * as Stage from './empty-stage-demo.stage-params';

@Component({
    selector: 'app-empty-stage-demo',
    templateUrl: './empty-stage-demo.component.html',
    styleUrls: ['../_common/ng3d-stage-style.scss']
})
export class EmptyStageDemoComponent extends Object3DDemoDirective implements OnInit {

    codeStringsObj = LGG_TYPE;

    constructor(
        protected ThreeJSrendererService: RendererService,
        protected ThreeJSloggerService: LoggerService
    ) {
        super(ThreeJSrendererService, ThreeJSloggerService);
    }

    ngOnInit() {
        this.loadInitSceneParameters('all', Stage);
    }

    /**
     * In case user gets "lost", and rendering goes to some 'out-of-sight' spots,
     * he/she can reset the all Stage to the initial coordinates,
     * clicking on a "reset" button @ each Stage 3D Object settings tab's template panel.
     *
     * In this case, all we have to do is to load the initial parameters, somewhere on './xxxx-demo.stage-params.ts' file,
     * for the specific 3D object's Stage values,
     * as we did on ngOnInit()'s 'loadInitSceneParameters()' method, for all of them.
     *
     * @param on - string identifying wich ThreeJS Stage Obj is to revert params from
     */
    onResetingToInitValues(on: string) {
        this.loadInitSceneParameters(on, Stage);
    }

}
