import * as ComponentMenuType from 'src/app/3d-component-menu/component-menu.interfaces';

export const CAMERA = {} as ComponentMenuType.Camera;

export const POINTS_OF_LIGHT = [] as Array<ComponentMenuType.PointOfLight>;

export const ACTOR3D = {} as ComponentMenuType.Actor3D;
