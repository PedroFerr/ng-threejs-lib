/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Deg2RadPipe } from '../../util/pipes/rad-degrees/deg2rad.pipe';

import { ConicalCylinderDemoComponent } from './conical-cylinder-demo.component';

describe('ConicalCylinderDemoComponent', () => {
    let component: ConicalCylinderDemoComponent;
    let fixture: ComponentFixture<ConicalCylinderDemoComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ConicalCylinderDemoComponent, Deg2RadPipe]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConicalCylinderDemoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
