import * as ComponentMenuType from 'src/app/3d-component-menu/component-menu.interfaces';

export const CAMERA: ComponentMenuType.Camera = {
    name: 'CONICAL Cylinder Camera',
    position: { x: 20, y: 58, z: 50 },
    rotation: { x: 0, y: 50, z: 0 },
    frustum: { fov: 60, near: 1, far: 1100 }
};

export const POINTS_OF_LIGHT: Array<ComponentMenuType.PointOfLight> = [
    {
        name: '1st CONICAL Stage Light',
        position: { x: 50, y: 50, z: 50 },
        emittance: { color: 0xffffff, intensity: 0.9, distance: 1000 }
    },
    {
        name: '2nd CONICAL Stage Light',
        position: { x: (30 + 40 + 10), y: ((-30 / 2) + (-20) + (-20)), z: -70 },
        emittance: { color: 0x80ff80, intensity: 4, distance: 200 }
    }
];

export const ACTOR3D: ComponentMenuType.Actor3D = {
    name: 'Blue chinese hat',
    position: { x: 40, y: -20, z: -30 },
    rotation: { x: -20, y: 0, z: 0 }
};
