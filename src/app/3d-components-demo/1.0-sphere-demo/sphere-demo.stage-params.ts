import * as ComponentMenuType from 'src/app/3d-component-menu/component-menu.interfaces';

export const CAMERA: ComponentMenuType.Camera = {
    name: 'SPHERE Camera',
    // movements: {
    //     axisUp: 'y',                // It's the defaut one, anyway...
    //     isLookingAtCenter: true,    // It's the defaut one, anyway...
    // },
    position: { x: 20, y: 58, z: 50 },
    rotation: { x: 0, y: 70, z: 0 },
    frustum: { fov: 60, near: 1, far: 1100 }
};

export const POINTS_OF_LIGHT: Array<ComponentMenuType.PointOfLight> = [
    {
        name: '1st SPHERE Stage Light',
        position: { x: 50, y: 50, z: 50 },
        emittance: { color: 0xffffff, intensity: 0.9, distance: 1000 }
    },
    {
        name: '2nd SPHERE Stage Light',
        position: { x: -50, y: 50, z: 20 },
        emittance: { color: 0xffffff, intensity: 0.1, distance: 1000 }
    }
];

export const ACTOR3D: ComponentMenuType.Actor3D = {
    name: 'Red Sphere',
    position: { x: 10, y: 15, z: 25 },
    rotation: { x: -20, y: 0, z: 0 }
};
