import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Object3DDemoDirective } from '../_common/object3D-demo.directive';

import { LoggerService, RendererService } from 'ng-threejs-lib';

import { LGG_TYPE } from './_ts-html-api-code';
import * as Stage from './sphere-demo.stage-params';

@Component({
    selector: 'app-sphere-demo',
    templateUrl: './sphere-demo.component.html',
    styleUrls: ['../_common/ng3d-stage-style.scss']
})
export class SphereDemoComponent extends Object3DDemoDirective implements OnInit {

    codeStringsObj = LGG_TYPE;

    // Loading Wheel to activate when something is happening on the 3D Stage world:
    loadingWheelCommand$: Observable<boolean>;
    loadingWheelHostId: string;

    constructor(
        protected ThreeJSrendererService: RendererService,
        protected ThreeJSloggerService: LoggerService
    ) {
        super(ThreeJSrendererService, ThreeJSloggerService);
    }

    ngOnInit() {

        this.loadInitSceneParameters('all', Stage);

        this.loadingWheelHostId = 'sphere-demo';
        // We want to keep Observing, from now on, the 'this.loadingWheelDirectiveCommand$' Subject changes,
        // that, by its turns, is commanded by the ThreeJS Logged Events, that keep pouring in on our Object3DDemoDirective
        this.loadingWheelCommand$ = this.loadingWheelDirectiveCommand$;
    }

    /**
     * In case user gets "lost", and rendering goes to some 'out-of-sight' spots,
     * he/she can reset the all Stage to the initial coordinates,
     * clicking on a "reset" button @ each Stage 3D Object settings tab's template panel.
     *
     * In this case, all we have to do is to load the initial parameters, somewhere on './xxxx-demo.stage-params.ts' file,
     * for the specific 3D object's Stage values,
     * as we did on ngOnInit()'s 'loadInitSceneParameters()' method, for all of them.
     *
     * @param on - string identifying wich ThreeJS Stage Obj is to revert params from
     */
    onResetingToInitValues(on: string) {
        this.loadInitSceneParameters(on, Stage);
    }
}
