import { FilesCodeTransporter } from 'src/app/3d-component-menu/component-menu.interfaces';

// We want to export, for each 3D Component at the demo lib app,
// the code (as a string, to be later highlighted by 'Prism') of (each of) the HTML, TS, etc. files THIS Component is made of.
// So we need to "$ npm install raw-loader"
// After, create a "typings.d.ts" new file were we declare "!raw-loader!*"as a new module to immport the string on it.
// --------------------
// There's an exception: Prism does not handle *.md files to highlight them.
// On those, we use "$ npm install html-loader" and "$ npm install markdown-loader" and also add it as a new Module @ "typings.d.ts".

import * as CommonDirectiveTScode from '!raw-loader!../_common/object3D-demo.directive';
import * as TScode from '!raw-loader!./sphere-demo.component';
import * as HTMLcode from '!raw-loader!./sphere-demo.component.html';
// import * as APIcode from 'html-loader?minimize=false!markdown-loader!./API.md';
//
// NOT USING THIS UPPER COMBINATION ANYMORE!
// Don't know why - I think security reasons, as Angular has envolved - the styling behaviour is NOT what used to be,
// when we discover this markup dependencies still with Angular 6 (on the UI/UX lib of components).
//
// Just the simple, plain 'raw-loader' dependency, to get all the (any!) file content:
import * as APIcode from '!raw-loader!./_API.md';
// Then, this '.md' content will be transformed to HTML one, by "$ npm i markdown-to-html-converter" dependency
// already at the 'src\app\3d-component-menu\component-menu.component.ts' component's logic.

import * as InitParams from '!raw-loader!./sphere-demo.stage-params';

// And export the code string from them ALL:
export const LGG_TYPE: FilesCodeTransporter = {
    name: 'Sphere Mesh', TS: `${TScode}\n${CommonDirectiveTScode}`, HTML: HTMLcode, MDAPI: APIcode, PARAMS: InitParams
};
