/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Deg2RadPipe } from '../../util/pipes/rad-degrees/deg2rad.pipe';

import { SphereDemoComponent } from './sphere-demo.component';

describe('SphereDemoComponent', () => {
    let component: SphereDemoComponent;
    let fixture: ComponentFixture<SphereDemoComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SphereDemoComponent, Deg2RadPipe]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SphereDemoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
