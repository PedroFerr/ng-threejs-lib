# 3D Component <ng3d-sphere-mesh />

## Input Directives:

* `radius` `number` The radius of the <ng3d-sphere-mesh />'s sphere ( **half** of the **diameter** ).
* `widthSegments` `number` <ng3d-sphere-mesh />'s *width* of each segment.
* `heightSegments` `number` <ng3d-sphere-mesh />'s *height* of each segment.
* `material` `string` Type of **material** used on <ng3d-sphere-mesh /> - there are 3 types: `'MeshPhongMaterial' | 'MeshLambertMaterial' | 'MeshPhongMaterial'`
* ...

## Output Directives

 * ...
