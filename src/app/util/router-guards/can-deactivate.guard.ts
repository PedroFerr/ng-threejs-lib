import { Injectable, Renderer2 } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';

// And the Components to have their own routes Guarded, regarding the moment of leavng them:
import { AboutComponent } from '../../views/about/about.component';
import { PageNotFoundComponent } from '../../views/page-not-found/page-not-found.component';

// Comprehensive examples for Ng Router Guards, for "deactivate" guard routes in particularly,
// (usually used to warn people if they are navigating AWAY from a page where they have some unsaved changes/actions to do)
// @ https://codecraft.tv/courses/angular/routing/router-guards/

// Quick example and "do-not-forget-this" @ https://stackoverflow.com/a/50876140/2816279

@Injectable({
    // Next enables us to NOT declare this Service in 'app.module.ts' under "@NgModule({ ..., providers: [ CanDeactivateGuard ] })"
    // => enables "Tree Shaking" - service won't be on build, if not used/needed anymore
    providedIn: 'root'
})
export class RevertMenuBtnColor implements CanDeactivate<AboutComponent | PageNotFoundComponent> {

    constructor(
        // private renderer: Renderer2
    ) {
        // MIND YOU:
        // this constructor() can NOT have any parameters!
        // Angular's 'CanDeactivate' interface, that this Class is implementing, does not accepts parameters...
        // It will simply NOT work - no errors thrown at compilation nor run time.
    }

    canDeactivate(
        component: AboutComponent | PageNotFoundComponent,
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | boolean {

        const menuTriggerButton: HTMLElement = document.querySelector('.menu-trigger');
        // this.renderer.setStyle(menuTriggerButton, 'color', 'var(--primary)');^
        //
        // MIND YOU:
        // Upper will simply NOT work since this is not an Angular Component, with an Angular Template to render!
        // Current 'RevertMenuBtnColor' JS Class is just providing an Angular Router service, through the CanDeactivate interface.
        // We "do" the service (here we want to change DOM's element styling color)
        // BEFORE this Class tells the router to proceed, or not, the routing to next asked route
        //
        // So... you have to grab, and manipulate, the DOM by yourself, within old school:
        menuTriggerButton.style.color = 'var(--primary)';

        return true;
    }
}
