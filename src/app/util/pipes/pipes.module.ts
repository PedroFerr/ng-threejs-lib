import { NgModule } from '@angular/core';

import { SafePipe } from './dom-sanitizer/safe.pipe';
import { TrackByPropertyPipe } from './ng-for/trackByProperty.pipe';
import { Deg2RadPipe } from './rad-degrees/deg2rad.pipe';
import { Rad2DegPipe } from './rad-degrees/rad2deg.pipe';

/**
 * Because we are importing SafePipe in "n" Modules
 *  - any one which has a component that needs some kind of Angular Sanitizaton for the HTML. CSS; scripts, URL, etc.
 * we did a Module just with this 'safe.pipe.ts' Pipe Directive and, then, we can import it to any number/type of Modules, in any place.
 * whenever a component needs the (.... | safeSanitized: 'xxxxx') PIPE within its Template.
 */
@NgModule({
    imports: [],
    declarations: [
        SafePipe,
        TrackByPropertyPipe,
        Deg2RadPipe,
        Rad2DegPipe
    ],
    exports: [
        SafePipe,
        TrackByPropertyPipe,
        Deg2RadPipe,
        Rad2DegPipe
    ],
})

export class PipesModule { }
