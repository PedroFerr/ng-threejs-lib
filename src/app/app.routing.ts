import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RevertMenuBtnColor } from './util/router-guards/can-deactivate.guard';

import { HomeComponent } from './views/home/home.component';
import { AboutComponent } from './views/about/about.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';

import { EmptyStageDemoComponent } from './3d-components-demo/0.0-empty-stage-demo/empty-stage-demo.component';
import { SphereDemoComponent } from './3d-components-demo/1.0-sphere-demo/sphere-demo.component';
import { CubeDemoComponent } from './3d-components-demo/1.1-cube-demo/cube-demo.component';
import { ConicalCylinderDemoComponent } from './3d-components-demo/1.2-conical-cylinder-demo/conical-cylinder-demo.component';
import { TorusDemoComponent } from './3d-components-demo/1.3-torus-demo/torus-demo.component';

import { ObjLoaderDemoComponent } from './3d-components-demo/2.0-office-3-floor/obj-loader-demo.component';
import { ObjStreamLoaderDemoComponent } from './3d-components-demo/3.0-obj-stream-loader-demo/obj-stream-loader-demo.component';

const routes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },    // <= "pathMatch: 'full'" not to load PageNotFound at the same time
    { path: 'about', component: AboutComponent, canDeactivate: [RevertMenuBtnColor] },

    { path: 'empty-stage', component: EmptyStageDemoComponent },

    { path: 'sphere', component: SphereDemoComponent },
    { path: 'cube', component: CubeDemoComponent },
    { path: 'conical-cylinder', component: ConicalCylinderDemoComponent },
    { path: 'torus', component: TorusDemoComponent },

    { path: 'obj', component: ObjLoaderDemoComponent, canDeactivate: [RevertMenuBtnColor] },
    { path: 'obj-stream-loader', component: ObjStreamLoaderDemoComponent },

    { path: '**', component: PageNotFoundComponent, canDeactivate: [RevertMenuBtnColor] }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
