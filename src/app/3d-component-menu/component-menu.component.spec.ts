/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SafePipe } from '../util/pipes/dom-sanitizer/safe.pipe';

import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ComponentMenuComponent } from './component-menu.component';

describe('ComponentMenuComponent', () => {
    let component: ComponentMenuComponent;
    let fixture: ComponentFixture<ComponentMenuComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule
            ],
            declarations: [ComponentMenuComponent, SafePipe]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ComponentMenuComponent);
        component = fixture.debugElement.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
