import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PipesModule } from '../util/pipes/pipes.module';

import { NgThreejsLibModule } from 'ng-threejs-lib';

import { ComponentMenuComponent } from './component-menu.component';

// With children:
import { SettingsComponent } from './settings/settings.component';

// That has childrens:
// Common Move card for both Translation and Rotation movements
import { SettingsMoveCardComponent } from './settings/_common/settings-move-card/settings-move-card.component';
// The form to create, on current DEMO's 3D Stage, new 3D Objects (Actors, Cameras, PointOfLight, etc)
import { New3dObjectFormComponent } from './settings/_common/new-3d-object-form/new-3d-object-form.component';

// ... and for the other (3D object props) cards:
import { Actors3DComponent } from './settings/actors-3D/actors-3D.component';
import { PerspectiveCameraComponent } from './settings/perspective-camera/perspective-camera.component';
import { PointOfLightComponent } from './settings/point-of-light/point-of-light.component';


@NgModule({
    imports: [
        CommonModule,

        NgThreejsLibModule,
        PipesModule
    ],
    declarations: [
        ComponentMenuComponent,
        SettingsComponent,
        SettingsMoveCardComponent, New3dObjectFormComponent,
        PerspectiveCameraComponent,
        PointOfLightComponent,
        Actors3DComponent,
    ],
    exports: [
        ComponentMenuComponent,
        SettingsComponent,
        SettingsMoveCardComponent, New3dObjectFormComponent,
        PerspectiveCameraComponent,
        PointOfLightComponent,
        Actors3DComponent,

        NgThreejsLibModule,   // Send it to whomever imports THIS 'ComponentMenuModule' Module
        PipesModule           // Send it to whomever imports THIS 'ComponentMenuModule' Module
    ]
})
export class ComponentMenuModule { }
