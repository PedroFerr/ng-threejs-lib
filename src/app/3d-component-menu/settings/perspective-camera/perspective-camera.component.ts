import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

import { SomeNgChanges, AxisTypeInit } from 'ng-threejs-lib';

import * as ComponentMenuType from '../../component-menu.interfaces';

@Component({
    selector: 'app-perspective-camera',
    templateUrl: './perspective-camera.component.html',
    styleUrls: ['../_common/settings-children-components.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PerspectiveCameraComponent implements OnInit, OnChanges {

    isLookingAtCenter = true;
    labelLookAtCenter = `<span class="font-lato text-ng3d-primary small">(0,0,0)</span>`;
    labelNoLookChanges = `<span class="font-lato small">(<i>none</i>)</span>`;

    currentAxisUp: ComponentMenuType.AxisType = AxisTypeInit;

    obj3Dtype: ComponentMenuType.IndexedDEMOstageParams = { param: 'camera', idx: 0 };

    @Input() demoName: string;
    // Get the current Stage parameters values sent to ThreeJS rendering, from the DEMO Component being rendered:
    @Input() cameraParams: ComponentMenuType.Camera;
    @Input() positionUpdated: SomeNgChanges;
    @Input() rotationUpdated: SomeNgChanges;

    // On the same way, send back to the DEMO Component being rendered, the values User is fiddling around on each <input />:
    @Output() sendCameraNewCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendCameraNewRotationCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendCameraMoveParam = new EventEmitter<ComponentMenuType.MoveParam>();


    @Output() sendCameraNewFOV = new EventEmitter<number>();
    @Output() sendCameraNewNEAR = new EventEmitter<number>();
    @Output() sendCameraNewFAR = new EventEmitter<number>();

    @Output() resetToInitValues = new EventEmitter<string>();

    constructor() { }

    ngOnInit() {
    }

    ngOnChanges(propsChanged: SimpleChanges) {
        const obj3DparamsChgs = propsChanged.cameraParams;

        if (obj3DparamsChgs && obj3DparamsChgs.firstChange === true && obj3DparamsChgs.currentValue.hasOwnProperty('movements')) {
            // Check if the 'xxxx-demo.stage-params' sets a specific Camera's 'axisUp' or,
            // in absence of it, we should use the already attributed 'defaultAxisUp' one:
            const cameraParams = this.cameraParams as ComponentMenuType.Camera;
            const moveParams: {[key: string]: ComponentMenuType.AxisType | boolean} = obj3DparamsChgs.currentValue.movements;

            if (moveParams.hasOwnProperty('axisUp') && moveParams.axisUp !== undefined) {
                // We are definetely constructing a CAMERA settings where 'axisUp' is set to some value/axis!
                this.currentAxisUp = cameraParams.movements.axisUp;
            }

            // tslint:disable-next-line: max-line-length
            if (moveParams.hasOwnProperty('isLookingAtCenter') && moveParams.isLookingAtCenter === false) {    // default is 'true' => is applied if 'undefined'
                this.isLookingAtCenter = false;
            }
        }


    }

    rotAxisChg(evt: MouseEvent) {
        const selectedOption /*: HTMLElement*/ = evt.target as any;
        this.currentAxisUp = selectedOption.value;
        this.sendCameraMoveParam.emit( {param: 'axisUp', value: this.currentAxisUp} );
    }

    /**
     * In case user gets "lost", and rendering goes to some white/black spots,
     * he/she can reset the all Stage to the initial coordinates.
     *
     * Necessary operations should be made at the parent Component - send back the trigger.
     *
     * @param on - string identifying wich ThreeJS Stage Obj is to revert params from
     */
    resetStage(on: string) {
        if ( window.confirm('Are you sure\nyou want to reset this Obj CAMERA params to init values...?')) {
            this.resetToInitValues.emit(on);
        }
    }
}
