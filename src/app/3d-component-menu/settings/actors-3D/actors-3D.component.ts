import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

import * as ComponentMenuType from '../../component-menu.interfaces';

@Component({
    selector: 'app-actors-3d',
    templateUrl: './actors-3D.component.html',
    styleUrls: ['../_common/settings-children-components.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush

})
export class Actors3DComponent implements OnInit {

    @Input() demoName: string;
    // Get the current Stage parameters values sent to ThreeJS rendering, from the DEMO Component being rendered:
    @Input() actor3DParams: ComponentMenuType.Actor3D;

    // On the same way, send back to the DEMO Component being rendered, the values User is fiddling around on each <input />:
    @Output() sendActorNewCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendActorNewRotationCoord = new EventEmitter<ComponentMenuType.Coordinate>();

    @Output() resetToInitValues = new EventEmitter<string>();

    constructor() { }

    ngOnInit() {
    }

    /**
     * In case user gets "lost", and rendering goes to some white/black spots,
     * he/she can reset the all Stage to the initial coordinates.
     *
     * Necessary operations should be made at the parent Component - send back the trigger.
     *
     * @param on - string identifying wich ThreeJS Stage Obj is to revert params from
     */
    resetStage(on: string) {
        if ( window.confirm('Are you sure\nyou want to reset this 3D Actor position to init values...?')) {
            this.resetToInitValues.emit(on);
        }
    }

}
