/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Actors3DComponent } from './actors-3D.component';

describe('Actors3DComponent', () => {
  let component: Actors3DComponent;
  let fixture: ComponentFixture<Actors3DComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Actors3DComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Actors3DComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
