import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

import { SomeNgChanges, AxisTypeInit } from 'ng-threejs-lib';

import * as ComponentMenuType from '../../../component-menu.interfaces';

@Component({
    selector: 'app-settings-move-card',
    templateUrl: './settings-move-card.component.html',
    styleUrls: ['./settings-move-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsMoveCardComponent implements OnInit, OnChanges {

    newObjNamePfx: string = ComponentMenuType.UserNewObjNamePfx;
    defaultAxisUp: ComponentMenuType.AxisType = AxisTypeInit;

    // Regarding set 'axisType', we only set DISABLED = false, on the 'currentAxisUp' axis - User can only Input values on this axis.
    // All the others have DISABLED set to true:
    // disableRotInput = [true, true, true];
    //
    // Since we have a default 'currentAxisUp' (if nothing is said on 'xxxx-demo.stage-params' Camera settings),
    // we must also have a default set for 'disableRotInput' array of booleans:
    disableRotInput: Array<boolean> = [this.defaultAxisUp !== 'x', this.defaultAxisUp !== 'y', this.defaultAxisUp !== 'z'];

    // Define which Template will be render on this component HTML commonly used for 2 movement types:
    @Input() movementTypeTemplate: 'position' | 'rotation';

    // Get the current Stage parameters values sent to ThreeJS rendering, from the DEMO Component being rendered:
    @Input() obj3Dtype: ComponentMenuType.IndexedDEMOstageParams;
    @Input() obj3Dparams: ComponentMenuType.Actor3D | ComponentMenuType.Camera;
    @Input() perspectiveCameraRotAxis: ComponentMenuType.AxisType;

    // Updates comings from "a" Logged Event, catched on 'settings' parent componnet:
    renderedPosX: number = null;
    renderedPosY: number = null;
    renderedPosZ: number = null;
    @Input('obj3DparamsUpdatedPos') set posCoordValues(changedProps: SomeNgChanges) {
        const stringOfPos = changedProps ? changedProps.toValue as string : null;
        const positions: Array<string> = stringOfPos ? stringOfPos.split(',') : [];

        if (positions.length > 0) {
            this.renderedPosX = Number(positions[0].substr(1));
            this.renderedPosY = Number(positions[1]);
            this.renderedPosZ = Number(positions[2].slice(0, -1));
        }
    }

    // On the same way, send back to the DEMO Component being rendered, the values User is fiddling around on each <input />:
    @Output() sendObj3DCoord = new EventEmitter<ComponentMenuType.Coordinate>();

    @Output() removeObj3D = new EventEmitter<string>();

    @Output() resetToInitValues = new EventEmitter<string>();

    constructor() { }

    ngOnInit() {
    }

    ngOnChanges(propsChanged: SimpleChanges) {
        const rotAxisChgs = propsChanged.perspectiveCameraRotAxis;

        if (rotAxisChgs) {
            const thisAxis: ComponentMenuType.AxisType = rotAxisChgs.currentValue;
            // ... and User can only Input values on THAT axis:
            ['x', 'y', 'z'].forEach(
                (eachAxisToEnable: ComponentMenuType.AxisType, idx: number) => this.disableRotInput[idx] = eachAxisToEnable !== thisAxis
            );
        }
    }

    /**
     * In case user gets "lost", and rendering goes to some white/black spots,
     * he/she can reset the all Stage to the initial coordinates.
     *
     * Necessary operations should be made at the parent Component - send back the trigger.
     *
     * @param on - string identifying wich ThreeJS Stage Obj is to revert params from
     */
    resetStage(on: string) {
        if ( window.confirm('Are you sure\nyou want to reset this 3D Actor position to init values...?')) {
            this.resetToInitValues.emit(on);
        }
    }

    removeObj3DFromStage(obj3DToRemoveUuid: string) {
        this.removeObj3D.emit(obj3DToRemoveUuid);
    }
}
