import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import * as ComponentMenuType from '../../../component-menu.interfaces';

@Component({
    selector: 'app-new-3d-object-form',
    templateUrl: './new-3d-object-form.component.html',
    styleUrls: ['../settings-children-components.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class New3dObjectFormComponent implements OnInit {

    @Input() newObj3Dtype: ComponentMenuType.IndexedDEMOstageParams;

    // @Input() newObj3DParams: ComponentMenuType.New3DObject;
    //
    // tslint:disable-next-line: max-line-length
    customObj3D: any; // ComponentMenuType.New3DObject;    // 'New3DObject', since it's an Union, will throw TS errors (non existent props) on the Template...
    @Input('newObj3DParams') set setCustomObj3D(obj3DParams: ComponentMenuType.New3DObject)  {
        this.customObj3D = obj3DParams;
    }

    @Output() newObj3DToCreate = new EventEmitter<ComponentMenuType.New3DObject>();

    constructor() { }

    ngOnInit() {
    }

    newObj3DCoord(thisNewCoord: ComponentMenuType.Coordinate) {
        this.customObj3D.position[thisNewCoord.axis] = thisNewCoord.value;
    }

    newObj3DRotationCoord(thisNewRotationCoord: ComponentMenuType.Coordinate) {
        this.customObj3D.rotation[thisNewRotationCoord.axis] = thisNewRotationCoord.value;
    }

    newObj3DEmittanceColor(value: string) {
        const hexColorNumber: number = parseInt(value.replace('#', '0x'), 16);

        // console.error('Emittance Color param:', value, hexColorNumber, `"#${hexColorNumber.toString(16)}"`);
        this.customObj3D.emittance.color = hexColorNumber;
        // console.error(this.customObj3D);
    }

    newObj3DEmittanceIntensity(incomingValue: number) {
        this.customObj3D.emittance.intensity = incomingValue;
    }

    newObj3DEmittanceDistance(incomingValue: number) {
        this.customObj3D.emittance.distance = incomingValue;
    }

    addNewCustomObj3DToTheStage() {
        this.newObj3DToCreate.emit(Object.assign({}, this.customObj3D));
    }

}
