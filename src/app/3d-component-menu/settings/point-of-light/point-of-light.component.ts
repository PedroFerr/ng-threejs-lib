import {
    Component, OnInit, ElementRef,  ChangeDetectionStrategy, Input, Output, EventEmitter,
    OnChanges, SimpleChanges, SimpleChange } from '@angular/core';

import * as ComponentMenuType from '../../component-menu.interfaces';

@Component({
    selector: 'app-point-of-light',
    templateUrl: './point-of-light.component.html',
    styleUrls: ['../_common/settings-children-components.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PointOfLightComponent implements OnInit, OnChanges {

    componentDOM: HTMLElement;
    buttonAddPoL: HTMLElement;

    triggerLoadingWheel = false;

    newObj3DtypeToCreate: ComponentMenuType.IndexedDEMOstageParams = { param: 'pol', idx: 0 };
    newObj3DParamsToCreate: ComponentMenuType.PointOfLight = {
        name: `${ComponentMenuType.UserNewObjNamePfx} Point of Light`,
        position: {x: 0, y: 0, z: 0},
        emittance: {
            color: 0xff0000,
            intensity: 1,
            distance: 10000
        }
    };
    newObjNamePfx: string = ComponentMenuType.UserNewObjNamePfx;
    // Having upper const, we can define what's the first look of the new Point Of Light form, for User to customize:
    newUserPointOfLightSettings: ComponentMenuType.PointOfLight;

    @Input() demoName: string;
    // Get the current Stage parameters values sent to ThreeJS rendering, from the DEMO Component being rendered:
    // @Input() pointsOfLightParams: Array<ComponentMenuType.PointOfLight>;
    //
    pointsOfLight: Array<ComponentMenuType.PointOfLight>;
    @Input('pointsOfLightParams') set setPointsOfLight(pointsOfLightParams: Array<ComponentMenuType.PointOfLight>) {
        this.pointsOfLight = pointsOfLightParams;
    }
    // And the eventual User created 3D Object, added dynamically into the current DEMO's 3D Stage:
    @Input() createdPointOfLight: ComponentMenuType.PointOfLight;


    // On the same way, send back to the DEMO Component being rendered, the values User is fiddling around on each <input />:
    @Output() sendPLNewTranslatePosCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendPLNewColor = new EventEmitter<ComponentMenuType.IndexedValue>();
    @Output() sendPLNewIntensity = new EventEmitter<ComponentMenuType.IndexedValue>();
    @Output() sendPLNewDistance = new EventEmitter<ComponentMenuType.IndexedValue>();

    @Output() addNewPointOfLight = new EventEmitter<ComponentMenuType.PointOfLight>();
    newPointOfLight: ComponentMenuType.PointOfLight = null;

    @Output() removePointOfLight = new EventEmitter<string>();

    @Output() resetToInitValues = new EventEmitter<string>();

    constructor(private hostElement: ElementRef) { }

    ngOnInit() {
        this.componentDOM = this.hostElement.nativeElement;
        this.buttonAddPoL = this.componentDOM.querySelector('button[data-target="#newPointOfLightForm"]');
        this.userPointOfLightSettingsInit();
    }

    ngOnChanges(propsChanged: SimpleChanges) {
        const createdPointOfLightChgs: SimpleChange = propsChanged.createdPointOfLight;

        if (createdPointOfLightChgs) {
            this.triggerLoadingWheel = false;
            // setTimeout(() => this.triggerLoadingWheel = false, 2000 );

            if (this.newPointOfLight) {  // <= an User New Point of Light has just been created a little while ago - add its ThreeJS 'uuid':
                this.buttonAddPoL.click();
                this.newPointOfLight.uuid = createdPointOfLightChgs.currentValue.uuid;

                // Finally we can add it to the current settings panel - point of light Tab - array of Points of Light:
                this.pointsOfLight.push(Object.assign({}, this.newPointOfLight));

                // And prepare for a new User possible one
                this.newPointOfLight = null;
                this.userPointOfLightSettingsInit();
            }

        }
    }

    /**
     * In case user gets "lost", and rendering goes to some white/black spots,
     * he/she can reset the all Stage to the initial coordinates.
     *
     * Necessary operations should be made at the parent Component - send back the trigger.
     *
     * @param on - string identifying wich ThreeJS Stage Obj is to revert params from
     */
    resetStage(on: string) {
        if ( window.confirm('Are you sure\nyou want to reset this Obj LIGHT params to init values...?')) {
            this.resetToInitValues.emit(on);
        }
    }

    userNewPointOfLight(obj3DToCreate: ComponentMenuType.New3DObject) {
        this.newPointOfLight = obj3DToCreate as ComponentMenuType.PointOfLight;

        this.triggerLoadingWheel = true;
        this.addNewPointOfLight.emit(Object.assign({} , this.newPointOfLight));
    }

    /**
     * Be careful on "just" removing an element - any other variables referring to the original array item
     * will NOT be removed!
     * Check this good discussion: https://stackoverflow.com/a/15287938/2816279
     * And this one, cncerning how to "empty" an Array: https://stackoverflow.com/a/1232046/2816279
     *
     * @param obj3DToRemoveUuid - the ThreeJS 'uuid' of the PoL User wants to see removed form the current DEMO's Stage
     */
    removePoL(obj3DToRemoveUuid: string) {
        const confirmMsg = 'Are you sure\nyou want to REMOVE from the current DEMO\'s 3D Stage\nTHIS Point Of Light added by you...?';
        const userIsConfirmingRemoval: boolean = window.confirm(confirmMsg);

        if (userIsConfirmingRemoval) {
            // First remove it from our Array of current Points of Light:
            const pointsOfLightReduced: Array<ComponentMenuType.PointOfLight> =
                this.pointsOfLight.filter(
                    (pol: ComponentMenuType.PointOfLight) => pol.uuid === undefined || pol.uuid !== obj3DToRemoveUuid
                )
            ;
            this.pointsOfLight.length = 0;
            pointsOfLightReduced.forEach((pol: ComponentMenuType.PointOfLight) => this.pointsOfLight.push(pol));

            // And send order back to the current DEMO's 3D Stage - down to 'src\app\3d-components-demo\_common\object3D-demo.directive.ts':
            this.removePointOfLight.emit(obj3DToRemoveUuid);
        }
    }

    /**
     * Init, with a deep copy, the <app-new-3d-object-form /> new PoL form values,
     * from an Object const ('newObj3DParamsToCreate')
     */
    private userPointOfLightSettingsInit() {
        this.newUserPointOfLightSettings = Object.assign({}, this.newObj3DParamsToCreate,
            {position: Object.assign({}, this.newObj3DParamsToCreate.position)},
            {emittance: Object.assign({}, this.newObj3DParamsToCreate.emittance)}
        );
    }
}
