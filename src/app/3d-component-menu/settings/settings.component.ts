import {
    Component, OnInit, ChangeDetectionStrategy,
    Input, Output, EventEmitter, AfterViewInit, OnDestroy
} from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import {
    CameraDirective, SceneComponent, RendererService, LoggerService, LoggEventData,
    EventData, SomeNgChanges
} from 'ng-threejs-lib';

import * as ComponentMenuType from '../component-menu.interfaces';


@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss'],
    // changeDetection: ChangeDetectionStrategy.OnPush, // Optimize performance - component will only RE-mount IF some @Input() has CHANGES.
    // Unfortunetely we can NOT also have it...
    //
    // See explanation on PARENT's 'component-menu'.
})
export class SettingsComponent implements OnInit, AfterViewInit, OnDestroy {

    componentDOM: HTMLElement;

    @Input() demoName: string;
    @Input() stageParamsFile: string;

    scene: SceneComponent;
    camera: CameraDirective<any>;

    // NgRxJS stream of added logg events, through LoggerService, all over the App:
    loggEvent$: Observable<LoggEventData | string>;
    loggSubscription: Subscription;
    // ... and the settings for the SONY Monitor debug screen:
    monitorMarginTop: string; // = 'calc(50vh + 2rem)';

    // Get the current Stage parameters values sent to ThreeJS rendering, from the DEMO Component being rendered:
    @Input() stageCamera: ComponentMenuType.Camera;
    @Input() cameraPositionChanged: SomeNgChanges;
    // tslint:disable-next-line: max-line-length
    @Input() cameraRotationChanged = null as SomeNgChanges;  // <= for now, we shall NOT ever have CHANGES on rotation - see bottom's 'if (chgdPropOnCameraRotation) {}'
    addedNewStageCamera: ComponentMenuType.Camera;

    @Input() stageLights: Array<ComponentMenuType.PointOfLight>;
    addedNewStageLight: ComponentMenuType.PointOfLight;

    @Input() stageActor3D: ComponentMenuType.Actor3D;
    addedNewStageActor3D: ComponentMenuType.Actor3D;

    // And the eventual User created 3D Object, added dynamically into the current DEMO's 3D Stage:
    @Input('createdObj3D') set setNewStage3DObject(new3DObject: ComponentMenuType.New3DObject) {
        if(new3DObject !== undefined) {
            // tslint:disable-next-line: no-string-literal
            switch (new3DObject['type']) {
                case 'PointLight':
                    this.addedNewStageLight = new3DObject as ComponentMenuType.PointOfLight;
                    break;

                // More will be added here:
                case 'Mesh':
                    this.addedNewStageActor3D = new3DObject as ComponentMenuType.Actor3D;
                    break;

                case 'PerspectiveCamera':
                    this.addedNewStageCamera = new3DObject as ComponentMenuType.Camera;
                    break;

                default:
                    console.error('ERROR: some non identified 3D Object TYPE has been added to the current DEMO\'s 3D Stage...');
                    break;
            }
        }
    }


    // On the same way, send back to the DEMO Component being rendered, the values User is fiddling around on each <input />:
    // --------------------------------------------------------------
    @Output() sendCameraNewCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendCameraNewRotationCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendCameraMoveParam = new EventEmitter<ComponentMenuType.MoveParam>();

    @Output() sendCameraNewFOV = new EventEmitter<number>();
    @Output() sendCameraNewNEAR = new EventEmitter<number>();
    @Output() sendCameraNewFAR = new EventEmitter<number>();

    @Output() sendPLNewTranslatePosCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendPLNewColor = new EventEmitter<ComponentMenuType.IndexedValue>();
    @Output() sendPLNewIntensity = new EventEmitter<ComponentMenuType.IndexedValue>();
    @Output() sendPLNewDistance = new EventEmitter<ComponentMenuType.IndexedValue>();

    @Output() addNewPointOfLight = new EventEmitter<ComponentMenuType.PointOfLight>();
    @Output() removePointOfLight = new EventEmitter<string>();

    @Output() sendActorNewCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendActorNewRotationCoord = new EventEmitter<ComponentMenuType.Coordinate>();

    @Output() resetToInitValues = new EventEmitter<string>();

    // --------------------------------------------------------------

    @Output() angularLoaded = new EventEmitter();

    constructor(
        private ThreeJSrendererService: RendererService,
        private ThreeJSloggerService: LoggerService
    ) { }

    ngOnInit() {
        this.startListening3DloggedEvents();
    }

    ngAfterViewInit() {
        this.scene = this.ThreeJSrendererService.getScene();
        // console.log('ngAfterViewInit() => Scene on curent DEMO Stage..?', this.scene);
    }

    ngOnDestroy() {
        if (this.loggSubscription) {
            this.loggSubscription.unsubscribe();
        }
    }

    /**
     * Start listening our Logged 3D Events,
     * to fetch a lot of information of WHAT, WHEN and HOW everything is happening under the hood,
     * currently being rendered on our 3D Stage and all over the different App's states.
     */
    private startListening3DloggedEvents() {
        // The rendering of the logged msgs Service:
        this.loggEvent$ = this.ThreeJSloggerService.newEvtLogged$;

        this.loggSubscription = this.loggEvent$.subscribe((loggEvtObj: LoggEventData | string) => {

            // Check we're subscribing from the beginning, and not missing any sent msgs to the Child component:
            // console.log('$$$$$$$$ MENU DEMO new LOGG evt: ', loggEvtObj);

            const logEvtObjSTR = loggEvtObj as string;
            const logEvtObjOBJ = loggEvtObj as LoggEventData;
            const evtData: EventData = logEvtObjOBJ.data;

            if (evtData.hasOwnProperty('onWhat')) {
                this.camera = this.getCurrentCamera(evtData);
                this.sendCameraPosChangesToSettingsMoveCard(evtData);
            }
        });
    }

    /**
     * Get our 3D Camera,
     * from the Logged Events being catched on loggEvent$ Subject (@ '...lib\services\app-demo\logger.service.ts'),
     * being subscribed on tis Component
     *
     * @param data - the data content of the Logged Event
     */
    private getCurrentCamera(data: EventData) {
        if (data.where === 'CameraDirective' && data.operation === 'create') {
            return data.onWhat as CameraDirective<any>;
        }
    }

    /**
     * Get the Angular changes felt on our 3D Camera,
     * from the Logged Events being catched on loggEvent$ Subject (@ '...lib\services\app-demo\logger.service.ts'),
     * being subscribed on tis Component
     *
     * @param data - the data content of the Logged Event
     */
    private sendCameraPosChangesToSettingsMoveCard(data: EventData) {
        if (data.where === 'OrbitControlsDirective' && data.operation === 'changed') {
            const changedProps: Array<SomeNgChanges> = data.scalarValues;

            const chgdPropOnCameraPosition: SomeNgChanges = changedProps.filter((on: SomeNgChanges) => on.whatProp === 'cameraPosition')[0];
            const chgdPropOnCameraRotation: SomeNgChanges = changedProps.filter((on: SomeNgChanges) => on.whatProp === 'cameraRotation')[0];

            if (chgdPropOnCameraPosition) {
                this.cameraPositionChanged = chgdPropOnCameraPosition;
            }
            if (chgdPropOnCameraRotation) {

                if (chgdPropOnCameraRotation.fromValue !== null) {
                    console.error('ALERT!!!! We are counting that every ROTATION is a TRANSLATION move on 2 AXIS!!');
                    console.warn('!!!!!! We have HERE a ROTATION detected case !!!!!!');
                    console.error('Revise your code...');
                }

            }
        }
    }
}
