// ----------------------------------------------
// Name of DEMO's object, TS, HTML and API's MD Code,
// and the Initial Stage params file content, on each DEMO:

import { AxisType } from 'ng-threejs-lib';
// MAke it available from this 'interfaces.ts' Module:
export { AxisType } from 'ng-threejs-lib';

// ----------------------------------------------
export interface FilesCodeTransporter { name: string; TS: string; HTML: string; MDAPI: string; PARAMS: string; }

// ----------------------------------------------
// ThreeJS 3D Objects props on Stage
// ----------------------------------------------
export interface Camera {
    uuid?: string;
    name: string;
    position: Position3D;
    rotation: Position3D;
    movements?: {                    // Some DEMOs might no have an init all/parts 'movements' set up => Angular logic should assume default
        isLookingAtCenter?: boolean;
        axisUp?: AxisType;           // Related to ROTATION movements
    };

    frustum: Frustum;
}

export interface Position3D {
    x: number;
    y: number;
    z: number;
}

export interface Frustum {
    fov: number;
    near: number;
    far: number;
}

export interface PointOfLight {
    uuid?: string;    // <= only filled AFTER a new User Point Of Light has been created, by the User, on the ThreeJS
    // check 'startListening3DloggedEvents()' @ src\app\3d-components-demo\_common\object3D-demo.directive.ts
    name: string;
    position: Position3D;
    emittance: LightEmittance;
}

export interface LightEmittance {
    color: number;
    intensity: number;
    distance: number;
}

export interface Actor3D {
    uuid?: string;
    name: string;
    position: Position3D;
    rotation: Position3D;
}

export type DEMOstageParams = Camera | Array<PointOfLight> | Actor3D;
// And when you have severals, comes handy an Index:
type DEMOstageParamsID = 'camera' | 'pol' | 'actor';
export interface IndexedDEMOstageParams {
    param: DEMOstageParamsID;
    idx: number;
    uuid?: string;    // <= only filled AFTER a new User Point Of Light has been created, by the User, on the ThreeJS
}


// ----------------------------------------------
// @Output emitters Type
// ----------------------------------------------

// Single Objects on the Scene will NOT have 'idx'
// 'Point Of Light's, if more than one, MUST have the (HTML order) idx of the PoL.
export interface Coordinate {
    axis: string;
    value: number;
    idx?: number;
    uuid?: string;    // <= only filled AFTER a new User Point Of Light has been created, by the User, on the ThreeJS
}

// There's other parameters to send back than Coordinate, either for Translation or Rotation, that also relate to Movement/Position:
// i.e. 'isLookingAtCenter' for Translation, 'axisUp' for Rotation, etc.
export interface MoveParam {
    param: string;
    value: AxisType | boolean;    // Add more Types, if needed...
}


// A value sent back, sometimes need to be indexed to the Obj element that wants to modify:
// (if it doesn't, it's a simple 'number | string' value)
// Or belonging to a dynamic user added 3D Object (a light, a camera, an actor, etc.),
// on which MUST exist a ThreeJS 'uuid' returned on the creation event - will "replace" the 'idx' index
export interface IndexedValue {
    value: number | string;
    idx: number;
    uuid?: string;    // <= only filled AFTER a new User Point Of Light has been created, by the User, on the ThreeJS
}

// ----------------------------------------------
// ThreeJS 3D Objects props created dynamically, by the User
// ----------------------------------------------
export const UserNewObjNamePfx = 'User added';
// Any 3D Object can be dynmically created/added into the current DEMO's 3D Stage
// - a Camera, a Point Of LIght, a 3D Actor, a Mesh type 3d Obj, an OBJ file Model, etc., etc.
export type New3DObject = Actor3D | Camera | PointOfLight | undefined;
