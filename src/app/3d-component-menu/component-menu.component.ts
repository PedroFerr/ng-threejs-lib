import {
    ChangeDetectionStrategy, Component, OnInit, Input,  Output, EventEmitter,
    OnChanges, SimpleChanges, ElementRef, Renderer2, AfterViewInit
} from '@angular/core';
import { Title } from '@angular/platform-browser';

import { ActivatedRoute } from '@angular/router';

import * as ComponentMenuType from './component-menu.interfaces';

import { RendererService, CookieHandlerService, VpBtnValueType } from 'ng-threejs-lib';

// Dependencies to highlight code "strings" in different coding languages:
// HTML and TS:
import Prism from 'prismjs';
import 'prismjs/components/prism-typescript';
// API's 'md' markdown type, that it's transformed into HTML
import convertToHTML from 'markdown-to-html-converter';

// ----------------------------------------------------------------------

// It's a CSS variable that we transpone to JS logic - don't forget to change it here IF you change it on SCSS!
const GENERAL_PADDING = '1rem';

// The CSS 'height' value logic of these const LABELS
// are always related to the '.visualizer-3d' container - '.tab-content' will subtract to the '100vh' value.
// 'maximize' is an exception, since we 'display: none' the other '.component-menu-container' half part.
const V_CSS_HEIGHTS = {
    visualizer3D: {
        maximize:   `calc(100vh - 2 * ${GENERAL_PADDING})`,
        original:   `calc(75vh - ${GENERAL_PADDING})`,
        halfHeight: `calc(50vh - ${GENERAL_PADDING})`,
        minimize:   `calc(16vh - ${GENERAL_PADDING})`,
    },
    menuTabCodeContent: {
        maximize: '',
        original:   `calc(25vh - ${GENERAL_PADDING} - 1px - 2 * ${GENERAL_PADDING} - 1px - ${GENERAL_PADDING})`,
        halfHeight: `calc(50vh - ${GENERAL_PADDING} - 1px - 2 * ${GENERAL_PADDING} - 1px - ${GENERAL_PADDING})`,
        minimize:   `calc(84vh - ${GENERAL_PADDING} - 1px - 2 * ${GENERAL_PADDING} - 1px - ${GENERAL_PADDING})`,
    }
};

// ----------------------------------------------------------------------

@Component({
    selector: 'app-component-menu',
    templateUrl: './component-menu.component.html',
    styleUrls: ['./component-menu.component.scss'],
    // changeDetection: ChangeDetectionStrategy.OnPush, // Optimize performance - component will only RE-mount IF some @Input() has CHANGES.
    // Unfortunetely we can NOT have it...
    //
    // As this is a PARENT of 'settings' component, where HEAVY DOM manipulation is going to happen on <ng3d-monitor-debug />,
    // markedup on its Template's HTML, if, HERE, we STOP chamge detection on ANY var other than @Input() ones,
    // on 'monitor-debug' all Event Listeners dynamic manipulation will NOT be detected IF user changes TABS @ 'settings' component.
    //
    // Only if user STAYS on the '.tab-pane' where 'id="monitor"', the listeners on 'monitor-debug's method 'doPrintMonitorData()'
    // will be effectively attached to each button whenever a NEW line of a logged event renders on the monitor's screen.
    // As soon as User switches to any other tab (camera, light, actor, etc.) and, from there, triggers a NEW Event Logg...
    // ... pumba! Listeners will NOT attach anymore to the buttons - Angular does not see that as a DOM change, which in fact is.
})
export class ComponentMenuComponent implements OnInit, OnChanges, AfterViewInit {

    // Some DOM elements we need to grab:
    componentDOM: HTMLElement;

    componentName: HTMLElement;

    componentMenuContainer: HTMLElement;
    componentMenuTabNav: HTMLElement;
    componentMenuTabContent: HTMLElement;
    viewportMaxStateControllerMenu: HTMLElement;

    visualizer3D: HTMLElement;
    canvas3D: HTMLCanvasElement;
    isVisualizer3Dmaximize = false;
    isToActivateBtn = 1;
    // Make viewport height control... SMOOTH!
    // 400 ms is the time for a lot of things: JS setTimeout()s and CSS transitions on height, linear:
    timeToViewportHeightChanges = 400;

    // Control which is the '.tab-content' active button/tab to render, on loading:
    buttonSelected: 0 | 1 | 2 | 3 | 4 = 0; // 0;

    // The name of the current demoing <ng3d-xxxx /> component being visualized
    title: string;

    // The 3 code files (HTML, TS and API's Markdown), of the current demoing <ng3d-xxxx /> component being visualized
    @Input() lggCodeObj: { name: string; TS: string; HTML: string; MDAPI: string, PARAMS: string };
    // tslint:disable: variable-name
    getPrism_HTML_content: string;
    getPrism_TS_content: string;
    // API content beautifier (on 'this.lggCodeObj.MDAPI') is NOT being highlighted by "Prism" - it's already done!
    // We have used 'html-loader' and 'markdown-loader', directly on the 'ts-html-api-code' construction.
    // We now just have to add a couple of extra CSS lines on this Component's SCSS and at 'src\assets\css\prism-okaidia.css'.
    //
    // BUT, don't know why - I think security reasons, as Angular has envolved - the styling behaviour is NOT what used to be,
    // when we discover this markup dependencies still with Angular 6 (on the UI/UX lib of components).
    //
    // So... we are currently using single 'raw-loader' dependency, on the 'ts-html-api-code' construction, to fectch entire '.md' content.
    // Then, here, instead of 'Prism' we are using 'markdown-to-html-converter' dependency: "$ npm i markdown-to-html-converter".
    // You have a cool playground at https://markdown-to-html-demo.herokuapp.com/
    // so you can "test" visually everything there and then paste the 'md' code, when satisfied,
    // into any of the 'API.md' files of each 3D Component!
    //
    // See bottom of 'src\assets\css\prism-okaidia.css' for any extra styling - BS4 should handle it, by default, with no problems...
    get_API_content: string;
    get_PARAMS_content: string;

    // Get the current Stage parameters values sent to ThreeJS rendering, from the DEMO Component being rendered:
    @Input() currentStageCamera: ComponentMenuType.Camera;
    @Input() currentStageLights: Array<ComponentMenuType.PointOfLight>;
    @Input() currentStageActor3D: ComponentMenuType.Actor3D;
    // And the eventual User created 3D Object, added dynamically into the current DEMO's 3D Stage:
    @Input() createdObj3D: ComponentMenuType.New3DObject;

    // On the same way, send back to the DEMO Component being rendered, the values User is fiddling around on each <input />:
    @Output() sendCameraNewCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendCameraNewRotationCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendCameraMoveParam = new EventEmitter<ComponentMenuType.MoveParam>();
    @Output() sendCameraNewFOV = new EventEmitter<number>();
    @Output() sendCameraNewNEAR = new EventEmitter<number>();
    @Output() sendCameraNewFAR = new EventEmitter<number>();

    @Output() sendPLNewTranslatePosCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendPLNewColor = new EventEmitter<ComponentMenuType.IndexedValue>();
    @Output() sendPLNewIntensity = new EventEmitter<ComponentMenuType.IndexedValue>();
    @Output() sendPLNewDistance = new EventEmitter<ComponentMenuType.IndexedValue>();

    @Output() addNewPointOfLight = new EventEmitter<ComponentMenuType.PointOfLight>();
    @Output() removePointOfLight = new EventEmitter<string>();

    @Output() sendActorNewCoord = new EventEmitter<ComponentMenuType.Coordinate>();
    @Output() sendActorNewRotationCoord = new EventEmitter<ComponentMenuType.Coordinate>();

    @Output() resetToInitValues = new EventEmitter<string>();

    @Output() angularHasLoaded = new EventEmitter();

    constructor(
        private browserTitleService: Title,
        private hostElement: ElementRef,
        private renderer: Renderer2,

        private ThreeJSrendererService: RendererService,
        private cookieHandlerService: CookieHandlerService,

        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        // console.warn('this.lggCodeObj', this.lggCodeObj);
        this.setBrowserTabTitle(this.title);
        this.componentDOM = this.hostElement.nativeElement;

        // ===================================================================================================
        // this.startColectingLoggedEvents();
        // ===================================================================================================
        // Angular is throwing an ERROR: 'ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked...'
        // As it affects directly the HTML template (vars), we have move code to ngAfterViewInit()
        // ===================================================================================================
    }

    ngOnChanges(propsChanged: SimpleChanges) {
        // console.error('propsChanged:', propsChanged);

        const lggCodeObjChanged = propsChanged.lggCodeObj;

        if (lggCodeObjChanged /*&& !lggCodeObjChanged.firstChange*/) {
            this.title = this.lggCodeObj.name;

            this.getPrism_HTML_content = Prism.highlight(this.lggCodeObj.HTML, Prism.languages.markup);
            this.getPrism_TS_content = Prism.highlight(this.lggCodeObj.TS, Prism.languages.typescript);
            this.get_API_content = convertToHTML(this.purgeChars(this.lggCodeObj.MDAPI));

            this.get_PARAMS_content = Prism.highlight(this.lggCodeObj.PARAMS, Prism.languages.typescript);
        }
    }

    ngAfterViewInit() {
        this.componentName = this.componentDOM ? this.componentDOM.querySelector('.component-name') : null;

        this.componentMenuContainer = this.componentDOM ? this.componentDOM.querySelector('.component-menu-container') : null;
        this.componentMenuTabNav = this.componentMenuContainer ? this.componentMenuContainer.querySelector('#v-pills-tab') : null;
        this.componentMenuTabContent = this.componentMenuContainer ? this.componentMenuContainer.querySelector('.tab-content') : null;

        this.visualizer3D = document.querySelector('.visualizer-3d');
        this.canvas3D = this.visualizer3D ? this.visualizer3D.querySelector('canvas') : null;

        this.viewportMaxStateControllerMenu = this.componentDOM ?
            this.componentDOM.querySelector('ng3d-viewport-controler-menu.if-visualizer-3d-maximized')
            :
            null
        ;

        this.setInitStyle();
        this.viewportSizeInit();
        this.codeTabInit();
    }

    // -------------------------------------------------------------------

    maximize3D() {
        this.isVisualizer3Dmaximize = true;
        this.isToActivateBtn = 0;
        this.resizeHTMLElement(this.visualizer3D, V_CSS_HEIGHTS.visualizer3D.maximize);
        setTimeout(() => {
            this.renderer.setStyle(this.componentMenuContainer, 'display', `none`);
            this.renderer.setStyle(this.viewportMaxStateControllerMenu, 'opacity', '1');
        }, this.timeToViewportHeightChanges);
    }

    /**
     * Set this.isVisualizer3Dmaximize to 'false' (does the toggling between the 2 existent viewport button's bars)
     * We then set which button is to be '.active', on the bar that has just been visible at the browser.
     *
     * Then, and only then, we can switch view's proportional heights to 100vh,
     * coresponding to what size (index) did the user clicked to go to, activating that button on the (toggled) bar.
     * Do not forget to unhide the '.component-menu-container' (where is the code printing and the 3D component's setting)
     *
     * The switching is done by setting the '.visualizer-3d' container height accordingly, along with the <canvas /> content
     * (where the currently 3d Component, from our 3D library, is currently being rendered),
     * spliting, so, the viewport vertically to the pretended (related) heights division : 'halfHeight', 'minimize' or 'original' height.
     *
     * Mind you that 'halfHeight', 'minimize' or 'original' labels are always related to the '.visualizer-3d' height.
     *
     * @param whatSize - a string, that will correspond a CSS selector 'height', indexing a @ 'V_CSS_HEIGHTS' const
     */
    resize3Dto(whatSize: string) {
        // 1st things first:
        if (this.isVisualizer3Dmaximize === true) {
            this.renderer.setStyle(this.viewportMaxStateControllerMenu, 'opacity', '0');
            this.isVisualizer3Dmaximize = false;
            this.renderer.setStyle(this.componentMenuContainer, 'display', `block`);
        }

        switch (whatSize) {
            case 'minimize':
                this.isToActivateBtn = 3;
                break;

            case 'halfHeight':
                this.isToActivateBtn = 2;
                break;

            case 'original':
                this.isToActivateBtn = 1;
                break;

            default:
                break;
        }

        this.resizeHTMLElement(this.visualizer3D, V_CSS_HEIGHTS.visualizer3D[whatSize]);
        this.resizeHTMLElement(this.componentMenuTabContent, V_CSS_HEIGHTS.menuTabCodeContent[whatSize]);
    }

    // -------------------------------------------------------------------

    private setBrowserTabTitle( newTitle: string) {
        this.browserTitleService.setTitle( newTitle );
    }

    /**
     * Operate on browser's UI, once  this 'component-menu' Component
     * completely loads on DOM
     */
    private setInitStyle() {

        // 1) the 3d component's name should be '.lowered', with some CSS animation:
        if (this.componentName) {
            this.renderer.addClass(this.componentName, 'lowered');
        }

        // 2) '.visualizer-3d' can have its 'loading-wheel.gif' stopped (deleted):
        // console.error('---------------------------------- Loaded...?');
        if (this.visualizer3D) {
            this.renderer.addClass(this.visualizer3D, 'has-loaded');
        }
    }

    /**
     * Very strange the scrolY bar, appearing 'onLoading' aparentely meaning some height miss calculation was done...
     * But it isn't!
     * I'm so sure of that... if you click the "restore" (viewport size to the default, natural, onLoading values) button
     * (75vh for '.visualizer-3d', less padding-top, and 25vh for '.component-menu-container', less padding-bottom)
     * the scrollY bar... just disappears!
     * So why does it appears... for the exact SAME values... onLoading...?
     * Will the CSS and JS values be read by the browser with different understandings? Timmings? Is it something to do with Angular...?
     * Just don't know...
     *
     * And, to solve it, we do exactly that: we click on the 'restore' viewport control btn.... as soon as DOM settles down.
     *
     * WAIT!
     * Since this is a property, adjustable by the user mainly because of the device measures he/she is having to surf the Web,
     * we better leave it "selected" while user travels from DEMO to DEMO component, of the chosen 3D Component to render.
     * As so, we now are memorizing which viewport button the user has clicked on, at the browser's localStorage.
     *
     * So just click on the default IF localStorage is still empty (1st loading of all times)
     * If it's the case, you don't need to bother seting it on the localStorage - it's the click() event that changes the vpBtnCookie value.
     *
     * MIND YOU:
     * ------------------------------------------------------------------------
     * There's certainly MORE then ONE bar of buttons to control viewport size!
     * ------------------------------------------------------------------------
     * So to select "A" button, you can NOT go for the result index (of the array collection it belongs) of the JS query over DOM.
     * - i,e. if, when you query the buttons, exist 4 buttons on the component's Template of the library (<ng3d-viewport-controler-menu />),
     * if the App has 2 bars of these... you'll get 8 buttons! ;-)
     * You have to go by DATA-TAGs and then click on (each of) the "collection" (with same data-tag) you've found...
     * And then pick up the FIRST, or the SECOND found "same" buttons, cheking 'this.isVisualizer3Dmaximize'!
     *
     */
    private viewportSizeInit() {
        const vpBtnCookie = this.cookieHandlerService.getCookieProp('vpBtn') as VpBtnValueType;
        const menuResizeBtn: NodeListOf<Element> = document.querySelectorAll(`[data-function=${vpBtnCookie ? vpBtnCookie  : 'restore'}]`);
        // MIND YOU upper does NOT belong anymore to THIS component - it's a 2D library's component, multi used in all DEMO's components.
        // That's why we've queried 'document' and not 'this.componentMenuTabNav'...

        // The click() must be sync!
        // We have to make sure ALL operations on DOM have finished - and they are not that few, once working with 3D transitions...
        // menuResizeBtn.forEach( (btn: HTMLElement) => setTimeout(() => btn.click(), 0) );
        //
        // More than that! Now we have some animation - which we didn't had before...
        // The localStorage btn might be one different than the 'restore' one => CSS animation.
        // More than sync, needs to be done AFTER the CSS transition (time) eventually pulls up/down the '.component-menu-content'
        const timeToClick: number = vpBtnCookie && vpBtnCookie !== 'restore' ? this.timeToViewportHeightChanges : 0;
        menuResizeBtn.forEach(
            (btn: HTMLElement, idx: number) => setTimeout(() => {
                if (!this.isVisualizer3Dmaximize && idx === 1 || this.isVisualizer3Dmaximize && idx === 0) {
                    // ---------------------------------------------------------------------------------------
                    btn.click(); // Will also set the 'new' cookie, or value of the existent, on local storage
                    // ---------------------------------------------------------------------------------------
                    console.error('Sorry! 2nd component\'s Menu height resize click(), to overcome a BUG...');
                }
            }, timeToClick)
        );
    }

    /**
     * A cool feature we are implementing is controlling the code tab to see, from an outside (page) link:
     */
    private codeTabInit() {
        const codeTabToSelect: string = this.route.snapshot.queryParamMap.get('code') as 'settings' | 'html' | 'ts' | 'api';
        const tabSelectors: NodeListOf<Element> = this.componentMenuTabNav.querySelectorAll('[data-toggle]');

        const elemToClick = [].slice.call(tabSelectors).filter((btn: HTMLElement) => btn.id === `v-pills-${codeTabToSelect}-tab`)[0];
        if (elemToClick) {
            elemToClick.click();
        }
    }

    /**
     * Get ride of some chars (that are HTML significative) but pretty handy to use on this 3D library of components.
     * I.e., on the original '.md' file, documenting the API of any 3D Component, it's obvious that the chars
     * '<' and '>' will be used quite often - to describe references to the component being presented (i.e. '<ng3d-sphere-mesh />')
     *
     * So in order to not interfeer with the (native) code tags, once to render on this component's HTML Tabs,
     * we must, previously to render this component's Template, purge them.
     */
    private purgeChars(strToPurge: string) {
        const purgedString: string = strToPurge
            .replace(/</g, '&lt;').replace(/>/g, '&gt;')
            .replace(/&lt;/g, '<code>&lt;').replace(/&gt;/g, '&gt;</code>')

        ;
        return purgedString;
    }

    /**
     * Sets new 'height' values for 'what' HTML element.
     *
     * If 'what' is the <canvas /> container (section '.visualizer-3d'),
     * where the currently 3d Component, from our 3D library, is being rendered, also resize it, using the ThreeJS Service for the effect.
     *
     * MIND YOU:
     * All 'height' values (because they are strings containing CSS selectors values) are already defined @ 'V_CSS_HEIGHTS' const Obj
     *
     * @param what - the HTML element we want to impose a specific 'height'
     * @param toCSSsize - the CSS selector 'height' string, defined @ 'V_CSS_HEIGHTS' const Obj
     */
    private resizeHTMLElement(what: HTMLElement, toCSSsize: string) {
        if (what) {
            this.renderer.setStyle(what, 'height', toCSSsize);

            if (what.classList.contains('visualizer-3d')) {
                // this.ThreeJSrendererService.resize(this.canvas3D, '100%')

                // Wait for the '$timeToViewportHeightChanges' time for 'height' CSS transition to finish:
                this.renderer.setStyle(this.visualizer3D, 'opacity', '0');
                setTimeout(() => {
                    this.renderer.setStyle(this.visualizer3D, 'opacity', '1');
                    this.ThreeJSrendererService.resize(this.canvas3D, '100%');
                }, this.timeToViewportHeightChanges + 300); // <= we reeeeally need to give DOM some time to settle down... it's 3D!
                // this.ThreeJSrendererService.resize(this.canvas3D, '100%') should ONLY be called
                // when we have 100% sure 'canvas' container has reached its FINAL height position - going up or down.
            }
        }
    }

}
