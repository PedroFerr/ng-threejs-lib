import { Component, ElementRef, OnInit, AfterViewInit, Renderer2 } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

    title = 'Angular Library of 3D components, using ThreeJS API';

    componentDOM: HTMLElement;
    logo3Dobj: HTMLElement;

    constructor(
        private browserTitleService: Title,
        private hostElement: ElementRef,
        private renderer: Renderer2
    ) {}

    ngOnInit() {
        this.browserTitleService.setTitle( this.title );
        this.componentDOM = this.hostElement.nativeElement;

        // Start with left side menu OPEN ...? Not so cool on mobiles... I think!
        // this.toggleMenu();
    }

    ngAfterViewInit() {
        this.logo3Dobj = this.componentDOM.querySelector('.logo');

        setTimeout(() => {
            this.renderer.addClass(this.logo3Dobj, 'lowered');
            this.doAnimationOnBackLighting();
        }, 0);

    }

    public toggleMenu(evt?: KeyboardEvent) {
        // console.warn(evt.target, evt.currentTarget, ' ... was clicked!');
        const wrapper: HTMLElement = this.componentDOM.querySelector('#wrapper');
        wrapper.classList.toggle('toggled');
    }

    /**
     * 3 layers give  to the '.logo' 3D block the neede 'z-axis' depth (the far back, the less blur).
     * As 'text-shadow' can not be smoothly 'transition'ed, on '.lowered' class of the '.logo' block,
     * we draw this 3 layers with JS, with 3 increments of 2s, with setTimout()!
     * MIND YOU that (3 x 2s) is the exacttime the '::after' front shade of the '.logo' Obj takes to slide to final position
     *
     * Well... it's not linear;!
     * Mind you we have an "easing" pattern going on: ease/ease-out/ease-in, etc.,
     * which makes it much faster at the beginning/end of the 6 secs than at the end/begining... add a factor to adjust
     */
    private doAnimationOnBackLighting() {
        // Timings:
        const tStartLogoMOv = 1000;
        const tLogoMov = 800;
        const tStartFrontShadowMov = 600;    // Starts 0,4s BEFORE the 3D Logo block movement.
        const tFrontShadowMov = 3000;
        // $primary color = #a10d31 = hsl(345, 85%, 34%)
        // $secondary color = #B0A288 = hsl(39, 20%, 61%)

        // Shades
        // To beter follow up, see the commented CSS selectors, at the SCSS file - that's all we're doing; bringing them to here
        const uniformizationLayer = `
            1px -1px 0px rgba(255, 255, 255, 0.1),
            0px -1px 5px rgba(255, 255, 255, 0.4),
            -1px -1px 1px rgba(255, 255, 255, 0.9)
        `;    // last text-shadow gives some 3D to the face of the block - the higher the last 'a' parameter, the "whiter"
        // Color and first layer with this color:
        const firstLayerColor = 'hsl(345, 85%, 34%)';
        const firstLayer = `
            0px -2px 3px ${firstLayerColor},
            -1px -2px 3px ${firstLayerColor},
            -2px -2px 3px ${firstLayerColor},
            -1px -3px 2px ${firstLayerColor}
        `;
        // And so:
        const firstFrame = `${uniformizationLayer}, ${firstLayer}`;

        const secondLayerColor = 'hsl(345, 85%, 34%)';
        const secondLayer = `
            -2px -3px 2px ${secondLayerColor},
            -3px -3px 2px ${secondLayerColor},
            -2px -4px 2px ${secondLayerColor},
            -3px -4px 2px ${secondLayerColor}
        `;
        const secondFrame = `${firstFrame}, ${secondLayer}`;

        const thirdLayerMainColor = 'hsl(345, 85%, 34%)';
        const thirdLayerSecondColor = 'hsl(345, 85%, 34%)';
        const thirdLayer = `
            -4px -4px 2px ${thirdLayerMainColor},
            -3px -5px 2px ${thirdLayerMainColor},
            -4px -5px 1px ${thirdLayerMainColor},
            -5px -5px 1px ${thirdLayerMainColor}
        `;
        const thirdFrame = `${secondFrame}, ${thirdLayer}`;


        setTimeout(() => {
            // this.renderer.setStyle(this.logo3Dobj, 'color', `black`);
            this.renderer.setStyle(this.logo3Dobj, 'text-shadow', firstFrame);

            setTimeout(() => {
                this.renderer.setStyle(this.logo3Dobj, 'text-shadow', secondFrame);
                setTimeout(() => {
                    this.renderer.setStyle(this.logo3Dobj, 'text-shadow', thirdFrame);
                }, 200);
            }, 200);

        }, (tStartLogoMOv + tLogoMov) * 0.8);    // <= add the non linear 'easing' pattern, to adjust
    }

}
