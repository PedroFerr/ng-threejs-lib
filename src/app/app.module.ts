import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SideMenuComponent } from './views/side-menu/side-menu.component';
import { HomeComponent } from './views/home/home.component';
import { AboutComponent } from './views/about/about.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found.component';

import { AppRoutingModule } from './app.routing';

// UI/UX 3D demo Components:
// ------------------------------------------------------------------------------
import { ComponentMenuModule } from './3d-component-menu/component-menu.module';
// ------------------------------------------------------------------------------
import { EmptyStageDemoComponent } from './3d-components-demo/0.0-empty-stage-demo/empty-stage-demo.component';

import { Object3DDemoDirective } from './3d-components-demo/_common/object3D-demo.directive';
import { SphereDemoComponent } from './3d-components-demo/1.0-sphere-demo/sphere-demo.component';
import { CubeDemoComponent } from './3d-components-demo/1.1-cube-demo/cube-demo.component';
import { ConicalCylinderDemoComponent } from './3d-components-demo/1.2-conical-cylinder-demo/conical-cylinder-demo.component';
import { TorusDemoComponent } from './3d-components-demo/1.3-torus-demo/torus-demo.component';

import { ObjLoaderDemoComponent } from './3d-components-demo/2.0-office-3-floor/obj-loader-demo.component';
import { ObjStreamLoaderDemoComponent } from './3d-components-demo/3.0-obj-stream-loader-demo/obj-stream-loader-demo.component';
// ------------------------------------------------------------------------------

@NgModule({
   declarations: [
      AppComponent,
      HomeComponent,
      SideMenuComponent,
      AboutComponent,
      PageNotFoundComponent,

      Object3DDemoDirective,
      EmptyStageDemoComponent,
      SphereDemoComponent,
      CubeDemoComponent,
      ConicalCylinderDemoComponent,
      TorusDemoComponent,

      ObjLoaderDemoComponent,
      ObjStreamLoaderDemoComponent
   ],
   imports: [
      BrowserModule,

      ComponentMenuModule,   // IS already bringing our 'NgThreejsLibModule' library of 3D Components, and 'PipesModule'

      AppRoutingModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
