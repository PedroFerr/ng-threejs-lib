// For ANY file content:
declare module '!raw-loader!*' {
    const contents: string;
    export = contents;
}

// For OLDER (not used anymore) *.md content, like README.md, API.md, etc.:
declare module 'html-loader?minimize=false!markdown-loader!*' {
    const contents: string;
    export = contents;
}