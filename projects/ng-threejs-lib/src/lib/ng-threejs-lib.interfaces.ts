// Next is just for TSLint, once passing properties (string) to any Method of CookieHandlerService, either from App or from Lib
export type NG3DCookieArgsType = 'vpBtn'
    | 'consoleLogStateONper3Dcomponent'
    | 'snapShotOfConsolesONper3Dcomponent'
    | 'monitorSwitches.power'
    | 'monitorSwitches.data'
;

// And, off course, once any cookie prop has FIXED entries (NOT String) for VALUE, we should type its interface
export type VpBtnValueType = 'maximize' | 'restore' | 'halfHeight' | 'minimize';

export interface NG3DCookie {
    // Which Component's bottom Menu adjust panel height button was last chosen/clicked by the User
    vpBtn: VpBtnValueType;

    // List of 3D Components with 'consoleLogIt' set to TRUE:
    consoleLogStateONper3Dcomponent: Array<ComponentBeingConsoledType>;
    // And some snap shot in time of the previous list:
    snapShotOfConsolesONper3Dcomponent: Array<ComponentBeingConsoledType>;

    // -------------------------
    // monitorPowerBtn: boolean;
    // monitorDataBtn: boolean;
    // -------------------------
    // TypeScript BUG: "Type 'any' is not assignable to type 'never'
    // Check it @ https://github.com/microsoft/TypeScript/issues/31663
    // BUG FIX: we must "gather" more than 2 boolean
    // -------------------------
    monitorSwitches: { power: boolean; data: boolean };
    //
    // ------------------------------------------------------------------
    // If you update THIS, DON'T FORGET to update 'NG3DCookieArgsType'!
    // On here there's no hard (TypeScript) corelation - but will exist throughout the App && Lib!
    // ------------------------------------------------------------------
}
export const NG3DCookieInit: NG3DCookie = {
    vpBtn: 'restore',

    consoleLogStateONper3Dcomponent: [], // MIND YOU: the ConsoleLogService constructor() will fill this 3D Components name's list
    // with the initially, hard-coded, that we want to have its 'consoleLogIt' set to TRUE
    // Then User will do it by him/herself, thanks to the expertise of the Front End Developer who did this awesome App! ;-)

    // Same happens hee - see details @ './services-threejs/console-log.service.ts', constructor() code comments
    snapShotOfConsolesONper3Dcomponent: [],

    monitorSwitches: { power: true, data: true }
};

// The Angular  (some) 'SimpleChanges' props, echoed on ngOnInit()
export interface SomeNgChanges {
    whatProp: string;
    fromValue: string | number | THREE.Euler;
    toValue: string | number | THREE.Euler;
}
export type MoveTypeChanged = 'position' | 'translate' | 'rotate';
export type AxisType = 'x' | 'y' | 'z';
export const AxisTypeInit: AxisType = 'y';

// The precious info carried on some Event, that is to be looged on LoggedServices
export interface LoggEventData {
    msg: string;
    data?: EventData;
}

// The properties/obj of 'data' prop, of the Event to logg:
export interface EventData {
    consoleLogIt: boolean;              // Renderered with a tick box, user can choose to console.log, or not,
                                        // the "forbidden", potentially massive, 3D object that can have 'children' with 6.000 + objects!

    when?: string;                      // A unique timeStamp, with the real time, to the 1/1000 sec, when the event occurred
                                        // Obviously, it's only written at the very end, @ 'logger-service' - that's why it's '?' optional
    // tslint:disable-next-line: max-line-length
    where: ComponentBeingConsoledType;  // The Class / Component name that has suffered THIS Event (on Rendering, will be the rendered Obj name)
    operation: EventDataOperationType;

    scalarValues: Array<SomeNgChanges>; // OR 'results' - obj (with) most significative (props) values, before and after operation.
                                        // Still... we should reflect those on 'msg', to be quick readeable.
    onWhat: any;                        // The entire Obj...? Sure? 'children' with 6.000 + objects...?
                                        // Maybe ONLY some needed props for DEMO TS file's operations/logic...? For the Monitor...? Yeap...

    // --------------------------------
    // Specific to "catch" some cases:
    // --------------------------------
    isLoading?: boolean;
}

// The properties of the logged Event, that are to be sent to 'monitor-debug's '<pre />' HTML tag:
// tslint:disable-next-line: no-empty-interface
export interface MonitorData extends LoggEventData {}

export type EventDataOperationType = 'intialize' | 'create' | 'ng-renderer' | 'changed' | 'remove';

export type ComponentBeingConsoledType = 'What is the Class name?!?!?!'
    | 'ComponentMenuComponent' | 'SphereDemoComponent' | 'CubeDemoComponent' | 'ConicalCylinderDemoComponent' | 'TorusDemoComponent'

    | 'ObjLoaderDemoComponent'
    | 'ObjStreamLoaderDemoComponent'

    | 'LoadingWheelComponent'
    | 'MonitorDebugComponent'

    // 3D Stage Components
    | 'RendererCanvasComponent'
    | 'PerspectiveCameraComponent'
    | 'OrbitControlsDirective'
    | 'OrbitControlsComponent'
    | 'RendererService'

    | 'CameraDirective' | 'SceneComponent' | 'AxisHelperComponent' | 'GridHelperComponent'
    | 'PointOfLightComponent' | 'DirectionalLightComponent' | 'HemisphereLightComponent'

    // 3D Components, rendered on the Stage:
    | 'EmptyComponent'
    | 'Mesh3Ddirective' | 'SphereMeshComponent' | 'CubeMeshComponent' | 'CylinderMeshComponent' | 'TorusMeshComponent'

    | 'LoaderDirective'
    | 'Object3Ddirective'
    | 'Object3DLazyDirective'
    | 'ObjLoaderComponent'

    | 'ObjectLoaderComponent'

    | 'SVGLoaderComponent'
;

export interface UserObj3D {
    type: UserObj3DType;
    operation: UserObj3DOperation;
    obj3DParams: any;
}

export type UserObj3DType = 'PointLight' | 'Mesh' |'PerspectiveCamera';
export type UserObj3DOperation = 'create' | 'update' | 'remove';
