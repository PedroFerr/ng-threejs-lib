import { Directive, OnInit, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import * as THREE from 'three';

import { Object3dDirective } from 'projects/ng-threejs-lib/src/lib/modules/scene/object-3d.directive';

import { LoggEventData, ComponentBeingConsoledType } from '../../ng-threejs-lib.interfaces';

import { appliedMaterial } from '../../util/threejs/applied-material';
import { CheckWhatMOVEchanged, CheckWhatChangedOn, LoggThisEvent } from '../../util/threejs/check-ng-changes';

/**
 * A mesh model consists of vertices, edges, and faces that use polygonal representation, including triangles and quadrilaterals,
 * to define a 3D SHAPE.
 * Unlike solid models, mesh has no mass properties.
 * 3D meshes use reference points in X, Y and Z axes to define shapes with height, width and depth.
 */
@Directive({})
export abstract class Mesh3dDirective extends Object3dDirective<THREE.Mesh> implements OnInit, OnChanges {


    @Input() material: string;
    @Input() materialColor = 0x5DADE2;

    @Input() castShadow = true;
    @Input() receiveShadow = true;

    @Input() depthWrite = true;

    ngOnInit() {

    }

    ngOnChanges(propsChanged: SimpleChanges) {
        console.error('*****************************************');
        console.log('Mesh3dDirective changes:', Object.assign({}, propsChanged));
        console.warn('Will this ever going to be called...?');
        console.error('*****************************************');
        // Only if we catch this changes on each 'Mesh' type of Objects extending Class!
        //
        // Because rotateX, Y, Z and translateX,Y,Z only exist at 'Object3dDirective' and...
        // works beautifully on ALL objects - so far tested Camera and Lights ((translation)) and Mesh (translation and rotation) ones!

        // super.ngOnChanges(propsChanged);    // <= we shouldn't be fiddling around with Component's Life Cycle Hooks!!!
        // "might" cause some turbulence on the Mesh3dDirective Class., here extended...
        // Much better - and also called @ Mesh3dDirective:
        // this.ngMeshOnChanges(propsChanged, this.className, this.mesh);

        // Maybe put this changes catch on bottom method? 'ngMeshOnChanges'...?
        //
        // And on 'Object3dDirective' ADD each of the @Input()...? NOPES! **
        // 'material', 'materialColor', 'depthWrite', etc...?
        // I think so!
        //
        // No you don't...
        // ** Only 'Mesh'components have 'material', 'materialColor', 'depthWrite', etc. props.
        // No need to catch them on 'Object3dDirective'. Here, at 'Mesh3dDirective', would be the perfect catch!
        //
        // Just move next block of code (changes catch) into 'ngOnMeshDir...Changes()' method;
        // they come, and are COMMON, from each, and all, of the Mesh component's 'ngOnChanges()' LCH.

        if (CheckWhatChangedOn(this, propsChanged, 'material', 'materialColor', 'depthWrite')) {
            this.applyMaterial();
            // Log it:                                        You see? All this block should be under next Method!
            const loggEvt: LoggEventData = LoggThisEvent(this, 'What is the Class name?!?!?!', null, 'changed', (new THREE.Mesh(null)));
            // Render it:
            this.rendererService.render(loggEvt);
        }
    }

    /**
     * All extended Classes from this 'Mesh3dDirective' one,
     * come here to code "different" changes, but logically treated on the SAME way.
     *
     * @param thatPropsChanged - the 'propsChanged' on THAT extended Class (Sphere, Cube, Torus, etc.)
     * @param onThatClassName - the Class name of THAT extended Class (SphereMeshComponent, CubeMeshComponent, etc.)
     * @param ofThatMesh - the THreeJS Mesh object, created on THAT extended Classes.
     */
    // tslint:disable-next-line: max-line-length
    protected ngMeshOnChanges(thatPropsChanged: SimpleChanges, onThatClassName: ComponentBeingConsoledType, ofThatMesh: THREE.Mesh) {
        // console.error(`Mesh3dDirective: CHANGES on extended "${onThatClassName}": `, Object.assign({}, thatPropsChanged));

        const name: SimpleChange = thatPropsChanged.name;
        if (name && name.currentValue) { // !name.firstChange // <= on begining of times, or by User change
            // Whatever... when DEMO's component menu 'setting's allows the change of the (ANY) 3D object name
        }
        if (['translateX', 'translateY', 'translateZ'].some(propName => propName in thatPropsChanged)) {
            CheckWhatMOVEchanged(this, thatPropsChanged, 'translate', onThatClassName, ofThatMesh);
        }

        if (['rotateX', 'rotateY', 'rotateZ'].some(propName => propName in thatPropsChanged)) {
            CheckWhatMOVEchanged(this, thatPropsChanged, 'rotate', onThatClassName, ofThatMesh);
        }

        const loadFinish: SimpleChange = thatPropsChanged.afterDEMOisLoaded;
        if (loadFinish && loadFinish.currentValue === true && !loadFinish.firstChange) {
            LoggThisEvent(this, onThatClassName, null, 'ng-renderer', ofThatMesh);
        }

    }

    public applyMaterial() {
        this.getObject().material = this.getMaterial();
    }

    protected getMaterial(): THREE.Material {
        return appliedMaterial(this.materialColor, this.material, this.depthWrite);
    }

    protected applyShadowProps(mesh: THREE.Mesh) {
        mesh.castShadow = this.castShadow;
        mesh.receiveShadow = this.receiveShadow;
    }

}
