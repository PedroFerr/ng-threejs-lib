import { Component, forwardRef, Input, OnChanges, SimpleChanges, AfterContentInit } from '@angular/core';
import * as THREE from 'three';

import { Mesh3dDirective } from './_mesh-3d.directive';

import { CreateMeshInstance } from '../../util/threejs/create-instance';

import { Object3dDirective } from 'projects/ng-threejs-lib/src/lib/modules/scene/object-3d.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../services/app-demo/console-log.service';

import { ComponentBeingConsoledType } from '../../ng-threejs-lib.interfaces';

@Component({
    selector: 'ng3d-cube-mesh',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => CubeMeshComponent) }],
    template: '<ng-content></ng-content>'
})
export class CubeMeshComponent extends Mesh3dDirective implements OnChanges, AfterContentInit {

    mesh: THREE.Mesh;
    className: ComponentBeingConsoledType = 'CubeMeshComponent';

    @Input() width = 1.0;
    @Input() height = 1.0;
    @Input() depth = 1.0;

    @Input() widthSegments = 1;
    @Input() heightSegments = 1;
    @Input() depthSegments = 1;

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    ngOnChanges(propsChanged: SimpleChanges) {
        // console.error('Sphere (extends Mesh3dDirective) changes:', Object.assign({}, propsChanged));

        // super.ngOnChanges(propsChanged);    // <= we shouldn't be fiddling around with Component's Life Cycle Hooks!!!
        // "might" cause some turbulence on the Mesh3dDirective Class., here extended...
        // Much better - and also called @ Mesh3dDirective:
        this.ngMeshOnChanges(propsChanged, this.className, this.mesh);
    }

    ngAfterContentInit() {    // <= it's template is an '<ng-content></ng-content>'
        // console.error('Cube Component has its Angular\'s HTML template <ng-content></ng-content> READY');
        // console.warn('... but with its CONTENT empty. ThreeJS will fill it in a minute!');
    }

    protected newObject3DInstance(): THREE.Mesh {
        // console.log('CubeMeshComponent.newObject3DInstance');

        // Uncomment next to see ThreeJS properties TypeScript of this Object's Method:
        // const geometry = new THREE.BoxGeometry();
        this.mesh = CreateMeshInstance(this, 'BoxGeometry', this.name,
            this.width, this.height, this.depth, this.widthSegments, this.heightSegments, this.depthSegments
        );

        return this.mesh;
    }

}
