import { Component, forwardRef, Input, OnChanges, SimpleChanges, AfterContentInit } from '@angular/core';
import * as THREE from 'three';

import { Mesh3dDirective } from './_mesh-3d.directive';

import { CreateMeshInstance } from '../../util/threejs/create-instance';

import { Object3dDirective } from 'projects/ng-threejs-lib/src/lib/modules/scene/object-3d.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../services/app-demo/console-log.service';

import { ComponentBeingConsoledType } from '../../ng-threejs-lib.interfaces';

@Component({
    selector: 'ng3d-torus-mesh',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => TorusMeshComponent) }],
    template: '<ng-content></ng-content>'
})
export class TorusMeshComponent extends Mesh3dDirective implements OnChanges, AfterContentInit {

    mesh: THREE.Mesh;
    className: ComponentBeingConsoledType = 'TorusMeshComponent';

    @Input() radius = 0.4;
    @Input() tube: number;
    @Input() radialSegments = 8;
    @Input() tubularSegments = 6;
    @Input() arc: number = Math.PI * 2;

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    ngOnChanges(propsChanged: SimpleChanges) {
        // console.error('Sphere (extends Mesh3dDirective) changes:', Object.assign({}, propsChanged));

        // super.ngOnChanges(propsChanged);    // <= we shouldn't be fiddling around with Component's Life Cycle Hooks!!!
        // "might" cause some turbulence on the Mesh3dDirective Class., here extended...
        // Much better - and also called @ Mesh3dDirective:
        this.ngMeshOnChanges(propsChanged, this.className, this.mesh);
    }

    ngAfterContentInit() {    // <= it's template is an '<ng-content></ng-content>'
        // console.error('Torus Component has its Angular\'s HTML template <ng-content></ng-content> READY');
        // console.warn('... but with its CONTENT empty. ThreeJS will fill it in a minute!');
    }

    protected newObject3DInstance(): THREE.Mesh {
        // console.log('TorusMeshComponent.newObject3DInstance');

        // Uncomment next to see ThreeJS properties TypeScript of this Object's Method:
        const geometry = new THREE.TorusGeometry();
        this.mesh = CreateMeshInstance(this, 'TorusGeometry', this.name,
            this.radius, this.tube, this.radialSegments, this.tubularSegments, this.arc
        );

        return this.mesh;
    }
}
