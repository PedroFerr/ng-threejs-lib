import { Component, Input, Output, EventEmitter, HostListener, ElementRef, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'ng3d-switch-button',
    template: `
        <div class="switch-container" [style.minWidth]="minWidth">
            <span class="switch"
                [class.checked]="checked"
                [class.disabled]="elemDisabled"
                [class.switch-large]="size === 'large'"
                [class.switch-medium]="size === 'medium'"
                [class.switch-small]="size === 'small'"
                [style.background-color] = "setBackgroundColor"
            >
                <mark></mark>
            </span>

            <span class="switch-text ml-3">
                <span *ngIf="checked && enabledText" class="ui-switch-boolean enabled-text" [innerHTML]="enabledText"></span>
                <span *ngIf="!checked && disabledText" class="ui-switch-boolean disabled-text" [innerHTML]="disabledText"></span>
            </span>
        </div>
    `,
    styleUrls: ['./switch.component.scss'],
    // Next obliges us to work with immutable objects (Observables), or manually trigger (by DOM event or Method) a changes detection.
    // (the component only depends on its @inputs() initializtion/re-initialization - NOT on ALL component's changes)
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwitchButtonComponent {

    @Input() public checked: boolean;
    @Input() public elemDisabled: boolean;
    @Input() public minWidth: string;
    @Input() public size: 'small' | 'medium' | 'large' = 'small';
    @Input() public setBackgroundColor = 'transparent';

    @Input() public enabledText: string;
    @Input() public disabledText: string;

    @Output() public changed = new EventEmitter<boolean>();

    constructor(
        private hostElement: ElementRef
    ) { }

    @HostListener('click', ['$event']) onToggle(evt: any) {

        evt.stopImmediatePropagation();

        // MIND YOU
        // @HostListener() only supports window, document, and body as global event targets.
        // So we have to restrain the listening actions, to this ONLY component elements:
        if (!this.elemDisabled) {
            const componentDOM: HTMLElement = this.hostElement.nativeElement;

            if (componentDOM.contains(evt.target)) {
                this.checked = !this.checked;
                this.changed.emit(this.checked);
            }
        }
    }

}
