import {
    Component, OnInit, Input, Output, EventEmitter, ElementRef,
    ChangeDetectionStrategy, OnChanges, SimpleChanges
} from '@angular/core';
import { SomeNgChanges } from '../../../ng-threejs-lib.interfaces';

@Component({
    selector: 'ng3d-input-block',
    templateUrl: './input-block.component.html',
    styleUrls: ['./input-block.component.scss'],
    // changeDetection: ChangeDetectionStrategy.OnPush
    // We can't optimize upper! ;-) We need to perform changes (on the Template) on others than current @Input()s
    // 'thisColorOfLed' will never return to 'red' again @ 'doSendValueBack()', if the detection of changes is OnPush...
})
export class InputBlockComponent implements OnInit, OnChanges {

    componentDOM: HTMLElement;
    inputElem: HTMLInputElement;

    thisColorOfLed: 'red' | 'green' | 'yellow' | 'blue' = 'red';

    @Input() inputLabel: string;            // <= can be an HTML string

    @Input() initValue: number | string;
    @Input() inputType: 'number' | 'text' | 'color' = 'number';
    @Input() isDisabled: boolean;

    @Input() aidText: string;
    @Input() set renderedValue(rendered3Dvalue: number) {
        this.aidText = rendered3Dvalue !== null ? rendered3Dvalue.toString() : '... start typing.';
    }

    @Output() sendInputBack = new EventEmitter<number | string>();
    timerId: ReturnType<typeof setInterval>;

    constructor(
        private hostElement: ElementRef,
    ) { }

    ngOnInit() {
        this.componentDOM = this.hostElement.nativeElement;
    }

    ngOnChanges(propsChanged: SimpleChanges) {

        if (propsChanged.initValue && !propsChanged.initValue.firstChange) {
            // // Fetch most recent values:
            // this.inputElem = this.componentDOM.querySelector('input');
            // // Inform the world:
            // console.error(
            //     `Change <input /> value from "${propsChanged.initValue.previousValue}" to "${propsChanged.initValue.currentValue}" @`,
            //     this.inputElem
            // );
        }
    }

    /**
     * Echo values inputted by the user, along he/she is writing into it
     *
     * MIND YOU 1):
     * TypeScript does NOT recognize 'value' as a valid prop of HTMLElement or of HTMLInputElement types.
     * That's why we have to cast 'input' as any.
     *
     * MIND YOU 2):
     * The 'if( evt.key...)' condition is to avoid that the user chooses value from native browser's inputs history dropdown...
     * If this happens, it is not considered a "keyboard stroke" and, as so, passed value to the @Output() emitter will be null.
     * The 'if( evt.key.length === 1...)' condition is to assure @Output() emitter won't fire on control chars like 'Tab', 'Esc', etc.
     *
     * @param evt - the key up of a keyboard stroke
     */
    userIsTyping(evt: KeyboardEvent) {
        const input: any = evt.target; // as HTMLInputElement;
        const inputValue: number = input.value;

        if (evt.key && evt.key !== '' && evt.key.length === 1 && this.isNumber(evt.key)) {
            // Inputing '123e3' ignores correctly the 'e3', but inputting '123e[x]3', being [x] any string char, will result on ''.
            if (inputValue) {
                this.doSendValueBack(Number(inputValue));
            }
        }
    }

    userHasChosenColor(evt: KeyboardEvent) {
        const input: any = evt.target; // as HTMLInputElement;
        const inputValue: string = input.value;
        this.doSendValueBack(inputValue);
    }

    /**
     * Believe me; it's tricky to detect if 'some' var is of 'some' type - particular numbers and objects!
     *
     * THis next method was tested with the following results:
     * - - - - - - - - - - - - - - - - - - - - - -
     * isNumber('123');     // TRUE
     * isNumber('123abc');  // false
     * isNumber(5);         // TRUE
     * isNumber('q345');    // false
     * isNumber(null);      // false
     * isNumber(undefined); // false
     * isNumber(false);     // false
     * isNumber('   ');     // false
     *
     * @param n - any "sort" of Number var to be tested
     */
    private isNumber(n: any) {
        return !isNaN(parseFloat(n)) && !isNaN(n - 0); // && !isNaN(parseInt(n.toString(), 10));
    }

    private doSendValueBack(value: number | string) {
        this.thisColorOfLed = 'green';

        // A bit of fun - wait till user stops typing for some 'checkingTime':
        const checkingTime = 500;
        clearInterval(this.timerId);    // <= the PREVIOUS one - next 'timerId' will only be set.... next!

        this.timerId = setInterval(() => {
            this.thisColorOfLed = 'red';
            this.aidText = value.toString();
            this.sendInputBack.emit(value);

            clearInterval(this.timerId);
        }, checkingTime);
    }
}
