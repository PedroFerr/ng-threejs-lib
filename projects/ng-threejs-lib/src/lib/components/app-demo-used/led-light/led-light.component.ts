import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'ng3d-led-light',
    templateUrl: './led-light.component.html',
    styleUrls: ['./led-light.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LedLightComponent implements OnInit {

    @Input() ledColor: 'red' | 'green' | 'yellow' | 'blue';
    @Input() size: 'x-small' | 'small' | 'regular' = 'regular';

    constructor() { }

    ngOnInit() {
    }

}
