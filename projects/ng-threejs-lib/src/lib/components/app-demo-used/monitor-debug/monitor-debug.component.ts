import {
    Component, ViewEncapsulation, ChangeDetectionStrategy, ElementRef, Renderer2, OnInit,
    Input, OnChanges, SimpleChanges, Output, EventEmitter, AfterViewInit, OnDestroy
} from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';
import { CookieHandlerService } from '../../../services/app-demo/cookie-handler.service';

import { LoggEventData, EventData, MonitorData, ComponentBeingConsoledType } from '../../../ng-threejs-lib.interfaces';

import { returnNowsTime } from '../../../util/js/returnNowsTime';
import { waitForFinalEvent } from '../../../util/js/timeout-controller';

@Component({
    selector: 'ng3d-monitor-debug',
    templateUrl: './monitor-debug.component.html',
    styleUrls: [
        './monitor-debug.component.scss',
        '../switch/switch.component.scss'
    ],
    // changeDetection: ChangeDetectionStrategy.OnPush, // Optimize performance - component will only RE-mount IF some @Input() has CHANGES.
    // See explanation below, on 'ngOnChanges()', of why this HAS to be COMMENTED.

    encapsulation: ViewEncapsulation.None            // THIS WOULD EVER BE DONE! Takes encapsulation out of the CSS but also of the HTML!
    // As the monitor content is done by [innerHTML], without this lifting of the component's encapsulation, '.scss' would NOT render.
    //
    // MIND YOU:
    // This is a very well known bug from Angular team, since... many years ago.
    // And on Angular 9 they still didn't deal with it properly. This (getting ride of componnet's encapsulation, when injecting HTML)
    // it's a 'magic' turnaround - Angular has, in fact, a problem updating shadow DOM styling. Period.
    // https://github.com/angular/angular/issues/7845
    // Best answer on this thread: https://github.com/angular/angular/issues/7845#issuecomment-412372506~
    // though not entirely correct. If you use SCSS, (just) sanitizing the injection of HTML won't resolve the problem.
    //
    // Like we do on the 3D Components of the library, you could use, as an alternative: 'host /deep/',
    // though Angular itself says it's deprecated, on the component's style sheet.
    // With this (again hacked solution) we could leave the encapsulation alone, on its very own important business
    // (this, I repeat, its a BUG and a very strong one, due to the exposed HTML (probably) clash with other components!)
    //
    // Or, as the best possible solution of all, you could move this styling code to main's App style sheet: 'src\styles.scss'
    // Will work beautifull, you would stay in control of the entire componentiztion (proper) styling, over the entire APP,
    // and you would NOT have to do this crazy thing anymore - taking the encapsulation out of a Component.
})
export class MonitorDebugComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {

    componentDOM: HTMLElement;
    componentWrapper: HTMLElement;

    monitor: HTMLElement;
    isMonitorOn = true;

    monitorScreen: HTMLElement;
    preContent: HTMLElement;
    contentHTMLmsg: string;

    // --------------------------------
    monitorControls: HTMLElement;

    monitorPowerBtn: HTMLElement;
    powerButtonBgrd = 'transparent';
    colorOfPowerLED = 'green';
    isAFakeClick = false;

    monitorDataBtn: HTMLElement;
    dataButtonBgrd = 'darkolivegreen';
    colorOfDataLED = 'blue';
    isDataBtnDisabled = false;
    isDataBtnChecked = true;
    isDataToBeRendered = true;

    setEmptyBinDisabled = false;
    loggEvtsCounter: HTMLElement;
    // --------------------------------

    @Input('goToTopPosition') set settingComponentPos(atCSSvalue: string) { this.marginTop = atCSSvalue; }
    marginTop: string;

    // HTML [] is a SCALAR Object value, either directly written OR by PIPE 'async':
    @Input() dataToPrint: MonitorData | string;
    // OR
    // HTML [] is an OBSERVABLE value of an Object:
    // @Input() dataToPrint$: Observable<MonitorData>;

    // NgRxJS stream of added logg events, through LoggerService, all over the App:
    loggEvent$: Observable<LoggEventData | string>;
    loggSubscription: Subscription;
    // loggEvent: string;

    @Output() angularLoaded = new EventEmitter();

    separatorPerLine = '<span class="separator"><hr class="per-line"></span>';
    blockSeparator =   '<span class="separator"><hr></span>';
    fakeSeparator =    '<span class="separator"><hr class="fake-separator"></span>';
    initMsg = 'Angular\'s \'Debug SANYO Monitor\' mounted @ "ngOnInit()".' + this.blockSeparator;
    afterInitMsg = 'Monitor\'s "ngAfterViewInit()": start listening to "ThreeJS" 3D Event\'s data...' + this.blockSeparator;
    afterReInitMsg = 'Monitor\'s cls: listening to "ThreeJS" 3D Event\'s data...' + this.blockSeparator;
    loggEvtsCounterInitMsg = '0 evts';

    timerId: ReturnType<typeof setInterval>;
    monitorLineCounter = 0;
    loggedEvtCounter = -1;

    constructor(
        private hostElement: ElementRef,
        private ngRenderer: Renderer2,

        private ThreeJSloggerService: LoggerService,
        private consoleLogService: ConsoleLogService,
        private cookieHandlerService: CookieHandlerService

    ) { }

    ngOnInit() {
        this.componentDOM = this.hostElement.nativeElement;
        this.contentHTMLmsg = '';

        // 1st message on debug 'SONY' monitor:
        const timeStamp: string = returnNowsTime();
        const firstMonitorMsg = `${this.blockSeparator + '@ ' + timeStamp + ' - ' +  this.initMsg}`;
        this.printData( { msg: firstMonitorMsg, data: null } );

        this.loggEvent$ = this.ThreeJSloggerService.newEvtLogged$;
        // Send to the Monitor the scalar of this Observable:
        this.loggSubscription = this.loggEvent$.subscribe((loggEvtObj: LoggEventData | string) => {

            if (this.isMonitorOn) {
                if (this.consoleLogService.isThisBeingConsoled('MonitorDebugComponent')) {
                    console.log('|||||||| MONIT LIBY new LOGG evt: ', loggEvtObj);
                }
                this.sendDataToMonitor(loggEvtObj);
            }
        });

    }

    ngOnChanges(propsChanged: SimpleChanges) {
        // console.error('------------------------------ Monitor ngOnChanges():', Object.assign({}, propsChanged));

        // -------------------------------------------------------------------------------------------------------------------------------
        //         Receiving OBSERVABLE (loggEvent$) into @Input() dataToPrint$
        // -------------------------------------------------------------------------------------------------------------------------------
        // if ( propsChanged.dataToPrint$ && propsChanged.dataToPrint$.currentValue /* && !propsChanged.dataToPrint$.firstChange */) {
        // if (propsChanged.dataToPrint$.firstChange === true) {
            // We only need a FIRST trigger (a CHANGE from 'undefined' to anything - our Observable)
            // in 'propsChanged.dataToPrint$.currentValue'. Then next 'subscription' is triggered FOR EVER.
            //
            // Meaning it's NO LONGER the 'dataToPrint$' content/value that is inducing CHANGES - it is NOT, after 1st change
            // BUT rather the 'subscription' that connects our 'this.contentHTMLmsg' to the ThreeJSloggerService 's
            // 'public newEvtLogged$ = new Subject<string>();' var, through our 'this.dataToPrint$' channel, sort of speak...
            //
            // To proove it, next 'console.error', or upper one,
            // will only appear ONCE - on 'propsChanged.dataToPrint$.firstChange === true'! That's it.
            //
            // This is the reason why we have to COMMENT upper 'changeDetection: ChangeDetectionStrategy.OnPush'
            // If that was on, component would ONLY re-mount/re-render (update the componnet's HTML vars)
            // IF, and only if, Angular's ChangeDetection saw one of the @Input()s CHANGING it's VALUE.
            //
            // Here, and concerning the printing of logg msgs onto '<pre [innerHTML]="contentHTMLmsg"></pre>', that will NEVER happen twice!
            // So... ;-) in fact we do NOT need ngOnChanges() life cycle hook, at all!
            // We could simply do next 'subscription' on upper ngOnInit, subscribing to 'this.ThreeJSloggerService.newEvtLogged$;' DIRECTLY.
            // -----------------------------------------------------
            // And that's exactly what we are going to do!
            // -----------------------------------------------------

            // console.error('=============== dataToPrint has CHANGED:', this.dataToPrint$);    // <= HAS NOT! The 'firstChange' has! Once.

            // Next is the place were loggEvt changes will be detected, for the dynamically updating of 'this.contentHTMLmsg':
            // this.loggSubscription = this.dataToPrint$.subscribe((logEvt: string) => {
            //     console.log('|||||||| MONIT new LOGG evt: ', logEvt)
            //     // this.printData(this.dataToPrint);
            //     // this.contentHTMLmsg = logEvt;
            //     this.printData({ msg: logEvt, data: null});
            // });
        // }

        // -------------------------------------------------------------------------------------------------------------------------------
        //         Receiving PIPE (loggEvent$ | async) scalar into @Input() dataToPrint
        // -------------------------------------------------------------------------------------------------------------------------------
        if ( propsChanged.dataToPrint && propsChanged.dataToPrint.currentValue /* && !propsChanged.dataToPrint.firstChange */ ) {
            // -----------------------------------------------------
            // Now we have working, on parallel, the 2 "methods" of aquiring data into 'contentHTMLmsg':
            // 1) the 'ngOnInit()' subscription to the logged msgs Observable
            // AND
            // 2) the '@Input() dataToPrint: string;' fed by the parent's HTML PIPE '[dataToPrint]="(loggEvent$ | async)"'.
            //
            // Though very, very weird... it works! And in the only possible combination I've tried.
            // NO MISSED printed LOGGED EVENTS!
            // See launched thread in here: https://stackoverflow.com/q/61737835/2816279
            // -----------------------------------------------------
            //
            // Now we only have to delete coming DUPLICATES - so we comment this 'if', ngOnChange() data aquire!
            // That's what I've explained on my own answer to the launched thread: https://stackoverflow.com/a/61743303/2816279
            // This has also originate the opening of an Issue #3 on our repo: https://gitlab.com/PedroFerr/ng-threejs-lib/-/issues/3
            //
            // However, once we have STARTED to print the 'data' prop BEAUTIFIED JSON's, we have started AGAIN missing some events...
            // It seems clear now that going to beautifier JS manipulation is still too "heavy"!
            //
            // So, on this part, after the component mounts and settles down, if you ONLY print the 'msg'... it works!!!
            // But it's a pitty we can't have our beautified JSON's anymore, when 'onChanges()' is called...
            // -----------------------------------------------------

            // console.error('=============== dataToPrint has CHANGED:', this.dataToPrint);

            // // this.printData({ msg: this.dataToPrint, data: null});
            // const logEvtObjSTR = this.dataToPrint as string;
            // const logEvtObjOBJ = this.dataToPrint as LoggEventData;

            // const dataToMonitor: MonitorData = {
            //     msg: this.dataToPrint.hasOwnProperty('msg') ? logEvtObjOBJ.msg : logEvtObjSTR,
            //     data: this.selectDataToMonitor(logEvtObjOBJ)
            // };
        }
        // -------------------------------------------------------------------------------------------------------------------------------

        if (propsChanged.settingComponentPos /*&& !propsChanged.settingComponentPos.firstChange*/) {
            // This @Input() is sent directly from the parent 'monitorMarginTop' var, on the HTML's square brackets
            console.log(
                'TODO: settingComponentPos @Input() has CHANGED:',
                this.marginTop,
                `Will it be needed...? Once Monitor has moved from 'component-menu' to 'settings' child..?`
            );
        }

    }

    ngAfterViewInit() {
        this.componentWrapper = this.componentDOM.querySelector('.component-wrapper');

        this.monitor = this.componentWrapper.querySelector('main');

        this.monitorScreen = this.monitor.querySelector('.monitor-screen-printer');
        this.preContent = this.monitorScreen.querySelector(`pre`);

        this.monitorControls = this.monitor.querySelector(`section.monitor-controls`);
        this.monitorPowerBtn = this.monitorControls.querySelectorAll('ng3d-switch-button')[0] as HTMLElement;
        this.monitorDataBtn = this.monitorControls.querySelectorAll('ng3d-switch-button')[1] as HTMLElement;
        this.loggEvtsCounter = this.monitorControls.querySelector('div.delete-content-wrapper > div.button-control-text');

        this.ngRenderer.setStyle(this.componentWrapper, 'margin-top', this.marginTop);

        this.checkMemorizedMonitorSwitchesStatus();

        // ===============================================================
        // This is the MOST IMPORTANT line of code of the ENTIRE APP!
        // 3D Library only starts mounting at THIS exact MOMENT!
        // ================================================================
        // DOM is stable on the entire DEMO Angular set of components
        // => say it back to the App's DEMO 3D Stage:
        // =======================
        this.angularLoaded.emit();
        // =======================
    }

    ngOnDestroy() {
        if (this.loggSubscription) {
            this.loggSubscription.unsubscribe();
        }
    }

    /**
     * Delete all Data Monitor messages currently on ' this.ThreeJSloggerService.loggBook' private Array
     * and on 'section.monitor-screen-printer > pre' Monitor
     *
     * Like in 'ngAfterViewInit()', when this Component has just finished mounting,
     * send a mesage to the monitor, announcing party is ready to begin again.
     * Don't forget to initialize the counters! As if we were on the begining of times...
     */
    deleteDataContent(printInitMsg: boolean): boolean {

        // ------------------------------------------------------------------------
        // BUG fix found @ sendDataToMonitor(), this.timerId = setInterval(...):
        this.printData({msg: this.fakeSeparator, data: null});
        this.pullBottomMonitorMessagesDown();
        // ------------------------------------------------------------------------

        const deleteSuccess = this.ThreeJSloggerService.emptyLoggBook();    // <= if successful, also clears console

        if (deleteSuccess) {
            this.contentHTMLmsg = '';
            this.loggEvtsCounter.innerHTML = this.loggEvtsCounterInitMsg;

            if (printInitMsg) {
                // The begining of times:
                this.monitorLineCounter = 0;
                this.loggedEvtCounter = -1;
                // Does the turns of 'this.afterInitMsg', on 'ngAfterViewInit' - we can't say it's mounted AGAIN!
                const timeStamp: string = returnNowsTime();
                this.monitorLineCounter ++;
                this.printData( { msg: `${this.blockSeparator}@ ${timeStamp} - ${this.afterReInitMsg}`, data: null } );
            }
        }

        return deleteSuccess;
    }

    /**
     * Power is set to ON, on loading.
     *
     * If User turns power OFF:
     * 1) the buttons UX should pair - if no power, also no data
     * 2) also disable the 'empty bin' icon
     * 3) like 'deleteDataContent()', we should clean up everything to do with logged events printing
     * except emiting the 'ready' msg to the Monitor and stop receiving them - this.isMonitorOn = false;
     * 4) finally we don't need to keep saving the events on our Logger Service 'loggBook' array.
     *
     * If User turns the power ON again:
     * 1) we don't necessarily have to turn DATA on... but we have to ENABLE back the 'empty bin' icon
     * 2) we must re-start receiving logged events again - set the flag back to true and the 'ready' msg to the Monitor
     * 3) we must not forget to continue, from now, saving the Events to the Logger Service 'loggBook' array.
     *
     * @param isOn - the (already) CHANGED state of the switch, once User HAS clicked on it
     */
    onSwitchingMonitorPower(isOn: boolean) {
        const onOff = isOn.toString().replace('true', 'ON').replace('false', 'OFF');
        console.log('Monitor\'s POWER switch is ', onOff);

        // ------------------------------------------------------------------------
        // BUG fix found @ sendDataToMonitor(), this.timerId = setInterval(...):
        this.printData({msg: this.fakeSeparator, data: null});
        this.pullBottomMonitorMessagesDown();
        // ------------------------------------------------------------------------

        this.powerButtonBgrd = !isOn ? 'var(--primary)' : 'transparent';

        if (!isOn) {
            const deleteSuccess = this.deleteDataContent(false);    // <= if successful, also clears console
            // User mibght have canceled or, in fact, an error could be thrown...
            if (!deleteSuccess) {
                // We have to "revert" all the UX done:
                this.powerButtonBgrd = isOn ? 'var(--primary)' : 'transparent';
                // And finally, click back to previous state:
                this.isAFakeClick = true;     // <= this will make code jump to the outer else
                this.monitorPowerBtn.click();

            } else {
                this.colorOfPowerLED = 'red';
                // ------------------------------------------
                // We're good! It's really to turn power OFF!
                // ------------------------------------------
                this.isAFakeClick = false;
                this.preContent.classList.toggle('power-off');
                // Turn also off 'data' button IF, and only IF, it was turned on:
                if (this.monitorDataBtn.style.backgroundColor !== 'transparent' && this.isDataBtnChecked)  { this.monitorDataBtn.click(); }
                // No power, no 'data' and no deleting or events counter:
                this.isDataBtnDisabled = !isOn;
                this.colorOfDataLED = 'red';
                this.setEmptyBinDisabled = !this.setEmptyBinDisabled;
                this.loggEvtsCounter.innerHTML = '';

                this.isMonitorOn = false;
                this.ThreeJSloggerService.isEventToBeSaved = false;
                this.cookieHandlerService.setCookieProp('monitorSwitches.power', false);
            }

        } else {

            waitForFinalEvent(() => { // <= MIND YOU once an alert() is launched by the browser (@ this.deleteDataContent()) ... time stops!
                if (!this.isAFakeClick) {
                    // ------------------------------------------
                    // It's really to turn power ON!
                    // ------------------------------------------
                    this.cookieHandlerService.setCookieProp('monitorSwitches.power', true);
                    this.ThreeJSloggerService.isEventToBeSaved = true;
                    this.isMonitorOn = true;
                    this.preContent.classList.toggle('power-off');
                    // Power ON => DATA on.... in a minute! Msgs can be deleted and counted again
                    this.colorOfPowerLED = 'green';
                    this.isDataBtnDisabled = !isOn;
                    this.setEmptyBinDisabled = !this.setEmptyBinDisabled;
                    this.loggEvtsCounter.innerHTML = this.loggEvtsCounterInitMsg;

                    // this.deleteDataContent(true); // <= we don't want to delete nothing; just the msg!
                    this.monitorLineCounter = 0;
                    this.loggedEvtCounter = -1;
                    const timeStamp = returnNowsTime();
                    this.monitorLineCounter ++;
                    this.printData( { msg: `${this.blockSeparator}@ ${timeStamp} - ${this.afterReInitMsg}`, data: null } );

                    // Force the rendering - the bug again which we are, here, taking some advantage on:
                    // ------------------------------------------------------------------------
                    // BUG fix found @ sendDataToMonitor(), this.timerId = setInterval(...):
                    this.monitorDataBtn.click();
                    // ------------------------------------------------------------------------
                    // Power ON => DATA on... in a minute!
                    waitForFinalEvent(() => {
                        this.monitorDataBtn.click();
                        // Set up the hard coded "true"s on ConsoleLogService
                        this.consoleLogService.setAllConsoleLogONcomponentsToInitState();
                        // MIND YOU:
                        // After we have coded the APP for the UX's browser's local storage (cookie) memorization,
                        // to always preserve User choices - even if the user closes the browser and come back on next other day!)
                        // THIS code point is the ONLY chance of the User to "reset" ALL the 3D Component's 'consoleLogIt' into
                        // the initial, begining of times, state settings
                        // (just in case he/she got lost, or had set up too many components to echo their Data objects into the Inspector).
                        //
                        // It is not codded NO other way in the entire App!
                        // It's either by turning POWER OFF and then ON again... or by MANUAL deleting the cookie on the browser! ;-)
                        //
                    }, 1000, `Unique Id #${timeStamp}` );
                }
            }, 0, `Unique Id #${returnNowsTime()}` );
        }

    }

    onSwitchingMonitorData(isOn: boolean) {
        const onOff = isOn.toString().replace('true', 'ON').replace('false', 'OFF');
        console.log('Monitor\'s DATA switch is ', onOff);

        // ------------------------------------------------------------------------
        // BUG fix found @ sendDataToMonitor(), this.timerId = setInterval(...):
        this.printData({msg: this.fakeSeparator, data: null});
        this.pullBottomMonitorMessagesDown();
        // ------------------------------------------------------------------------

        this.dataButtonBgrd = isOn ? 'darkolivegreen' : 'transparent';
        this.isDataBtnChecked = !this.isDataBtnChecked;

        if (!isOn) {
            this.colorOfDataLED = 'red';
            // Monitor doesn't have, anymore, the 'data' button
            this.isDataToBeRendered = false;
            // Stop consoling the 'data' objects @ browser's Inspector:
            this.consoleLogService.toggleAllCurrentMemorizedConsolesTo(false);
            // Memorize it so it previals crossing DEMO components routing:
            this.cookieHandlerService.setCookieProp('monitorSwitches.data', false);
        } else {
            this.colorOfDataLED = 'blue';
            // Bring the 'data' button to each logged event line, on the screen:
            this.isDataToBeRendered = true;
            // ... and allow the same consoles that were ON before he/she turned them OFF:
            this.consoleLogService.toggleAllCurrentMemorizedConsolesTo(true);
            // Memorize it so it previals crossing DEMO components routing:
            this.cookieHandlerService.setCookieProp('monitorSwitches.data', true);
        }
    }


    /**
     * User clcked 'a' Monitor line's button, identified by an 'i' index,
     * to see Data (object) content, JS manipulated @ 'this.dataObjBeautifiedHTML(i)'
     *
     * A couple of things have to be done, though the 1st and second are the core ones:
     * 1) Fill the underlying '<div class="obj-viewer-container"><pre></pre></div>' with the indexed 3D Event 'data'
     * 2) slide down the new injected HTML content
     * And:
     * a) Before passing down the clicked boolean, and sliding down the panel,
     *     check it's, effectively, a CHANGE - chosen boolean !== 'radioValueBeforeClick'
     * b) If it is, toggle this component 'consoleLogIt' state through consoleLogService
     * c) Sign the panel trigger with some UI change, so user can see all done toggles
     * d) To disable 'double' togles (though IT IS code prevented, on consoleLogService), also disable
     *     all panel triggers of the same event's 'where' prop (a 'ComponentBeingConsoledType' type string)
     *
     * @param viewerId - the 'i' index, on '.obj-viewer-trigger': Array<HTMLElement>, coming in as 'obj-viewer-[i]'
     */
    openObjViewer(viewerId: string) {
        const aTemp = viewerId.split('-');
        const contentIdx: number = Number(aTemp[aTemp.length - 1]);

        const thisObjViewer: HTMLElement = this.componentDOM.querySelector('#' + viewerId);
        const objViewerPreTagContent: HTMLElement = thisObjViewer.querySelector('pre');

        if (objViewerPreTagContent.innerHTML === ' -- to be filled ON CLICK! -- ') {
            // tslint:disable-next-line: variable-name
            const beautifiedObJSONtoHTML_OBJ: {
                html: string; hasConsoleLogItProp: boolean; fromWhere: ComponentBeingConsoledType
            } = this.dataObjBeautifiedHTML(contentIdx);
            const whereComponent: ComponentBeingConsoledType =  beautifiedObJSONtoHTML_OBJ.fromWhere;

            // -----------------------------------------------------------------
            objViewerPreTagContent.innerHTML = beautifiedObJSONtoHTML_OBJ.html;
            // -----------------------------------------------------------------

            // Now our JSON panel viewer is on DOM,
            setTimeout(() => {
                // do the usual adding of event listeners, on 'click', on '.console-log-it-wrapper'
                // pair of radio buttons ('consoleLogIt' prop ON/OFF), and finally, open it to the user:
                if (beautifiedObJSONtoHTML_OBJ.hasConsoleLogItProp) {
                    // (MIND YOU to send down the refreshment of 'thisObjViewer', now that DOM's changed - NOT 'thisObjViewer' const)
                    this.objViewerAddRadioBtns(this.componentDOM.querySelector('#' + viewerId), contentIdx);
                    setTimeout(() => this.objViewerAddListeners(viewerId, whereComponent), 0);
                }
                thisObjViewer.style.display === 'block' ? thisObjViewer.style.display = 'none' : thisObjViewer.style.display = 'block';
            }, 0);

        } else {
            thisObjViewer.style.display === 'block' ? thisObjViewer.style.display = 'none' : thisObjViewer.style.display = 'block';
        }
    }

    private objViewerAddListeners(viewerId: string, component3D: ComponentBeingConsoledType) {
        const lastObjectViewerCall: HTMLElement = this.componentDOM.querySelector('#' + viewerId);
        // tslint:disable-next-line: max-line-length
        const currentTriggerBtns: NodeListOf<HTMLElement> = lastObjectViewerCall.querySelectorAll('.console-log-it-wrapper > input');
        // tslint:disable-next-line: max-line-length no-string-literal
        const radioValueBeforeClick: string = lastObjectViewerCall.querySelector('.console-log-it-wrapper > input[type="radio"]:checked')['value'];

        currentTriggerBtns.forEach(
            (consoleLogItRadioBtn: any /* HTMLElement => TS error: unknown 'value' prop */ ) =>

                consoleLogItRadioBtn.addEventListener('click', (evt: MouseEvent) => {
                    evt.stopImmediatePropagation();

                    const clickedConsoleLogItValue /* : string */ = consoleLogItRadioBtn.value;
                    // !!!!! A string...?!?!?! Yeap... it's a string you're passing down! HTML does not have booleans...
                    const toPassBoolean: boolean = clickedConsoleLogItValue === 'true';
                    // ;-)
                    const onOffString: string = clickedConsoleLogItValue.replace('true', 'ON').replace('false', 'OFF');
                    // Anyway we are not passing down a 'boolean' anymore; we just TOGGLE current state.
                    // ---------------------------------------------------------------------------------------------
                    // So we need to know if, in fact, there was a TOGGLE intention, or the user clicked on the SAME boolean:
                    // ---------------------------------------------------------------------------------------------
                    if (radioValueBeforeClick !== clickedConsoleLogItValue) {
                        window.alert(
                            `You have turned ${onOffString} the "${component3D}" 'consoleLogIt' property` +
                            `\nThank you\n\n` +
                            `You do can revert it, by coming to this same "${component3D}" Event again...`
                        );
                        this.consoleLogService.toggleThisComponentConsoleLogStateTo(component3D, toPassBoolean);

                        this.objViewerStylingChanges(lastObjectViewerCall, viewerId, component3D);
                    }
                })
        );
    }

    private objViewerStylingChanges(objViewer: HTMLElement, viewerId: string, component3D: ComponentBeingConsoledType) {
        // Some UI changes, to outstand THIS, now changed, Event line:
        const thisEvtLineDataBtn: HTMLElement = this.componentDOM.querySelector('button[data-target=' + viewerId);
        this.ngRenderer.addClass(thisEvtLineDataBtn, 'has-changed');

        // Disable all similar 'where' Event's (same 'component3D') line buttons:
        const similarBtns: NodeListOf<Element> = this.componentDOM.querySelectorAll('button.obj-viewer-trigger');
        const filteredLogbook = this.ThreeJSloggerService.getLoggBookIdxFilterWhereEvts(component3D);

        filteredLogbook.forEach((idx: number, thisIndex: number) => {
            if (similarBtns[idx]) { this.ngRenderer.setAttribute(similarBtns[idx], 'disabled', 'true'); }
            if (thisIndex === filteredLogbook.length - 1) {
                // Give it 800 ms and close the ogject viewer panel:
                setTimeout(() => this.ngRenderer.setStyle(objViewer, 'display', 'none'), 800);
            }
        });
    }

    /**
     * After this component gets its DOM all settle down (this Method is exclusively called from 'ngAfterViewInit()'),
     * we must check if the User, in any other DEMO, has switched OFF any of the Monitor bottom bar control switches.
     *
     * i.e. if DATA button is OFF we'll NOT print 1st component (has mounted) message.
     *
     * MIND YOU 2 UI/UX important DETAILS:
     * 1) Besides Logged Events printing concerns, as buttons have different styling once ON or OFF,
     * we need to 'click()' each accordingly to what's memorized, so user understands, by the UI,
     * the Monitor he/she is using, and operating, 'is' the same and the only thing changing is chosen 3D DEMO Stage.
     *
     * 2) As this switches status are being memorized onto the browser's local storage, in an always-preserve-User-choices feature,
     * either by refreshing from top, or by closing the browser and coming back on next other day,
     * ALL User's customization choices done so far WILL prevail.
     * This is a cool feature, but can be a pain in case he/she gets lost, or, unconscientiously, had set up too many components to echo
     * their Data objects into the Inspector, on each 'init', 'change' or 'destroy' of each of the current on Stage 3D components.
     * So... we do need a kind of quick, easy to access, intuitive, 'factory' reset...
     *
     * The User POWER OFF, and then ON, action (check 'onSwitchingMonitorPower()', on the 'OFF' part) is the ONLY chance to "RESET"
     * ALL the 3D Component's 'consoleLogIt' into the initial, begining of times, resonable, thoughtful and hard coded state settings
     * (check it @ 'ConsoleLogService' initial status definition of global vars).
     *
     * So, about this second detail, it's either by turning POWER OFF/ON... or by MANUAL deleting the cookie on the browser! ;-)
     * There's no other way for the User to perform a 'factory' reset.
     *
     * MIND YOU
     * that both of the method's top vars can be.... null!
     * If user has entered the application now and NEVER touched the Monitor bottom bar control any of the switch buttons!
     * Nevertheless the whole component's logic was built around to have, at the begiing of times, BOTH switches ON.
     * We have, so, to make it happen => 'click()' each if local storage says they were switched to OFF by the User, anytime back.
     *
     */
    private checkMemorizedMonitorSwitchesStatus() {
        const switchDATAstate = this.cookieHandlerService.getCookieProp('monitorSwitches.data') as boolean;
        const switchPOWERstate = this.cookieHandlerService.getCookieProp('monitorSwitches.power') as boolean;

        if (switchPOWERstate !== false && (switchDATAstate === null || switchDATAstate === true)) {
            // Second message sent to monitor, saying party is ready to begin:
            const timeStamp: string = returnNowsTime();
            this.printData( { msg: `@ ${timeStamp} - ${this.afterInitMsg}`, data: null } );
        }

        if (switchDATAstate === false) { this.monitorDataBtn.click(); }
        if (switchPOWERstate === false) { this.monitorPowerBtn.click(); }
    }

    /**
     * Method NOT called anymore - from ngOnChanges()
     *
     * Code stays just for better understanding sake of the problem: print beautified JSONs, at a rate of 1/1000 sec.
     * Impossible.
     *
     * @param loggEvtObj - the 3D object, first to beautify and stringify, and then to be printed on the Monitor screen
     */
    private selectDataToMonitor(loggEvtObj: LoggEventData): any | EventData {
        if (loggEvtObj.hasOwnProperty('data')) {

            /*
            delete loggEvtObj.data.onWhat;        // That's, potentially, the massive Obj!
            // On Models done by loading a JOB file, can have thousands of properties on 'children' one - i.e. '3 floor Office'!
            // Now we keep deleting/reducing props.... till we have no missing msgs!

            delete loggEvtObj.data.where;         // It's more or less aleady on the 'msg'
            delete loggEvtObj.data.operation;     // It's more or less aleady on the 'msg'
            delete loggEvtObj.data.scalarValues;  // It's more or less aleady on the 'msg'
            */

            // Again.... the upper JS processing of an Object (remove props) takes an HUGE ammount of time!
            // Can't be done; Msgs start being missed again...
            return null;

        } else {
            return null;
        }
    }

    private sendDataToMonitor(rawData: LoggEventData | string) {
        const logEvtObjSTR = rawData as string;
        const logEvtObjOBJ = rawData as LoggEventData;

        this.printData({
            msg: rawData.hasOwnProperty('msg') ? logEvtObjOBJ.msg : logEvtObjSTR,
            data: rawData.hasOwnProperty('data') ?
                // logEvtObjOBJ.data    // <= Too much processing; monitor looses next entries!
                //
                // {
                //     0: logEvtObjOBJ.data.where,
                //     1: logEvtObjOBJ.data.scalarValues,
                // }
                //
                // As a default, pass down 'null' => NO data objs will be PRINTED, for now (on Monitor's run time).
                // They do can, later, by User click on 'data' button existent on each Logged Event line (with text, only)
                null
                :
                null
        });

        // Maybe the user is inspecting console's DATA huge Objs => always pull monitor content down, when overflow starts:
        this.pullBottomMonitorMessagesDown();

        // ----------------------------------------------------
        // If nothing is happening, on some 'checkingTime' time,
        // render one last line 'block separator' on the Monitor, to divide moments:
        // ----------------------------------------------------
        const checkingTime = 500;
        clearInterval(this.timerId);    // <= the PREVIOUS one - next 'timerId' will only be set.... next!

        this.timerId = setInterval(() => {
            // console.warn(`## ${checkingTime}ms elapsed => end of block rendering!`);
            this.printData({msg: this.blockSeparator, data: null});
            this.pullBottomMonitorMessagesDown();
            // We have a bit of time - update counter
            this.loggEvtsCounter.innerHTML = `${this.loggedEvtCounter + 1} evts`;
            //
            // We have here a (Angular...?!? Browser..?) BUG:
            // This extra end seperator does NOT get printed/rendered right away - and I can see it already on 'this.contentHTMLmsg'!
            // Only when next line comes, will this, plus the new, render entirely... but then, there's an end, and we're back to the issue
            // So... don't know what to do! It seems like it's needed some kind of 'return'...
            //
            // Check it @ https://dev.to/akanksha_9560/why-not-to-use-setinterval--2na9
            // "We see here when setInterval encounters time intensive operation, it does either of two things,
            // a) try to get on track
            // or
            // b) create new rhythm.
            // Here on chrome it creates a new rhythm."
            // And on discussion, below the article, you can read another opinion, going on the same way:
            // "I agree, if there will be heavy calculation or long time awaiting, it will work unpredictable. "
            // ...
            // "Another reason is because setIntervals stop running if your browser window loses focus.
            // your callbacks will queue up and run once the page has focus again.
            // Or even worse, they will be dropped. See jsfiddle.net/mendesjuan/y3h1n6mk/"
            // YES! The posted link is the FINAL proove: browser has a HUGE prolem with 'setInterval', when time is of the absence!
            //
            // And continues, the evil...
            // ANY click on the Monitor bottom control bar... makes MAGICALLY the 'lost' last separator to render!
            // F'ing weirdooooooooooooooooooooo
            // If we can't control it from here...
            // Let's assume the BUG as a native "behaviour" and control it from the Monitor's bottom control bar icon click...

            clearInterval(this.timerId);
            return null;

        }, checkingTime);
        // ----------------------------------------------------
    }

    /**
     * Print previous content + this.separatorPerLine + jsonToPrint (msg + data) Data,
     * using a JS stringify beautifier plug-in, into SANYO HTML's tag monitor.
     *
     * Do the 3D object viewer collapsable panel HTML, for user to click AFTER the massive rain of logged events has stopped.
     *
     * @param jsonToPrint - what to print NEW, cumulated with PREVIOUS, that can have a string msg and/or an data
     */
    private printData(dataToPrint: MonitorData) {

        this.monitorLineCounter ++;

        const isMonitorLineAnEvent: boolean = (
            this.monitorLineCounter > 2 && dataToPrint.msg !== this.blockSeparator && dataToPrint.msg !== this.fakeSeparator
        );
        if (isMonitorLineAnEvent) { this.loggedEvtCounter ++; }
        // --------------------------------------------------------------------------------------------

        const collapsableObjViewerLine = `
            <button class="btn btn-outline-secondary obj-viewer-trigger"
                data-target="obj-viewer-${this.loggedEvtCounter}" title="See this event data"
            ><small>data</small></button>
            ${dataToPrint.msg}
            <div id="obj-viewer-${this.loggedEvtCounter}" class="obj-viewer-container" style="display: none;">
                <pre> -- to be filled ON CLICK! -- </pre>
            </div>
        `;
        const cumulatedData = this.contentHTMLmsg + // this.separatorPerLine +    // <= not using anymore
            (dataToPrint.msg !== null ?
                `<div class="pre-div-content">
                    ${isMonitorLineAnEvent && this.isDataToBeRendered ? collapsableObjViewerLine : dataToPrint.msg}
                </div>`
                :
                ''
            )
        ;

        if (dataToPrint.data === null) {
            // No JSON 'data' was passed in to Print. What about 'msg'...?
            dataToPrint.msg !== null ?
                this.doPrintMonitorData(cumulatedData, null)
                :
                this.doPrintMonitorData(`<div class="pre-div-content">ERROR...? No 'msg' or 'data' were provided!</div>`, null)
            ;
        } else {
            // We have JSON 'data' for sure! 'msg' is not important - if null was aready converted to '':
            this.doPrintMonitorData(cumulatedData, dataToPrint.data);
        }
    }

    /**
     * To print BOTH 'data.msg' and 'data.obj' of 'MonitorData'
     *
     * We have here some heavy, thoughtful DOM Manipulation!
     *
     * MIND YOU:
     * Atribution of new content into <div id="obj-viewer-[contentIdx]"<pre></pre></div> is still to come,
     * when User clicks on the Monitor line button.
     * => before changing DOM, remove all previous listeners to these buttons
     * - they would prevail on DOM, even when the attached element(s) is/are replaced,
     * IF there were any code heritance/reference pointing to any of them.
     *
     * AND, obviously, after our 'objViewerPreTagContent' @ 'openObjViewer()' has refreshed content, new to DOM,
     * we have to put the just finished rendering NEW buttons to work, after DOM has settled down:
     *
     * @param htmlStr - the 'data.msg', resumed string, to print on the Monitor
     * @param jsonData - the 'data.obj' stringified event data, to print beautified on the Monitor
     */
    private doPrintMonitorData(htmlStr: string, jsonData: any) {
        // console.error('Each Logged Event => "doPrintMonitorData()" more than ONCE...?');

        let currentTriggerBtns: NodeListOf<HTMLElement> = this.componentDOM.querySelectorAll('.obj-viewer-trigger');
        currentTriggerBtns.forEach( (btn: HTMLElement) => btn.removeEventListener('click', () => this.openObjViewer(btn.dataset.target)) );

        // Refresh DOM with NEW content:
        this.contentHTMLmsg = jsonData ? htmlStr + this.beautifiedJSONPrinting(JSON.stringify(jsonData, undefined, 4)) : htmlStr;

        setTimeout(() => {
            currentTriggerBtns = this.componentDOM.querySelectorAll('.obj-viewer-trigger');
            currentTriggerBtns.forEach(
                (btn: HTMLElement) => btn.addEventListener('click', (evt: MouseEvent) => {
                    evt.stopImmediatePropagation();
                    this.openObjViewer(btn.dataset.target);
                })
            );
        }, 0);
    }

    /**
     * Always pull DATA monitor content down, when overflow starts
     *
     * 'delta' tune distance (to go up) is max height (in px)
     * of the blocks of writing that are coming in
     *
     */
    private pullBottomMonitorMessagesDown() {
        const height = this.preContent.clientHeight;
        const scrollHeight = this.preContent.scrollHeight;
        const vpHeight = scrollHeight - height;

        const delta = 600;
        const isScrolledToBottom = vpHeight <= this.preContent.scrollTop + delta;
        if (isScrolledToBottom) {
            this.preContent.scrollTop = vpHeight;
        }
    }

    /**
     * After Monitor has finished the busy rendering of logged event's greenish lines,
     * coming with the light speed ;-),
     * once User clicks on a Monitor's line button,
     * send back the HTML string just to (beautified) print the 'data.obj's stringified event data
     * immediately under the clicked line
     *
     * @param nEvent - the logged event index, on the 'ThreeJSloggerService.dataBook' array, to a beautified print on the Monitor
     */
    private dataObjBeautifiedHTML(nEvent: number): { html: string; hasConsoleLogItProp: boolean; fromWhere: ComponentBeingConsoledType } {
        const loggedEvt: LoggEventData = this.ThreeJSloggerService.getLoggBookEvt(nEvent);

        let dataObjToPrint = `ERROR: was supposed to be the HTML of the #${nEvent} EVENT's stringified "data" prop object`;
        // console.log('Current logg book of 3D Events:', this.ThreeJSloggerService.getLoggBook());

        if (loggedEvt.hasOwnProperty('data')) {
            dataObjToPrint = this.beautifiedJSONPrinting(JSON.stringify(loggedEvt.data, undefined, 4));
            // tslint:disable-next-line: max-line-length
            return { html: dataObjToPrint, hasConsoleLogItProp: loggedEvt.data.hasOwnProperty('consoleLogIt'), fromWhere: loggedEvt.data.where };

        } else {
            console.error(`ERROR: can\'t get the #${nEvent} Event from ThreeJSloggerService Logg Book!`);
            return { html: dataObjToPrint, hasConsoleLogItProp: false, fromWhere: null };
        }

    }

    /**
     * Many thanks to 'user123444555621' @ StackOverFlow (99.723 of reputation!)
     * that develop this JSON.stringify() with syntax highlighting
     * https://stackoverflow.com/a/7220510/2816279
     *
     * Has been updated to better serve this specific App purpose
     *
     * @param json - the JSON data to print @ HTML <pre> tag:
     */
    private beautifiedJSONPrinting(json: string) {
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');

        const htmlCodeBlockToReturn = json.replace(

            /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g

            , (match) => {

                let htmlClass = 'number';
                if (/^"/.test(match)) {
                    if (/:$/.test(match)) {
                        htmlClass = 'key';
                    } else {
                        htmlClass = 'string';
                    }
                } else if (/true/.test(match)) {
                    htmlClass = 'boolean-true';
                } else if (/false/.test(match)) {
                    htmlClass = 'boolean-false';
                } else if (/null/.test(match)) {
                    htmlClass = 'null';
                }

                const innerHtmlValue = match.replace(/"/g, '');
                return `<span class="${htmlClass}" title="${innerHtmlValue}">${innerHtmlValue}</span>`;
            }
        );

        return htmlCodeBlockToReturn;
    }

    /**
     * Customize HTML tags,
     * AFTER having the beautified JSON settled down on DOM.
     */
    private objViewerAddRadioBtns(containerTag: HTMLElement, loggedEvtIdx: number) {
        // Need to replace 1st occurrence of
        //     <span class="key" title="consoleLogIt:">consoleLogIt:</span>
        //     <span class="boolean-true" title="true">true</span>
        // by our super cool hiper-styled radio buttons's CONTAINER, classed as '.console-log-it-wrapper':
        //     <span class="console-log-it-wrapper">
        //         <input type="radio" id="console-log-it-${loggedEvtIdx}-on"
        //             class="console-log-it when-on" name="radio-${loggedEvtIdx}" value="true" on-checked
        //         >
        //         <label for="console-log-it-${loggedEvtIdx}-on" class="text-uppercase">On</label>
        //
        //         <input type="radio" id="console-log-it-${loggedEvtIdx}-off"
        //             class="console-log-it when-off" name="radio-${loggedEvtIdx}" value="false" off-checked
        //         >
        //         <label for="console-log-it-${loggedEvtIdx}-off" class="text-uppercase">Off</label>
        //     </span>
        // // ----------------------------------------------------------------------------
        // We are assuming DOM is settled down and we'll find the HTMLElemnt to replace
        // ----------------------------------------------------------------------------
        //
        // const simpleBooleanHTML: HTMLElement = containerTag.querySelector('[title=consoleLogIt:]');
        // Don't know why, but it's not working... maybe the title string? Having ':' at the end?
        // Well.. this is very well located => select the first element in the container, and then the first again.
        const simpleBooleanHTML: HTMLElement = containerTag.querySelector('*').querySelector('*');
        // REMMEBER we don't know if next one is '.boolean-true' or '.boolean-false'...
        // What we know is that we want the SIBLING (next one), of the just found 'simpleBooleanHTML'! ;-)
        const ourSimpleBooleanValueHTML = simpleBooleanHTML.parentNode.querySelector('span + span');
        // And now we know:
        const selectedOnOff = ourSimpleBooleanValueHTML.textContent;    // true || false
        // tslint:disable-next-line: variable-name
        const selectedOnOff_NOT = !selectedOnOff;

        const ourSpanContainer: HTMLElement = document.createElement('span');
        ourSpanContainer.innerHTML = `
            <input type="radio" id="console-log-it-${loggedEvtIdx}-on"
                class="console-log-it when-on" name="radio-${loggedEvtIdx}" value="true" true-checked
            >
            <label for="console-log-it-${loggedEvtIdx}-on" class="text-uppercase">On</label>

            <input type="radio" id="console-log-it-${loggedEvtIdx}-off"
                class="console-log-it when-off" name="radio-${loggedEvtIdx}" value="false" false-checked
            >
            <label for="console-log-it-${loggedEvtIdx}-off" class="text-uppercase">Off</label>
        `.replace(`${selectedOnOff}-checked`, 'checked').replace(`${selectedOnOff_NOT}-checked`, '');
        // ;-)  <=  have to get rid of 'true-checked' or 'false-checked', and replace the ONE staying by 'checked' attr.

        ourSpanContainer.classList.add('console-log-it-wrapper');
        // And that's it! Alive 'n kicking!

        // Guys upsatairs can now lower the panel down:
        setTimeout(() => ourSimpleBooleanValueHTML.parentNode.replaceChild(ourSpanContainer, ourSimpleBooleanValueHTML), 0);
    }

}
