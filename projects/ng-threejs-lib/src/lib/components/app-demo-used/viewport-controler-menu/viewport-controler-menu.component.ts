import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectionStrategy } from '@angular/core';

import { CookieHandlerService } from '../../../services/app-demo/cookie-handler.service';

import { NG3DCookie, VpBtnValueType } from '../../../ng-threejs-lib.interfaces';

@Component({
    selector: 'ng3d-viewport-controler-menu',
    templateUrl: './viewport-controler-menu.component.html',
    styleUrls: ['./viewport-controler-menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewportControlerMenuComponent implements OnInit {

    // 'btnActive' index is either filled by an Input() (button that is classed as '.active', on an initial default installation state)
    // or by clicking (Ng event) on any button (see Template conditions)
    @Input('isActiveBtn') set activeBtn(btnToActivate: number) { this.btnActive = btnToActivate; }
    btnActive: number;

    @Output() maximize = new EventEmitter();
    @Output() restore = new EventEmitter();
    @Output() halfHeight = new EventEmitter();
    @Output() minimize = new EventEmitter();

    constructor(private cookieHandlerService: CookieHandlerService) { }

    ngOnInit() {
    }

    setViewportSizeBtnCookie(value: VpBtnValueType) {
        this.cookieHandlerService.setCookieProp('vpBtn', value);
    }


}
