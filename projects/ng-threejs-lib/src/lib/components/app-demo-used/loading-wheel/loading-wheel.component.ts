import { Component, OnInit, OnChanges, AfterViewInit, Input, ChangeDetectionStrategy, SimpleChanges, ElementRef } from '@angular/core';

import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

/**
 * Just to not TSLint error - App's bootstrap will load **JQuery**...
 */
declare var $: any;

@Component({
    selector: 'ng3d-loading-wheel',
    templateUrl: './loading-wheel.component.html',
    styleUrls: ['./loading-wheel.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
/**
 * We have here 2 distinct methodologies to start/stop a loading wheel:
 *
 * OPTION 1)
 * either commanded by the TS of the HTML Template were you through this component
 * (you just specify an @Input() on the template named 'startLoadingWheel')
 *
 * OR
 *
 * OPTION 2)
 * it's automatically comanded by the loading of the component you passed in
 * as a SINGLE Input() on the template: 'domTagToBeLoaded', where 'domTagToBeLoaded' must be a sibbling CSS syntax tag of this component
 * i.e., '.container-class', '#some-id' or even a 'button' component, browser's native or not.
 *
 */
export class LoadingWheelComponent implements OnInit, OnChanges, AfterViewInit {

    // Our targeted container, that will only DOM exist after its contents are loaded - for now it's a string (NOT an HTMLElement..... yet!)
    @Input() private domTagToBeLoaded: string;
    // Direct orders to start/stop loading wheel, independently of loading native times:
    @Input() private startLoadingWheel: boolean;
    // Just a label, for us to Debug, saying where the loading wheel is hosted:
    @Input() private hostId: string;

    constructor(
        private loadWheelDOM: ElementRef,
        private consoleLogService: ConsoleLogService
    ) { }

    ngOnInit() {
        // If parent does not specify it (having 'undefined', 'null' or 'true', as the 'domTagToBeLoaded' Inputted() value),
        // and 'startLoadingWheel' is not telling us not to, we should START loading wheel RIGHT NOW - when this component's DOM is ready:
        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('LoadingWheelComponent');
        if (hasConsoleLog) { console.error('Inputted HTML tag exists on DOM...?', $(this.domTagToBeLoaded)); }

        if (this.startLoadingWheel !== false && $(this.domTagToBeLoaded)) {
            this.setLoadingWheelTo('on', 500);
            // MIND YOU there's No DOM problems, in jQuerying an element that still does not exists - DON'T DO IT WITH JS...
        }
    }

    ngOnChanges(propsChanged: SimpleChanges) {
        const tagIsLoaded = propsChanged.domTagToBeLoaded;
        const isToStart = propsChanged.startLoadingWheel;

        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('LoadingWheelComponent');
        if (hasConsoleLog) { console.warn('The State of the Input()s being changed:', Object.assign({}, propsChanged)); }

        if (isToStart && isToStart.firstChange) {
            if (hasConsoleLog) {
                console.error(`"isToStart" loading wheel @ "${this.hostId.toUpperCase()}" ON ngOnInit() of HOST`, isToStart.currentValue);
            }
        }
        if (isToStart && !isToStart.firstChange) {
            // We have settled down.
            // All parent components, as so the one which is hosting this loading whell component, have done ngOnInit()...
            // Data will start flowing and we must see what is to done, regarding showing, or not, the loading wheel:
            if (hasConsoleLog) {
                console.error(`"isToStart" loading wheel @ "${this.hostId.toUpperCase()}"`, isToStart.currentValue);
            }

            if (isToStart.currentValue) {
                this.setLoadingWheelTo('on', 500);
            } else {
                this.setLoadingWheelTo('off', 800);
            }
        }

    }

    /**
     * Methods involved, when component's DOM is 'ready', before 'loaded':
     *
     * Control 'domTagToBeLoaded' sibbling tag of this component, while it hasn't finish (fetch and) rendering
     */
    ngAfterViewInit() {

        if (this.domTagToBeLoaded) {
            this.waitForElementToLoad( this.domTagToBeLoaded, () => {
                this.setLoadingWheelTo('off', 800);
            });
        }
    }

    /**
     * Wait that DOM recognizes 'classSelector' as existent.
     * When that happens... 'classSelector' is definetey loaded - we only had it 'ready', so far at the components life cycles.
     *
     * When this happens, you can callback so caller can turn OFF the 'loading wheel'
     *
     * @param classSelector - the class to be loaded
     * @param callback - get back to the caller function, signaling the tag to be loaded has finished its loading
     */
    waitForElementToLoad(domTagSelector: string, callback: any) {
        const keepLooking = setInterval(() => {
            if ($(domTagSelector)) {
                clearInterval(keepLooking);
                callback();    // <= now we can fade it out; our string became a DOM element!
            }
        }, 100);
    }

    // ---------------------
    // AUX methods:
    // ---------------------

    /**
     * With whatever DOM manipulation code, and according to the written CSS... turn the loading wheel ON/OFF!
     *
     * 'whatever'... NO!
     * You can only do this with JQuery - JS will throw an error:
     * We gain a tremendous ammount of time, and 100% independent of Angular's mounting/dismounting,
     * by jQuerying an element that still does not exists!
     * (check next method is called on still on Angular's 'ngOnInit()' life cycle hook... On the second DOM is fully ready... PUMBA! ;-)
     *
     * @param whatState - to turn loading wheel 'on' or 'off'
     * @param timeToFade - if used on this method's code, represents a fade ('in' or 'out') within this time value
     */
    setLoadingWheelTo(whatState: 'on' | 'off', withTimeToFade: number) {
        const loadingWheelContainer: HTMLElement = this.loadWheelDOM.nativeElement.querySelector('.headers-preloader');
        // But we'll use JQUery selector - only JQuery has the cool fade thing!

        if (loadingWheelContainer) {
            if (whatState === 'on') {
                $(loadingWheelContainer)
                    .css('display', 'flex')    // <= Due to CSS coded flex alignement, we do NOT want a final display of 'block'
                    .hide()
                    .fadeIn(withTimeToFade)
                ;
            } else {
                $(loadingWheelContainer).fadeOut(withTimeToFade);
            }
        }
    }
}
