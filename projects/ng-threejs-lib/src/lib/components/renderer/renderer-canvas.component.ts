import { OnInit, AfterViewInit, Component, ElementRef, HostListener, Input, ViewChild } from '@angular/core';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../services/app-demo/console-log.service';


@Component({
    selector: 'ng3d-renderer-canvas',
    templateUrl: './renderer-canvas.component.html',
    styleUrls: ['./renderer-canvas.component.scss']
})
export class RendererCanvasComponent implements OnInit, AfterViewInit {

    @ViewChild('canvas', { static: true }) private canvasRef: ElementRef;

    @Input() enableWebGl = true;
    @Input() enableCss3d = true;


    constructor(
        private rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) { }

    ngOnInit() {
        this.onResize = this.onResize.bind(this);
    }

    ngAfterViewInit() {

        if (this.consoleLogService.isThisBeingConsoled('RendererCanvasComponent')) {
            console.warn('CREATED DOM «canvas /»:', this.canvas);
        }

        // Now let's render it:
        this.rendererService.initialize(this.canvas, this.enableWebGl, this.enableCss3d);
    }

    /**
     * The render pane on which the scene is rendered.
     * Currently, only the WebGL renderer with a canvas is used in this
     * implementation, so this property will always be an ElementRef to the
     * underlying <canvas> element.
     *
     * @example This property can be used to restrict the orbit control (i.e. the
     * area which is listened for mouse move and zoom events) to the rendering pane
     */
    public get renderPane(): ElementRef {
        return this.canvasRef;
    }

    private get canvas(): HTMLCanvasElement {
        return this.canvasRef.nativeElement;
    }

    @HostListener('window:resize', ['$event'])
    public onResize(event: Event) {
        this.rendererService.resize(this.canvas, '100%');
    }

}
