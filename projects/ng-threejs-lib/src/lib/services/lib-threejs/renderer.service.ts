import { Injectable, QueryList } from '@angular/core';

import * as THREE from 'three';
import { CSS3DRenderer } from 'three/examples/jsm/renderers/CSS3DRenderer';

import { SceneComponent } from '../../modules/scene/scene.component';
import { CameraDirective } from '../../modules/camera/camera.directive';

import { StatsService } from './stats.service';
import { LoggerService } from '../app-demo/logger.service';
import { LoggEventData, EventData, SomeNgChanges } from '../../ng-threejs-lib.interfaces';
import { ConsoleLogService } from '../app-demo/console-log.service';

@Injectable({
    // Next enables us to NOT declare this Service in 'ng-threejs-lib.module.ts' under "@NgModule({ ..., providers: [RendererService] })"
    // => enables "Tree Shaking" - service won't be on build, if not used/needed anymore
    providedIn: 'root'
})
export class RendererService {

    private init = false;

    private scene: SceneComponent;
    private camera: CameraDirective<any>;
    private enableWebGl: boolean;
    private enableCss3d: boolean;
    private webGlRenderer: THREE.WebGLRenderer;
    // TODO:
    private css3dRenderer: CSS3DRenderer;

    private aspect: number;

    constructor(
        private statsService: StatsService,
        private loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) { }

    public setCamera(camera: CameraDirective<any>) {
        camera.updateAspectRatio(this.aspect);
        this.camera = camera;
        // console.error('Camera:', this.camera);
    }

    public getCameraDirective() {
        return this.camera;
    }

    public setScene(scene: SceneComponent) {
        // console.error('Scene:', this.scene);
        this.scene = scene;
    }

    public getScene() {
        return this.scene;
    }

    public getDOMpointsOfLight() {
        // const DOMPointsOfLight: QueryList<PointOfLightComponent> = this.scene.DOMpointsOfLight;
        // const pointsOfLight = [];
        // DOMPointsOfLight.forEach(polDOMInstance => pointsOfLight.push(polDOMInstance));
        // return pointsOfLight;

        // return DOMPointsOfLight;

        // Upper are DOM Points of Light, from which we can get THreeJS OBJECT, which is, in fact, our Point of Light.
        // Here, at 3D Rendering, makes no sense to grab DOM ELements - will be logical in the Angular DEMO, but not here
    }

    public render(toLoggEvt: LoggEventData | string, paramToDelete?: any) {
        // 2 Types because of compat with "old" code - we will refactor bit by bit!
        // In the end, when all of the components have suffered the refactoring, we should have ONLY 'LoggEventData'
        // So:
        const toLogEvtMSG = toLoggEvt as string;
        const toLogEvtOBJ = toLoggEvt as LoggEventData;

        if (this.init && this.scene && this.camera) {
            if (this.enableWebGl) {
                this.webGlRenderer.render(this.scene.getObject(), this.camera.camera);
            }
            if (this.enableCss3d) {
                this.css3dRenderer.render(this.scene.getObject(), this.camera.camera);
            }
            this.statsService.update();

            // ----------------------------------------------------------------
            // NO NEED (? Sure?) - duplicated information, just sent 1/1000 sec before.
            // Yeap.... but then the finish of EACH solicited rendering, might not happen sync...
            // previous one might not be exactly what this one reports as finished!
            // Besides we always need to have the finish rendering TIME.
            // ----------------------------------------------------------------
            // Well send it, but with reduced 'data' and NO console.log
            // ----------------------------------------------------------------
            // tslint:disable: variable-name
            const commingText = toLogEvtOBJ.hasOwnProperty('msg') ? toLogEvtOBJ.msg : toLogEvtMSG;
            const echoMsg_0 = `RendererService, FINISH ${commingText}`;

            const textToAdd = toLogEvtOBJ.hasOwnProperty('msg') ? commingText.split(',').slice(1) : commingText;
            const echoMsg_1 = `RendererService, FINISH rendering @ ${textToAdd}`;

            // Next it's coming from previous logged event => it's alreay EventData:
            const data: EventData = toLogEvtOBJ.hasOwnProperty('data') ?  toLogEvtOBJ.data : (paramToDelete || null);
            // const dataSmaller: EventData = Object.assign({}, data);
            // delete dataSmaller.consoleLogIt;
            // delete dataSmaller.scalarValues;
            // delete dataSmaller.operation;
            // dataSmaller.where = data ? data.where : null;
            //
            // OR, FASTER => assign it directly!
            // MIND YOU we are cheating A LOT on TypeScript, here! ;-)
            // But everything goes, in name of "speed"! ;-) Object manipulation is toooooooo expensive.
            const dataSmaller = Object.assign({}, { where: data ? data.where : null } ) as EventData;
            // if (this.consoleLogService.rendererService) { console.warn(echoMsg_0, data); }
            // this.loggerService.logg3Devent('RendererService', null, echoMsg_1, null, null, false, false);
            //
            // JUST Logg it, no console and a very brief 'data':
            this.loggerService.addLog( {msg: echoMsg_1, data: dataSmaller} );
        }
    }

    public initialize(thisCanvas: HTMLCanvasElement, enableWebGl: boolean, enableCss3d: boolean) {
        this.enableWebGl = enableWebGl;
        this.enableCss3d = enableCss3d;
        let objToLogg: THREE.WebGLRenderer | CSS3DRenderer;

        if (enableWebGl) {
            // TODO: Multiple renderers
            this.webGlRenderer = new THREE.WebGLRenderer({
                canvas: thisCanvas,
                antialias: true,
                alpha: true
            });
            this.webGlRenderer.setPixelRatio(window.devicePixelRatio);
            this.webGlRenderer.setSize(thisCanvas.clientWidth, thisCanvas.clientHeight, true);

            // TODO: props
            this.webGlRenderer.shadowMap.enabled = false;
            this.webGlRenderer.shadowMap.autoUpdate = false;
            this.webGlRenderer.shadowMap.type = THREE.PCFSoftShadowMap;
            this.webGlRenderer.setClearColor(0x000000, 0);
            this.webGlRenderer.autoClear = true;
            thisCanvas.style.zIndex = '2';

            objToLogg = this.webGlRenderer;
        }

        if (enableCss3d) {
            this.css3dRenderer = new CSS3DRenderer();
            this.css3dRenderer.setSize(window.innerWidth, window.innerHeight);
            this.css3dRenderer.domElement.style.position = 'absolute';
            this.css3dRenderer.domElement.style.top = '0';
            this.css3dRenderer.domElement.style.zIndex = '-1';
            thisCanvas.parentElement.appendChild(this.css3dRenderer.domElement);

            objToLogg = this.css3dRenderer;
        }
        // ------------------------------ END

        this.updateChildCamerasAspectRatio(thisCanvas);

        this.init = true;

        const echoMsg = 'RendererService, INITIALIZATION of webGlRenderer | css3dRenderer';
        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('RendererService');
        // tslint:disable-next-line: max-line-length
        const objToLoggCSSprops = `${objToLogg.domElement.getBoundingClientRect().height} x ${objToLogg.domElement.getBoundingClientRect().width}`;
        const scalarValues: Array<SomeNgChanges> = [
            { whatProp: 'type', fromValue: null, toValue: objToLogg.domElement.nodeName },
            { whatProp: 'id', fromValue: null, toValue: `${objToLogg.domElement.id} | ${objToLogg.domElement.className}` },
            { whatProp: 'size', fromValue: null, toValue: objToLoggCSSprops },
        ];
        // Logg it:
        const loggEvt: LoggEventData = this.loggerService.logg3Devent(
            'RendererService', 'intialize', echoMsg, this.camera, scalarValues, hasConsoleLog
        );
        // Render it:
        this.render(loggEvt);
    }


    public resize(canvas: HTMLCanvasElement, size: string) {
        // console.warn(canvas, size) // size is always coming as '100%'
        if(!canvas) { return null }   // on '3.0-obj-stream-loader-demo'

        canvas.style.width = size;
        canvas.style.height = size;
        canvas.style.border = 'none';
        const width = canvas.clientWidth;
        const height = canvas.clientHeight;

        if (this.enableWebGl) {
            this.webGlRenderer.setSize(width, height, true);
        }

        if (this.enableCss3d) {
            this.css3dRenderer.setSize(width, height);
        }
        this.updateChildCamerasAspectRatio(canvas);

        const echoMsg = `RendererService, RESIZE the «canvas /» to "${width} x ${height}`;
        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('RendererService');

        const objToLoggCSSprops = `${canvas.getBoundingClientRect().height} x ${canvas.getBoundingClientRect().width}`;
        const scalarValues: Array<SomeNgChanges> = [
            { whatProp: 'type', fromValue: null, toValue: canvas.nodeName },
            { whatProp: 'id', fromValue: null, toValue: `${canvas.id} | ${canvas.className}` },
            { whatProp: 'size', fromValue: null, toValue: objToLoggCSSprops },
        ];
        // Logg it:
        const loggEvt: LoggEventData = this.loggerService.logg3Devent(
            'RendererService', 'intialize', echoMsg, canvas, scalarValues, hasConsoleLog
        );
        // Render it:
        this.render(loggEvt);
    }

    public clear3Dstage() {
        console.error('***********************************');
        console.log('clear3Dstage() still TODO! ;-) Have to eat and sleep... also!');
        console.error('***********************************');
        // ---------------------------------
        // Partially (3D Actors, Points of Light, etc.)
        // is already done here:
        // ---------------------------------
        // projects\ng-threejs-lib\src\lib\modules\scene\object-3d.directive.ts
        // ---------------------------------
        // @ 'ngOnDestroy()' method
        //
        // But there's others than 'object-3d.directive's objects - i.e. cameras!
    }

    private calculateAspectRatio(canvas: HTMLCanvasElement) {
        const height = canvas.clientHeight;
        if (height === 0) {
            return 0;
        }
        this.aspect = canvas.clientWidth / canvas.clientHeight;
    }

    private updateChildCamerasAspectRatio(canvas: HTMLCanvasElement) {
        this.calculateAspectRatio(canvas);
        if (this.camera) {
            this.camera.updateAspectRatio(this.aspect);
        }
    }

}
