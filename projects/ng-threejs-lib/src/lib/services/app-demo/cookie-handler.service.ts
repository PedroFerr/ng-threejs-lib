import { Injectable } from '@angular/core';

import {
    NG3DCookie, NG3DCookieInit, NG3DCookieArgsType,
    VpBtnValueType, ComponentBeingConsoledType
} from '../../ng-threejs-lib.interfaces';

@Injectable({
    providedIn: 'root'
})
export class CookieHandlerService {

    constructor() { }

    /**
     * To make it more "elegant" and not that much readeable (for no specific reason)
     * we will 'obfuscate' the stored cookie (it's a string, btw).
     * Obviously we have to 'de-obfuscate' it, when getting its values for the App's code.
     *
     * We'll use a Base64-encoded ASCII string from a binary string
     * (i.e., a String object in which each character in the string is treated as a byte of binary data).
     * Check it here: https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/btoa
     *
     * Method to obfuscate is 'btoa()', and 'atob()' to reverse the aplied obfuscate.
     *
     * MIND YOU:
     * The stored base64 encoded value is MUCH bigger than the simple JSON of the object,
     * with a minimu of AT LEAST 133% the size of its source - note each Base64 digit represents exactly 6 bits of data
     * Check it here: https://developer.mozilla.org/en-US/docs/Glossary/Base64, @ 'Encoded size increase'
     *
     * NOTE:
     * Measurements are being made, by UNCOMMENTing the Method's 'console.error'.
     * So far so good: current cookie strigified has 213 bytes, and the atob() of it, 284 bytes length.
     *
     * @param thisProp - the property to set on 'NG3D' local storage's cookie
     * @param thisValue - the value to set on the @thisProp property.
     */
    setCookieProp(thisProp: NG3DCookieArgsType, thisValue: VpBtnValueType | Array<ComponentBeingConsoledType> | boolean) {
        let decodedAppCookie: NG3DCookie | null = this.checkCookieExists();
        const isItPlainString: boolean = thisProp.indexOf('monitorSwitches.') === -1;
        // tslint:disable-next-line: max-line-length
        const thisRealProp: Array<string> | string = !isItPlainString  ? thisProp.split('.') as Array<string> : thisProp as string;

        if (decodedAppCookie) {
            if (!isItPlainString) {
                decodedAppCookie[thisRealProp[0]][thisRealProp[1]] = thisValue;
            } else {
                decodedAppCookie[thisProp] = thisValue;
            }
        } else {
            decodedAppCookie = Object.assign({}, NG3DCookieInit, { [thisProp]: thisValue });
        }
        // Set it:
        const notHumanReadableVar: string = btoa(JSON.stringify(decodedAppCookie));
        localStorage.setItem('NG3D', notHumanReadableVar);
    }

    getCookieProp(thisProp: NG3DCookieArgsType): VpBtnValueType | Array<ComponentBeingConsoledType> | boolean {
        const decodedAppCookie: NG3DCookie | null = this.checkCookieExists();
        const isItPlainString: boolean = thisProp.indexOf('monitorSwitches.') === -1;
        // tslint:disable-next-line: max-line-length
        const thisRealProp: Array<string> | string = !isItPlainString  ? thisProp.split('.') as Array<string> : thisProp as string;
        let cookieToReturn = null as VpBtnValueType | Array<ComponentBeingConsoledType> | boolean;

        if (decodedAppCookie) {
            if (!isItPlainString) {
                cookieToReturn = decodedAppCookie[thisRealProp[0]][thisRealProp[1]];
            } else {
                cookieToReturn = decodedAppCookie[thisProp];
            }
        }
        return cookieToReturn;
    }

    private checkCookieExists(): NG3DCookie {
        const humanReadableCookie: string = localStorage.getItem('NG3D') ? atob(localStorage.getItem('NG3D')) : null;
        return humanReadableCookie ? JSON.parse(humanReadableCookie) : null;
    }
}
