import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';

import {
    LoggEventData, EventData, EventDataOperationType, SomeNgChanges, ComponentBeingConsoledType
} from '../../ng-threejs-lib.interfaces';
import { returnNowsTime } from '../../util/js/returnNowsTime';

@Injectable({
    providedIn: 'root'
})
export class LoggerService {

    private loggBook = [] as Array<LoggEventData>;
    public isEventToBeSaved = true;

    public newEvtLogged$ = new Subject<LoggEventData | string>();

    constructor() { }

    logg3Devent(
        // that: any,    // The 'this.' context of who is calling this const (Method)
        // No Type for it, so far: https://www.typescriptlang.org/docs/handbook/functions.html#this-parameters
        thatWhere: ComponentBeingConsoledType,
        thatOperation: EventDataOperationType,
        thatMsg: string,
        thatObj: object,
        thatScalarValues: Array<SomeNgChanges>,
        thatHasConsoleLog: boolean,
    ): LoggEventData {

        // Console.log it, on browser's Inspector:
        if (thatHasConsoleLog) { console.warn(thatMsg, thatObj ? thatObj : ''); }

        // Logg it:
        const loggEvt: LoggEventData = this.loggEvtConstructor(
            thatWhere,
            thatOperation,
            thatMsg,
            thatObj,
            thatScalarValues,
            thatHasConsoleLog,
        );
        this.addLog(loggEvt);

        // Return it, in case we need to re-render, after, using another Service (RendererService):
        return loggEvt;
    }

    addLog(loggedObj: LoggEventData | string) {    // 2 Types because of compat with "old" code - we will refactor bit by bit!
    // In the end, when all of the components have suffered the refactoring, we should have ONLY 'LoggEventData'
    // So:
        const loggedObjSTR = loggedObj as string;
        const loggedObjOBJ = loggedObj as LoggEventData;
    // -------------------------------------------------------------
        const loggedObjMsg: string = loggedObj.hasOwnProperty('msg') ? loggedObjOBJ.msg : loggedObjSTR;
        const loggedObjData: EventData = loggedObj.hasOwnProperty('data') ? loggedObjOBJ.data : null;

        const timeStamp: string = returnNowsTime();
        let markConsoleLogIsTrue = '-';

        if (loggedObjData) {
            if (loggedObjData.consoleLogIt === true) { markConsoleLogIsTrue = '<mark></mark>'; }
            loggedObjData.when = timeStamp;
        }
        const loggedMsg = `@ ${timeStamp}  ${markConsoleLogIsTrue} ${loggedObjMsg}`;

        // Finally, we are all set up:
        const loggedEvt: LoggEventData = {msg: loggedMsg, data: loggedObjData};
        // Observe it:
        this.newEvtLogged$.next(loggedEvt);

        // Save it IF 'monitor-debug' component is NOT turned OFF (@ onSwitchingMonitorPower()),
        // purging the 'loggedObjData.onWhat' prop, an object that can be massive - we certainly don't want to SAVE IT,
        // thought User might be able to CONSOLE log it, on browser's Inspector and on the fly,
        // by catching 'newEvtLogged$' and subscribing to it.
        // ===========================================================
        // Object 'loggedObjData.onWhat' NEVER gets saved ANYWHERE
        // ===========================================================
        if (this.isEventToBeSaved) {
            this.loggBook.push({
                msg: loggedMsg,
                data: this.render3DobjectPropOnWhat(Object.assign( {}, loggedObjData), markConsoleLogIsTrue)
            });
        }
        // ===========================================================
    }

    getLoggBook(): Array<LoggEventData> {
        return this.loggBook;
    }

    getLoggBookEvt(idx: number): LoggEventData {
        return this.loggBook[idx];
    }

    getLoggBookIdxFilterWhereEvts(onWhere: ComponentBeingConsoledType): Array<number> {
        const filterWhereEvts = this.loggBook.filter((evt: LoggEventData, idx: number) => evt.data.where === onWhere);
        return filterWhereEvts.map((evt: LoggEventData) => this.loggBook.indexOf(evt));
    }

    /**
     * Clear the 'loggBook', from some UX moment.
     * There's a proper Method to clear it, from a Component's Method.
     *
     */
    emptyLoggBook(): boolean {
        const confirmMsg = 'Are you sure\nyou want to DELETE forever\nALL the logged 3D Events kept so far....?';

        // console.error('Logg Book:', this.getLoggBook());

        if (this.loggBook.length > 0) {
            const userIsConfirmingDelete: boolean = window.confirm(confirmMsg);

            if (userIsConfirmingDelete) {
                if (this.doEmptyLoggBook()) {
                    window.alert(
                        'You successfuly deleted ALL the logged 3D Events, kept so far.\n\nWe hope that\'s exactly what you wanted! ;-)'
                    );
                    // console..error('Logg Book:', this.getLoggBook());
                    return true;
                } else {
                    window.alert(
                        'ERROR: something failed...\n' +
                        '"Logg Book 3D Events" was NOT emptied!\n\nHave no explanation and there\'s anything else we can do...'
                    );
                    return false;
                }
            } else {
                return false;
            }

        } else {
            // In fact, nothing was, or was to be, done:
            this.clearConsole();
            return true;
        }

    }

    /**
     * Clear the 'loggBook' from some Angular Component's Method.
     *
     */
    clear3DloggedEvents() {
        if (!this.doEmptyLoggBook()) {
            throw new Error('ERROR: "Logg Book 3D Events" was NOT emptied!');
        }
    }

    /**
     * Clear the 'loggBook' content - might seems simple, but it's tricky!
     *
     * MIND YOU
     * each array's item could be pointing to MASSIVE Object.keys(...) length!
     * For instances, our '3 floor Office' Model, loaded from a 105 Mb OBJ file, is composed of 6.000 + child objects!
     * And each one... bla´, blá, blá...
     *
     * So:
     * 1) "thisArray = []" => assigns a reference to a new array to a variable,
     * while any other references are unaffected - references to the CONTENTS of the previous array are still kept in memory.
     *
     * 2) "thisArray.length = 0" => deletes everything in the array, which does hit also any references to it.
     *
     * Definitely we desperately want want the 2)nd one!
     */
    private doEmptyLoggBook(): boolean {
        this.loggBook.length = 0;
        this.clearConsole();
        return true;
    }

    // getLoggBook(): Observable<Array<string>> {
    //     const LoggBookObs = new Observable<Array<string>>(
    //         observer => observer.next(this.loggBook)
    //     );
    //     return LoggBookObs;
    // }

    // public getLoggBook = new Observable<Array<string>>(
    //     (observer) => {
    //         observer.next(this.loggBook);

    //         return {
    //             unsubscribe() {}
    //         };
    //     }
    // );

    /**
     * Insert trigger properties, that constitute an exception,
     * to the ones generally each 3D component is sending to DEMO components
     *
     */
    private loggEvtConstructor(
        thatWhere: ComponentBeingConsoledType,
        thatOperation: EventDataOperationType,
        thatMsg: string,
        thatObj: object,
        thatScalarValues: Array<SomeNgChanges>,
        thatHasConsoleLog: boolean,
    ): LoggEventData {

        const loggEvt: LoggEventData = {
            msg: thatMsg,
            data: {
                consoleLogIt: thatHasConsoleLog,
                where: thatWhere,
                operation: thatOperation,
                scalarValues: thatScalarValues,
                onWhat: thatObj
            }
        };

        switch (thatWhere) {
            case 'SphereMeshComponent':
            case 'CubeMeshComponent':
            case 'CylinderMeshComponent':
            case 'TorusMeshComponent':
                if (thatOperation === 'create') { loggEvt.data.isLoading = true; }
                if (thatOperation === 'ng-renderer') { loggEvt.data.isLoading = false; }
                break;

            case 'ObjLoaderComponent':
                if (thatOperation === 'intialize') { loggEvt.data.isLoading = true; }
                break;
            case 'Object3DLazyDirective':
                if (thatOperation === 'create') { loggEvt.data.isLoading = false; }
                break;

            default:
                break;
        }

        return loggEvt;
    }

    /**
     * I think we can NOT use this most cool feature of having rendered, on the Monitor,
     * the 3D Object's props (even if it was only for 1 or 2... the DEMO's most heavy...).
     *
     * Just render explanation strings - you do can see the 3D object on browser's inspector.
     *
     * @param loggedObjData - the LOgged Event 'data' prop
     * @param markConsoleLogIsTrue - the mark, on Logged Event 'msg' line, on the Monitor, signling the inspector's console of it
     */
    private render3DobjectPropOnWhat(loggedObjData: EventData, markConsoleLogIsTrue: string): EventData {

            const hasConsoleLogItprop: boolean = loggedObjData.hasOwnProperty('consoleLogIt');

            const infoStr = 'Due to performance, 3D Object properties are NOT rendered on this Monitor...';
            const whatToDoStr = 'Look for a red spot, on any Logged Event\'s line message';
            // tslint:disable-next-line: max-line-length
            const foundRedSpotStr = 'Open the browser\'s Inspector - Ctrl+Shift+i. On the consoled marked Event time, you have the 3D object full properties';
            // tslint:disable-next-line: max-line-length
            const noRedSpotStr = 'Minding browser\'s performance, you do can toggle Logged Event\'s red spot (on top, @ "consoleLogIt" checkers).';

            loggedObjData.onWhat = hasConsoleLogItprop ?
                markConsoleLogIsTrue === '<mark></mark>' ?
                    Object.assign( {}, {
                        info: infoStr,
                        // whatToDo: whatToDoStr,
                        whatToDo: foundRedSpotStr,
                        hint: noRedSpotStr
                    })
                    :
                    Object.assign( {}, {
                        info: infoStr,
                        whatToDo: whatToDoStr,
                        foundRedSpot: foundRedSpotStr,
                        noRedSpot: noRedSpotStr
                    })
                :
                Object.assign( {}, { info: 'This Logged Event does not have a 3D object - rather see the associated Event.'})
            ;
            // Just with this ONE fixed, writed, prop, I've managed to keep everything running, no misses, and abs sync,
            // on 'empty-stage', the simplest 3D Obj rendering we have on our lib,
            // till I reach 520 new Event lines (changes on Orbit controls, with my mouse).
            // It would carry on, I'm sure... things were looking prety easy, and complete sync was clearly noticed.
            //
            // But... even if we, eventually, got the 3D object rendering to work, on the massive '3 floor Office' - which we didn't;
            // Maximum was the 'sphere' and, as you know, that's not heavy at all (takes lass then 2 sec to a complete 3D Stage render)
            // Then, even if it was possible at loading, with user params customization, and mouse mooves,
            // eventualy would be a question of time - the lib would certainly stop rendering,
            // or the below-the-line panel, with the object's JSON printing, on user clicking, would simply stop opening...
            //
            // So, for 'onWhat' prop, we'll keep this fixed, steady 'object-info',
            // and if user wants, he/she can see the 3D object on browser's Inspector,
            // clicking on the radio buttons ON/OFF, inside the line's below panel, on top.
            // -------------------------------------------------------------

            return loggedObjData;
    }

    private clearConsole() {

        // tslint:disable-next-line: variable-name
        const _console: any = console;
        // tslint:disable-next-line: no-string-literal
        let consoleAPI: any = console['API'];

        if (typeof _console._commandLineAPI !== 'undefined') {                // Chrome
            consoleAPI = _console._commandLineAPI;

        } else if (typeof _console._inspectorCommandLineAPI !== 'undefined') { // Safari
            consoleAPI = _console._inspectorCommandLineAPI;

        } else if (typeof _console.clear !== 'undefined') {                    // rest
            consoleAPI = _console;
        }

        consoleAPI.clear();
    }

}
