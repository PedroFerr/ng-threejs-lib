import { Injectable } from '@angular/core';

import { CookieHandlerService } from './cookie-handler.service';

import { ComponentBeingConsoledType } from '../../ng-threejs-lib.interfaces';

@Injectable({
    providedIn: 'root'
})
export class ConsoleLogService {

    protected obj3Dconsoles: { [key: string]: boolean };

    // ---------------------------------
    // Angular DEMO component's consoles:
    // ---------------------------------
    private libComponentMonitorDebug = false;
    // ---------------------------------
    private demoSphereComponent = false;
    private demoCubeComponent = false;
    private demoConicalCylinderComponent = false;
    private demoTorusComponent = false;
    private demoObjloaderComponent = false;
    // ---------------------------------

    // ---------------------------------
    // Not yet logged on Logg Book, but already console.logged
    // ---------------------------------
    private loadingWheel = false;
    private rendererCanvas = false;
    private cameraPerspectiveFrustum = false;
    // ---------------------------------


    // ----------------------------------------------------

    // In order for us to zoom and move around, HreeJS need to provide some Orbit controls:
    private orbitControlsDirective = false;
    private orbitControls = false;

    // ---------------------------------
    // The MOST CALLED guy: component responsible for keep (re) rendering the <canvas /> WebGL:
    // ---------------------------------
    private rendererService = true;

    // And now we enter the part of WHAT we actually see, with 3D "aspect".
    // But, in order for us to see it, we need a:
    private cameraDirective = true;

    // ----------------------------------------------------

    // Finally, for the 3D Stage to be completed, ThreeJS must mount an entire SCENE:
    // ----------------------------------------------------
    private scene = true;
    // ----------------------------------------------------
    // ... that has inside ThreeJS Helpers (Axis + Grid), Lights and... yeaaah - our 3D main ACTORS:
    private axisHelper = false;
    private gridHelper = false;

    private pointOfLight = false;
    private directionalLight = false;
    private hemisphereLight = false;

    // ----------------------------------------------------

    // Our 3D Actors need some way to get dragged into the (current) Scene's 3D STAGE.
    // (this will define the Modelling technique of how they were conceived)
    private loaderDirective = false;
    private object3Ddirective = true;
    private object3DLazyDirective = false;
    // And that's it!

    // ----------------------------------------------------

    // 0) Empty Component aka 'Stage Settings';
    private emptyComponent = false;

    // 1) Mesh Components:
    private mesh3Ddirective = true;
    private sphereMeshComponent = true;
    private cubeMeshComponent = true;
    private cylinderMeshComponent = true;
    private torusMeshComponent = true;

    // 2) 3 floor Office 3D building:
    private objLoader = true;

    // ?) still to do a DEMO component:
    private objectLoader = false;
    private svgLoader = false;

    // ----------------------------------------------------

    private initConsolesSetToTrue = [] as Array<ComponentBeingConsoledType>;
    // private snapShotOfConsolesSetToTrue = [] as Array<ComponentBeingConsoledType>;

    constructor(private cookieHandlerService: CookieHandlerService) {

        this.obj3Dconsoles = {

            SphereDemoComponent: this.demoSphereComponent,
            CubeDemoComponent: this.demoCubeComponent,
            ConicalCylinderDemoComponent: this.demoConicalCylinderComponent,
            TorusDemoComponent: this.demoTorusComponent,
            ObjLoaderDemoComponent: this.demoObjloaderComponent,
            // ---------------------------------
            MonitorDebugComponent: this.libComponentMonitorDebug,
            // ---------------------------------
            LoadingWheelComponent: this.loadingWheel,
            RendererCanvasComponent: this.rendererCanvas,
            PerspectiveCameraComponent: this.cameraPerspectiveFrustum,
            // ----------------------------------------------------
            OrbitControlsDirective: this.orbitControlsDirective,
            OrbitControlsComponent: this.orbitControls,

            RendererService: this.rendererService,

            CameraDirective: this.cameraDirective,
            // ----------------------------------------------------
            SceneComponent: this.scene,
            // ----------------------------------------------------
            AxisHelperComponent: this.axisHelper,
            GridHelperComponent: this.gridHelper,

            PointOfLightComponent: this.pointOfLight,
            DirectionalLightComponent: this.directionalLight,
            HemisphereLightComponent: this.hemisphereLight,
            // ----------------------------------------------------
            LoaderDirective: this.loaderDirective,
            Object3Ddirective: this.object3Ddirective,
            Object3DLazyDirective: this.object3DLazyDirective,
            // ----------------------------------------------------
            EmptyComponent: this.emptyComponent,

            Mesh3Ddirective: this.mesh3Ddirective,
            SphereMeshComponent: this.sphereMeshComponent,
            CubeMeshComponent: this.cubeMeshComponent,
            CylinderMeshComponent: this.cylinderMeshComponent,
            TorusMeshComponent: this.torusMeshComponent,

            ObjLoaderComponent: this.objLoader,

            ObjectLoaderComponent: this.objectLoader,
            SVGLoaderComponent: this.svgLoader
        };

        // Init state of who is ON and who has its console.log turned OFF:
        // tslint:disable-next-line: max-line-length
        let key: any; // ComponentBeingConsoledType;  <= "The left-hand side of a 'for...in' statement must be of type 'string' or 'any'" - ts(2405)
        for (key in this.obj3Dconsoles) {
            if (this.obj3Dconsoles.hasOwnProperty(key)) {
                const eachConsole = this.obj3Dconsoles[key];
                if (eachConsole === true) {
                    this.initConsolesSetToTrue.push(key);
                }
            }
        }
        // MIND YOU:
        // 'this.initConsolesSetToTrue' should NOT change dynamically NEVER, on the following METHODS of this Service!!
        // (that's why we are NOW using LOCAL storage memorization - if you need to update 'consoleLogIt' dynamically, do it through those!)
        //
        // This way, if you ever want to get the setted 'hard coded' (here!) boolean values, despite they had been changed by the User,
        // just pick 'this.initConsolesSetToTrue' - i.e. when USER turns POWER back ON, after it had been turned off by him/her.
        // On all other situations, ALWAYS pick local browser's cookie one - the current ONLY single source of (a dynamic) truth!
        //
        // On the other hand, if at some point in time, you want to toggle ALL current states, but still save WHO was ON, or OFF,
        // you can use the temporary array 'snapShotOfConsolesSetToTrue', global to this Service, and so global to 'window',
        // meaning that, while the user does NOT refresh the page, will always have the previous values there.
        // ;-)
        // Always... cof, cof... problem is in fact that; any top refreshment and all Services will reinitialize their data
        // We also have to memorize this one more!
        // this.setThisSnapShotMomentFROMallConsoleLogONcomponents();
        // Well... for now it can be left empty/undefined.
        // We won't need to do it till when 'toggleAllCurrentMemorizedConsolesTo()' is being called, from anywhere.
        //
        // All good!
        // Let's so put on our Local Storage this first hard codded "true"s:
        const isToCheckCurrentCookie = true;
        this.setAllConsoleLogONcomponentsToInitState(isToCheckCurrentCookie);
    }

    /**
     * All 3D Components, from our NG3D library (and others)
     * come here to decide to console.log, or not, their Data objects,
     * when are being forced into some kind of state chenge: on mounting, on changing or on destroy.
     *
     * Basicaly we want a TRUE or a FALSE, to console.log it or not.
     *
     * @param thisComponentName - the 3D Component Class name (might not be exactly the same as the Angular Component Class name)
     */
    isThisBeingConsoled(thisComponentName: ComponentBeingConsoledType): boolean {
        const hasConsole: any /* boolean | string */ = this.getThisComponentConsoleLogState(thisComponentName);

        const hasConsoleBoolean: boolean = (hasConsole === 'true' || hasConsole === true);
        // In fact, this 'hasConsole' is a STRING, if changed from DOM's User click event, on our 'consoleLogIt' pair of radio buttons!
        // REINFORCE TYPESCRIPT!!!! Never declare/instantiate vars WITHOUT declaring the Type!
        //
        // And I just did it from 'monitor-debug\monitor-debug.component.ts' => 'openObjViewer()' => '....addEventListener('click', ...'
        // I've injected, HERE, a string... Pumba! App's doooooomed! ;-)
        // So...
        // We don't want to
        // return hasConsole;
        //
        // ...but, instead, we want to
        return hasConsoleBoolean;
        // NOW it's definetely a BOOLEAN!
    }

    /**
     * MIND YOU:
     * All 'ComponentBeingConsoledType' on browser's local storage array, have their 'consoleLogIt' prop set to TRUE.
     *
     * So, 3 things can happen here, once this Method is called, being only the 2 first of any interest:
     * 1) if the current state is TRUE, we must REMOVE that item
     *     from the current stored 'consoleLogStateONper3Dcomponent' list
     * 2) if the current state of @thisComponentName is FALSE, we must ADD it
     *     to the current stored 'consoleLogStateONper3Dcomponent' list
     * 3) passed in 'toThisBoolean' is NOT the opposite of the current 3D component's 'consoleLogIt' state
     *     and there's nothing to change - there has not been, in fact, a valid toggling operation
     *     (user has been 'fiddling' ON adn OFF around, with SEVERAL logged events, but from the SAME 3D component's Class name)
     *
     * @param thisComponentName - the 3D Component Class name (might not be exactly the same as the Angular Component Class name)
     * @param toThisBoolean - just a check parameter to ensure User hasn't been clicking 'too much' on several radio button's
     *                         pair of <input />s (true/false).
     *                         //
     *                         Meanwhile on 'monitor-debug's 'openObjViewer()' method,
     *                         we have disabled all the same 'where' Evt prop buttons, once a choice is made.
     *                         So, unless User manipulates the DOM, by Inspector manipulation of JavaScript/HTML, it's fair enough
     *                         saying this situation - ambiguos set state of @thisComponentName - will likely happen at all...
     */
    toggleThisComponentConsoleLogStateTo(thisComponentName: ComponentBeingConsoledType, toThisBoolean: boolean): void {
        if (this.isThisBeingConsoled(thisComponentName)) {
            if (toThisBoolean === false) {
                this.removeThisComponentFromCurrentONConsoleLogs(thisComponentName);
            }
        } else {
            if (toThisBoolean === true) {
                this.addThisComponentToCurrentONConsoleLogs(thisComponentName);
            }
        }
    }

    /**
     * The user wants to stop, temporarly, the browser's Inspector console.log of ALL/ANY of the 3D component's 'object',
     * by setting 'monitor-debug' bottom bar control's switch DATA to OFF.
     *
     * It's a sort of "set aside" the current snap shot of the 3D components,
     * which have their 'consoleLogIt' props set to TRUE.
     *
     * Its possible he/she wants to see again, on browser's Inspector, the current consoled 3D objects.
     * So we have to memorize WHAT we are putting on the side, on hold, so we can recover them later,
     * independentely of the user has, meanwhile, been on another DEMO component, has refreshed the page from the top,
     * or even has closed the browser and will come to it, again, in whatever number of days...
     *
     * This method is being called from 'monitor-debub''s onSwitchingMonitorData() method, which is the emthod called
     * when "someone" clicks on Monitor's bottom control DATA switch.
     * And, due to to even switches styles, on loading, code MIGHT induce a FAKE click() on this DATA switch!
     * I.e. on the very same 'monitor-debug' component's 'checkMemorizedMonitorSwitchesStatus()' method,
     * called from ngAfterViewInit() component's LCHook.
     * So, when this happens, we surely don't want to put into the snap shot an empty array - that is emptied
     * when user REALLY wants to do a swap, by suspending temporarly ALL console.logs at the browser's Inspector.
     *
     * So... don't forget to test for that!
     *
     * This stored snap shot is a cookie called 'snapShotOfConsolesONper3Dcomponent'
     *
     * @param toBoolean - either a TRUE value, meaning All 'ComponentBeingConsoledType' PREVIOUSLY on browser's local storage
     *                     will start NOW consoling into the browser's Inspector,
     *                     or FALSE meaning NO COMPONENT will echo anything on browser's Inspector, from NOW on.
     */
    toggleAllCurrentMemorizedConsolesTo(toBoolean: boolean): void {
        if (toBoolean === false) {
            // Just in case, if this is NOT a fake click just to even switches styles:
            if (this.getAllConsoleLogONcomponents().length !== 0) {
                // We're good; we can make a swap:
                this.setThisSnapShotMomentFROMallConsoleLogONcomponents();
                // OK:
                this.cookieHandlerService.setCookieProp('consoleLogStateONper3Dcomponent', [] );
            }

        } else {
            this.setAllConsoleLogONcomponentsFROMsnapShotMoment();
        }
    }

    // =================================================
    //     The ConsoleLogService getters and setters
    // =================================================

    /**
     * We come to this setter on this Service constructor() hook
     * and when User turns 'monitor-debug' POWER switch ON @ 'monitor-debug' component
     * (obviously after he/she had turn it off - there's no other way to do it, except in this UX)
     *
     * If we are coming from this Service constructor(), we should only set all 3D component's 'consoleLogIt' state
     * into 'this.initConsolesSetToTrue' hard-coded booleans, if no one has still set the 'consoleLogStateONper3Dcomponent' cookie,
     * for the first time.
     * If it has, we stick with this memorized set, that might have been already changed dynamically by the User.
     * Remember that on jumping fom DEMO to  DEMO, as we're following a new Route, Angular cleans the previous Component
     * entirely, but all (data) Services persist, unless user does a top refresh of the URL.
     * So, in fact, this Service Class constructor() only gets called more than once IF, and only IF, user refreshes the page.
     *
     * We could have called the CookieHandlerService directly, from the (so far....) 2 callers of this Method.
     * But it's different having a return of 'null' from the CookieHandlerService (definetely the cookie property does NOT exists
     * or ever existed), than having an empty array from 'getAllConsoleLogONcomponents()'
     * (content might be on 'snapShotOfConsolesONper3Dcomponent', as a temporary User's cleaning Inspector consoles swap,
     * meaning, in fact, someone has already initialize it), and we should NOT change nothing.
     *
     * Another reason is the fact taht this is a BLOCK of changes, to swap many booleans at once.
     * So, as there's, in this Service, more than one options to set, as a BLOCK, the sate of SEVERAL
     * 3D Comonent's 'consoleLogIt' prop at once, we have decided to, rather then going directly to the Cookie Handler Service,
     * make the callers come here - where you can study different ranges of a setter to ALL
     * (i.e. the 'toggleAllCurrentMemorizedConsolesTo()' method)
     *
     * @param isToCheckCurrentCookie - flag ordering to check, first, if any 'consoleLogStateONper3Dcomponent' cookie is already set
     */
    public setAllConsoleLogONcomponentsToInitState(isToCheckCurrentCookie: boolean = null): void {
        if (isToCheckCurrentCookie) {
            if (this.cookieHandlerService.getCookieProp('consoleLogStateONper3Dcomponent') === null) {
                this.cookieHandlerService.setCookieProp('consoleLogStateONper3Dcomponent', this.initConsolesSetToTrue );
            }
        } else {
            this.cookieHandlerService.setCookieProp('consoleLogStateONper3Dcomponent', this.initConsolesSetToTrue );
        }
    }

    /**
     * MIND YOU:
     * All 'ComponentBeingConsoledType' stored on browser's local sorage, in an Array of 3D Components names,
     * have all their 'consoleLogIt' prop set to TRUE.
     *
     * @param thisComponentName - the 3D Component Class name (might not be exactly the same as the Angular Component Class name)
     */
    private getThisComponentConsoleLogState(thisComponentName: ComponentBeingConsoledType): boolean {
        const componentsWhoseConsoleLogItisTrue: Array<ComponentBeingConsoledType> = this.getAllConsoleLogONcomponents();

        return componentsWhoseConsoleLogItisTrue.some((name: string) => name === thisComponentName);
    }

    /**
     * Gets THE list, stored in browser's local storage, of the 3D Components
     * that have currently 'consoleLogIt' property set to TRUE.
     *
     * MIND YOU
     * the cookie might not have been set yet, for the first begining of times,
     * and so we might need to return an empty array.
     */
    private getAllConsoleLogONcomponents(): Array<ComponentBeingConsoledType> {
        // tslint:disable-next-line: max-line-length
        const consoleLogItOnList = this.cookieHandlerService.getCookieProp('consoleLogStateONper3Dcomponent') as Array<ComponentBeingConsoledType>;

        return consoleLogItOnList || [];
    }

    /**
     * Gets THE list, stored in browser's local storage, of some snap shot moment in time,
     * of the 3D Components that, by that time, had the 'consoleLogIt' property set to TRUE.
     *
     * MIND YOU
     * the cookie might not have been set yet, for the first begining of times,
     * and so we might need to return an empty array.
     */
    private getThisSnapShotMomentOfConsolesONper3Dcomponent(): Array<ComponentBeingConsoledType> {
        // tslint:disable-next-line: max-line-length
        const storedSnapShotMoment = this.cookieHandlerService.getCookieProp('snapShotOfConsolesONper3Dcomponent') as Array<ComponentBeingConsoledType>;

        return storedSnapShotMoment || [];
    }

    /**
     * ADDs this 3D Component's @thisComponentName name
     * to the memorized list of the 3D Components
     * that have currently 'consoleLogIt' property set to TRUE
     *
     * @param thisComponentName - the 3D Component Class name (might not be exactly the same as the Angular Component Class name)
     */
    private addThisComponentToCurrentONConsoleLogs(thisComponentName: ComponentBeingConsoledType): void {
        const consoleLogItOnList: Array<ComponentBeingConsoledType> = this.getAllConsoleLogONcomponents();
        consoleLogItOnList.push(thisComponentName);
        // Memorize it, now bigger:
        this.cookieHandlerService.setCookieProp('consoleLogStateONper3Dcomponent', consoleLogItOnList);
    }

    /**
     * REMOVEs this 3D Component's @thisComponentName name
     * from the memorized list of the 3D Components
     * that have currently 'consoleLogIt' property set to TRUE
     *
     * @param thisComponentName - the 3D Component Class name (might not be exactly the same as the Angular Component Class name)
     */
    private removeThisComponentFromCurrentONConsoleLogs(thisComponentName: ComponentBeingConsoledType): void {
        const consoleLogItOnList: Array<ComponentBeingConsoledType> = this.getAllConsoleLogONcomponents();

        for (let i = 0; i < consoleLogItOnList.length; i++) {
            if ( consoleLogItOnList[i] === thisComponentName) {
                consoleLogItOnList.splice(i, 1);
            }
        }
        // Memorize it, now shorter:
        this.cookieHandlerService.setCookieProp('consoleLogStateONper3Dcomponent', consoleLogItOnList);
    }

    private setAllConsoleLogONcomponentsFROMsnapShotMoment(): void {
        const snapShotOfConsolesSetToTrue = this.getThisSnapShotMomentOfConsolesONper3Dcomponent();
        this.cookieHandlerService.setCookieProp('consoleLogStateONper3Dcomponent', snapShotOfConsolesSetToTrue );
    }

    private setThisSnapShotMomentFROMallConsoleLogONcomponents(): void {
        const consoleLogStateONper3Dcomponent = this.getAllConsoleLogONcomponents();
        this.cookieHandlerService.setCookieProp('snapShotOfConsolesONper3Dcomponent', consoleLogStateONper3Dcomponent);

    }
}
