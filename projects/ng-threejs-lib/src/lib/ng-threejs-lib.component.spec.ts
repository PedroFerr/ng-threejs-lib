import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgThreejsLibComponent } from './ng-threejs-lib.component';

describe('NgThreejsLibComponent', () => {
  let component: NgThreejsLibComponent;
  let fixture: ComponentFixture<NgThreejsLibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgThreejsLibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgThreejsLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
