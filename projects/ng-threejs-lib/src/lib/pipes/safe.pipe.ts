import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

@Pipe({ name: 'safeSanitized' })
export class SafePipe implements PipeTransform {

     // tslint:disable: max-line-length

    /**
     * The HTML Templates that need some Sanitizing (in the html, in the style, in the url, etc. - see './safe.pipe.ts')
     * will be like the next example one, that is injecting an HTML string into a link href HTML tag
     *
     * @param sanitizer - Angular's DomSanitizer helps preventing Cross Site Scripting Security bugs (XSS) by sanitizing values to be safe to use in the different DOM contexts.
     * @example - <a href="#" class="i-do-not-through-sanitizing-console-warnings" [innerHTML]="addNewButtonContent | safeSanitized: 'html'">Hi! I am safe!</a>
     */
    constructor(protected sanitizer: DomSanitizer) { }

    public transform(value: any, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
        switch (type) {
            case 'html': return this.sanitizer.bypassSecurityTrustHtml(value);
            case 'style': return this.sanitizer.bypassSecurityTrustStyle(value);
            case 'script': return this.sanitizer.bypassSecurityTrustScript(value);
            case 'url': return this.sanitizer.bypassSecurityTrustUrl(value);
            case 'resourceUrl': return this.sanitizer.bypassSecurityTrustResourceUrl(value);
            default: throw new Error(`Invalid safe type specified: ${type}`);
        }
    }
}
