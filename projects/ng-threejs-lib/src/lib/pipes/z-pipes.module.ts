import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Deg2RadPipe } from './deg2rad.pipe';
import { Rad2DegPipe } from './rad2deg.pipe';
import { SafePipe } from './safe.pipe';


@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        Deg2RadPipe,
        Rad2DegPipe,
        SafePipe
    ],
    exports: [
        Deg2RadPipe,
        Rad2DegPipe,
        SafePipe
    ]
})

export class PipesModule {}
