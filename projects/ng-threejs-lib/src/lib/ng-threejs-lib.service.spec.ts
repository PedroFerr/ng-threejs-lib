import { TestBed } from '@angular/core/testing';

import { NgThreejsLibService } from './ng-threejs-lib.service';

describe('NgThreejsLibService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgThreejsLibService = TestBed.get(NgThreejsLibService);
    expect(service).toBeTruthy();
  });
});
