import * as THREE from 'three';
import { SomeNgChanges } from '../../ng-threejs-lib.interfaces';

export function CreateCameraInstance() {
    // TODO, when some refactor passes by...
    // It's working with a lot of verbose code - will be impec if CreateMeshInstance() structure is used!
}

export function CreateLightInstance() {
    // TODO, when some refactor passes by...
    // It's working with a lot of verbose code - will be impec if CreateMeshInstance() structure is used!
}

/**
 * Creates a ThreeJS Mesh object instance
 *
 * @param that - the 'this.' context of who is calling this const (Method)
 *               No Type for it, so far: https://www.typescriptlang.org/docs/handbook/functions.html#this-parameters
 * @param ofThatGeometry - the ThreeJS geometry that gives (different) shape (sphere, cube, cylinder, etc.) to a generic Mesh object
 * @param withThatName - the custom name we can baptize any new born instance of a Mesh
 * @param withThatProps - the ThreeJS demanded props for the Mesh instance to be created, with THAT particularly Geometry
 *                        I.e., you can check them out by hovering your mouse over "const geometry = new THREE.SphereGeometry();"
 */
export function CreateMeshInstance(
    that: any,
    ofThatGeometry: string,
    withThatName: string,
    ...withThatProps: Array<string | number | boolean>
): THREE.Mesh {
    // console.log(that, withThatProps);

    const geometry = new THREE[ofThatGeometry](...withThatProps);
    const material = that.getMaterial();

    const mesh = new THREE.Mesh(geometry, material);
    // Our 3D Mesh component (sphere, cube,.... whatever) has born!

    mesh.name = withThatName;
    that.applyShadowProps(mesh);

    const echoMsg = `${that.className}, CREATED a 3D Object "${mesh.type}" instantiation`;
    const hasConsoleLog: boolean = that.consoleLogService.isThisBeingConsoled(that.className);

    const scalarValues: Array<SomeNgChanges> = [
        { whatProp: 'type', fromValue: null, toValue: mesh.type },
        { whatProp: 'uuid', fromValue: null, toValue: mesh.uuid },
        { whatProp: 'name', fromValue: null, toValue: mesh.name }
    ];
    // Logg it:
    that.loggerService.logg3Devent(that.className, 'create', echoMsg, mesh, scalarValues, hasConsoleLog);

    return mesh;
}
