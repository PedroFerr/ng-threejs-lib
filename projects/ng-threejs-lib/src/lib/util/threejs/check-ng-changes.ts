import { SimpleChanges, SimpleChange } from '@angular/core';
import {
    SomeNgChanges, MoveTypeChanged,
    LoggEventData, EventDataOperationType, ComponentBeingConsoledType
} from '../../ng-threejs-lib.interfaces';

/**
 * Enables, on any Angular component's 'ngOnChanges()' Life Cycle Hook,
 * to catch the 2 type of movements (translation AND/OR rotation) CHANGES, to occur on any object.
 *
 * These CHANGES can be:
 *     * fired at the SAME time, or in different times,
 *     * and with 1, 2 or 3 (x,y,z axis) properties CHANGED, on each type of movement
 *
 * After those have been analysed and manipulated, this Method also:
 *     * already loggs the proper 3D Event, with all the CHANGES, by calling, here, the proper THAT logger Service,
 *     * and forces the re-rendering of the WebGL <canvas />, by calling, here, THAT proper renderer Service
 *
 * Should be able to apply to ANY 3D object (Scene, Mesh, OBJ loaded, Points Of Light, etc)
 * or to ANY ThreeJS Camera.
 *
 * Pretty cool, huh? ;-)
 *
 * @param that - The 'this.' context of who is calling this const (Method)
 *               No Type for it, so far: https://www.typescriptlang.org/docs/handbook/functions.html#this-parameters
 * @param propsChanged  - Angular component's 'ngOnChanges()' Life Cycle Hook detected CHANGES
 * @param typeOfChange - either a 'translate' or a 'rotate' CHANGE on movement/position of the 3D object
 * @param className - Class NAME of the Angular component ('SphereMeshComponent', 'CubeMeshComponent', etc.) that is detecting the CHANGES
 * @param object3D - the type of 3D object that, through an extended Class @className is suffering the movement/position CHANGES,
 *                   either dynamically - by the User action on the 'setting's card knobs - or on loading/birth.
 *                   i.e., SphereMeshComponent, CubeMeshComponent, PerspectiveCameraComponent, THREE.Mesh, THREE.Camera, etc.
 */
export function CheckWhatMOVEchanged(
    that: any,
    propsChanged: SimpleChanges,
    typeOfChange: MoveTypeChanged,
    className: ComponentBeingConsoledType,
    object3D: any
): void {

    const Xchg: SimpleChange = propsChanged[`${typeOfChange}X`];
    const Ychg: SimpleChange = propsChanged[`${typeOfChange}Y`];
    const Zchg: SimpleChange = propsChanged[`${typeOfChange}Z`];

    // that.mustRerender.length = 0;                 // Kills all memory position, occupied by each item (object) of the array
    that.mustRerender = [] as Array<SomeNgChanges>;  // Re-starts it on a NEW fresh var

    // Make sure it's not a first component's mount change:
    if ( (Xchg && !Xchg.firstChange) || (Ychg && !Ychg.firstChange) || (Zchg && !Zchg.firstChange) ) {
        FillMustReRenderWith(that, propsChanged, typeOfChange);
        // Apply the changes, and wait for the logged event to be emmited, to re-render the WebGL <canvas />
        if (typeOfChange === 'translate' || typeOfChange === 'position') { that.applyTranslation(); }
        if (typeOfChange === 'rotate') { that.applyRotation(Xchg !== undefined, Ychg !== undefined, Zchg !== undefined); }
        // Log it:
        const loggEvt: LoggEventData = LoggThisEvent(that, className, typeOfChange, 'changed', object3D);
        // Render it:
        that.rendererService.render(loggEvt);
    }
}

/**
 * Simple Object manipulation of its properties - Angular component's NG CHANGES.
 * Transform each in an Array of changes
 *
 * @param that - The 'this.' context of who is calling this const (Method)
 *               No Type for it, so far: https://www.typescriptlang.org/docs/handbook/functions.html#this-parameters
 * @param theseChanges - Angular's detected changes on 'ngOnChanges' Life Cycle Hook
 * @param onThisType - either a 'translate' or a 'rotate' CHANGE on movement/position of the 3D object
 */
function FillMustReRenderWith(that: any, theseChanges: SimpleChanges, onThisType: MoveTypeChanged): void {
    for (const key in theseChanges) {
        if (theseChanges.hasOwnProperty(key)) {
            const propChanged: SimpleChange = theseChanges[key];

            if (key.indexOf(onThisType) === 0) {
                that.mustRerender.push({
                    whatProp: key, fromValue: propChanged.previousValue, toValue: propChanged.currentValue
                });
            }
        }
    }
}

export function LoggThisEvent(
    that: any,
    fromThisClassName: ComponentBeingConsoledType,
    changingWhat: MoveTypeChanged | null,
    withThisOperation: EventDataOperationType,
    ofThisObject: THREE.Mesh
): LoggEventData {
    const msg: string = changingWhat ? ChangesMsg(fromThisClassName, changingWhat) : LoadedDEMOmsg(fromThisClassName);
    const scalarValues: Array<SomeNgChanges> = that.mustRerender.length > 0 ? that.mustRerender : null;

    const logged3Devent = that.loggerService.logg3Devent(
        // tslint:disable-next-line: max-line-length
        fromThisClassName, withThisOperation, msg, ofThisObject, scalarValues, that.consoleLogService.isThisBeingConsoled(fromThisClassName)
    );

    return logged3Devent;
}

function ChangesMsg(fromThisClassName: ComponentBeingConsoledType, changingWhat: MoveTypeChanged): string {
    return `${fromThisClassName}, CHANGED a "${changingWhat}" param(s)`;
}

function LoadedDEMOmsg(thisClassName: ComponentBeingConsoledType): string {
    return `DEMO\'s 3D Stage ACTOR, rendered a ${thisClassName}`;
}

// ====================================================================================================================================

/**
 * Very similar to 'CheckWhatMOVEchanged()' top method, but fully generic.
 * This means its much agile and shorter in code, but also that will leave behind some cool features...
 *
 * The losses, comparing to the upper method, are:
 *     * does NOT logg any 3D Event - you have to do it on THAT context.
 *     * does NOT forces the re-rendering of the WebGL <canvas /> - you also have to do it after.
 *
 * The reason is simple: you don't know exactly what TO DO between the apply of the changes on the 3D object
 * and the re-render of it, on the WebGL.
 * Some times you need to 'applyMaterial()', others 'updateFrustum()', etc., etc.
 * We quickly would transform a simple unique generic method with loads of "if... then... else"s
 *
 * Basically, just loops the 'propsChanged' Angular object, produced on CHANGES.
 * That's all.
 *
 * All you have to do, after calling this Method, is to check if 'that.mustRerender' Array of changes
 * is still with length '0' or if has any 'SomeNgChanges' (object) inside.
 *
 * Or - we've made it simpler! - just check what boolean is returning.
 * ;-)
 *
 * @param that - The 'this.' context of who is calling this const (Method)
 *               No Type for it, so far: https://www.typescriptlang.org/docs/handbook/functions.html#this-parameters
 * @param propsChanged - Angular's detected changes on 'ngOnChanges' Life Cycle Hook
 * @param propsToCheck - the object properties that we want to check, to respond on any CHANGE
 * @returns - boolean
 */
export function CheckWhatChangedOn(that: any, propsChanged: SimpleChanges, ...propsToCheck: Array<string>): boolean {

    let somethingChanged = false;
    // Make sure it's not a first component's mount change:
    [...propsToCheck].forEach( (checkProp: string) => {
        const chg = propsChanged[checkProp];
        if (chg && !chg.firstChange) { somethingChanged = true; }
    });

    that.mustRerender = [] as Array<SomeNgChanges>;

    if (somethingChanged) {
        for (const key in propsChanged) {
            if (propsChanged.hasOwnProperty(key)) {
                const propChanged: SimpleChange = propsChanged[key];

                if ([...propsToCheck].indexOf(key) > -1) {
                    that.mustRerender.push({
                        whatProp: key, fromValue: propChanged.previousValue, toValue: propChanged.currentValue
                    });
                }
            }
        }
        return somethingChanged;
    } else {
        return somethingChanged;
    }
}
