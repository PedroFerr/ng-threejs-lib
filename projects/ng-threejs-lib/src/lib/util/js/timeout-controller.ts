// tslint:disable: max-line-length
// tslint:disable: only-arrow-functions

/**
 * ------------------------------------------------------------------------------------------------------------------------------------------*
 *                                                                                                                                          *
 *          == DELAYED EVENT TRIGGER ==                                                                                                     *
 *                                                                                                                                          *
 *  Sometimes, for "tiny-random" trigger situations (like $(window).onresize, for instances, the overload processing for each pixel/ms      *
 *  is too much and, in most cases absolutely desnecessary.                                                                                 *
 *  In order to reduce the number of times an event is triggered, we have a little timer... if nothing happens                              *
 *  we assume our "client" request is satisfied and, only then, we trigger the event! http://stackoverflow.com/a/4541963/2816279            *
 *                                                                                                                                          *
 *  The 'timer' variable, with an unique id is a tremendous improvement!                                                                    *
 *  As so, you can call it multiple times (even from different parts of your code), with a unique id for each callback                      *
 *  (just change the text on the listener function!), that those unique IDs are used to keep all the timeout events separate.               *
 *                                                                                                                                          *
 *  JUST Great! ;-)                                                                                                                         *
 *                                                                                                                                          *
 *  This delayed trigger function is simply called inside the event binding function, any number of times, in any part of the code:         *
 *
 *   $obj.on('event', function () {
 *       waitForFinalEvent(function(){
 *           alert('Only now the event has been called!');
 *           //...
 *       }, 500, "event trigger #1!");
 *   });
 *                                                                                                                                          *
 * -----------------------------------------------------------------------------------------------------------------------------------------
 */
export const waitForFinalEvent = (function() {
    const timers = {};

    return function(callback: () => void, ms: number, uniqueId: string) {
        if (!uniqueId) {
            uniqueId = 'Don\'t call this twice without a UNIQUE "uniqueId"';
        }
        if (timers[uniqueId]) {
            clearTimeout (timers[uniqueId]);
        }
        // console.error(timers);
        timers[uniqueId] = setTimeout(callback, ms);
    };
})();

