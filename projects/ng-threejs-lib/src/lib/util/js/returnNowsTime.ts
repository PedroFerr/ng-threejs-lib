    /**
     * We want to simulate the broser's Inspector timestamp.
     * A kind of an unique Id could be '#${new Date().getTime()}'
     * But we want user to see when the event happened...
     *
     * And for the 'milliseconds' we should pad it with 1 or 2 '0',
     * at front, if number is less than 100, or 10, respectively.
     */
    export const returnNowsTime = (): string => {
        const now = new Date();
        const milliSecs = now.getMilliseconds();
        const milliSecsPadded = ('00' + milliSecs).slice(-3);

        return now.toLocaleTimeString() + '.' + milliSecsPadded;

    };


