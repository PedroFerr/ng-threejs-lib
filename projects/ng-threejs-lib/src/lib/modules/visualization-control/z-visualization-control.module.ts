import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapControlsComponent } from './control-components/map-controls.component';
import { OrbitControlsComponent } from './control-components/orbit-controls.component';

import { DirectionalLightComponent } from './light-components/directional-light.component';
import { HemisphereLightComponent } from './light-components/hemisphere-light.component';
import { PointOfLightComponent } from './light-components/point-light.component';

import {AxisHelperComponent  } from './threejs-helpers/axes-helper.component';
import { GridHelperComponent } from './threejs-helpers/grid-helper.component';
import { EmptyComponent } from './threejs-helpers/object-3D-container-helper.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        MapControlsComponent,
        OrbitControlsComponent,

        DirectionalLightComponent,
        HemisphereLightComponent,
        PointOfLightComponent,

        AxisHelperComponent,
        GridHelperComponent,
        EmptyComponent
    ],
    exports: [
        MapControlsComponent,
        OrbitControlsComponent,

        DirectionalLightComponent,
        HemisphereLightComponent,
        PointOfLightComponent,

        AxisHelperComponent,
        GridHelperComponent,
        EmptyComponent
    ]
})
export class VisualizationControlModule {}
