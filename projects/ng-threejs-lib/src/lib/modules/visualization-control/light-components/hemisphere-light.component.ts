import { Component, forwardRef, Input } from '@angular/core';
import * as THREE from 'three';

import { Object3dDirective } from 'projects/ng-threejs-lib/src/lib/modules/scene/object-3d.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

import { appliedColor } from 'projects/ng-threejs-lib/src/lib/util/threejs/applied-color';

@Component({
    selector: 'ng3d-hemisphere-light',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => HemisphereLightComponent) }],
    template: '<ng-content></ng-content>'
})
export class HemisphereLightComponent extends Object3dDirective<THREE.HemisphereLight> {

    @Input() skyColor = 0xffffff;
    @Input() groundColor = 0x444444;
    @Input() intensity = 1;

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    protected newObject3DInstance(): THREE.HemisphereLight {

        const light = new THREE.HemisphereLight(
            appliedColor(this.skyColor),
            appliedColor(this.groundColor),
            appliedColor(this.intensity)
        );

        return light;
    }

}
