import { Component, forwardRef, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import * as THREE from 'three';

import { Object3dDirective } from 'projects/ng-threejs-lib/src/lib/modules/scene/object-3d.directive';

import { SceneComponent } from '../../scene';
import { appliedColor } from 'projects/ng-threejs-lib/src/lib/util/threejs/applied-color';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

import { LoggEventData, SomeNgChanges } from '../../../ng-threejs-lib.interfaces';

@Component({
    selector: 'ng3d-point-light',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => PointOfLightComponent) }],
    template: '<ng-content></ng-content>'
})
export class PointOfLightComponent extends Object3dDirective<THREE.PointLight> implements OnChanges {

    scene: SceneComponent;

    pointOfLight: THREE.PointLight;

    @Input() color = 0xffffff;
    @Input() intensity = 1;
    @Input() distance = 500;
    @Input() castShadow = false;

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    ngOnChanges(propsChanged: SimpleChanges) {
        let mustRerender = {} as SomeNgChanges;
        // console.log('CHANGES on a Point Of Light:', propsChanged);

        const colorChg: SimpleChange = propsChanged.color;
        const intensityChg: SimpleChange = propsChanged.intensity;
        const distanceChg: SimpleChange = propsChanged.distance;

        if (['color', 'intensity', 'distance'].some(propName => propName in propsChanged)) {

            if (colorChg && colorChg.currentValue && colorChg.currentValue && !colorChg.firstChange) {
                // this.pointOfLight.color = this.color;
                // NOPES! :
                this.pointOfLight.color.setHex( this.color );
                //
                mustRerender = Object.assign( {}, {
                    // tslint:disable-next-line: max-line-length
                    whatProp: 'color', fromValue: '#' + colorChg.previousValue.toString(16), toValue: '#' + colorChg.currentValue.toString(16)
                });
            }
            if (intensityChg  && intensityChg.currentValue && intensityChg.currentValue && !intensityChg.firstChange) {
                this.pointOfLight.intensity = this.intensity;
                mustRerender = Object.assign(  {}, {
                    whatProp: 'intensity', fromValue: intensityChg.previousValue, toValue: this.pointOfLight.intensity
                });
            }
            if (distanceChg  && distanceChg.currentValue && distanceChg.currentValue && !distanceChg.firstChange) {
                this.pointOfLight.distance = this.distance;
                mustRerender = Object.assign( {}, {
                    whatProp: 'distance', fromValue: distanceChg.previousValue, toValue: this.pointOfLight.distance
                });
            }
        }

        const xChg: SimpleChange = propsChanged.translateX;
        const yChg: SimpleChange = propsChanged.translateY;
        const zChg: SimpleChange = propsChanged.translateZ;

        if (['translateX', 'translateY', 'translateZ'].some(propName => propName in propsChanged)) {
            // this.applyTranslation();    // <= from Object3dDirective Directive
            // this.modified = true;

            if (xChg && xChg.currentValue && xChg.currentValue && !xChg.firstChange) {
                mustRerender = Object.assign( {}, {
                    whatProp: 'position_X', fromValue: xChg.previousValue, toValue: xChg.currentValue
                });
            }
            if (yChg  && yChg.currentValue && yChg.currentValue && !yChg.firstChange) {
                mustRerender = Object.assign(  {}, {
                    whatProp: 'position_Y', fromValue: yChg.previousValue, toValue: yChg.currentValue
                });
            }
            if (zChg  && zChg.currentValue && zChg.currentValue && !zChg.firstChange) {
                mustRerender = Object.assign( {}, {
                    whatProp: 'position_Z', fromValue: zChg.previousValue, toValue: zChg.currentValue
                });
            }

            // Something must have changed, by now:
            if (this.pointOfLight) {    // Anyway... might not if xxxx.firstChange is still true.
                this.applyTranslation();
            }
        }

        if (mustRerender.hasOwnProperty('whatProp')) {
            // tslint:disable-next-line: max-line-length
            const echoMsg = `PointOfLightComponent, CHANGED the ${mustRerender.whatProp} from "${mustRerender.fromValue}" to "${mustRerender.toValue}"`;
            const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('PointOfLightComponent');
            // Logg it:
            const loggEvt: LoggEventData = this.loggerService.logg3Devent(
                'PointOfLightComponent', 'changed', echoMsg, this.pointOfLight, [mustRerender], hasConsoleLog
            );
            this.rendererService.render(loggEvt);
        }

    }

    // Can be called from 'projects\ng-threejs-lib\src\lib\modules\scene\scene.component.ts' to dynamically add User (params) PointOfLight
    // with 'withThisParams':
    newObject3DInstance(withThisParams: any = {}): THREE.PointLight {

        const name = withThisParams.name;

        const position = withThisParams.position;
        const translateX = position ? position.translateX : this.translateX;
        const translateY = position ? position.translateY : this.translateY;
        const translateZ = position ? position.translateZ : this.translateZ;

        const emittance = withThisParams.emittance;
        const color = emittance ? emittance.color : this.color;
        const intensity = emittance ? emittance.intensity : this.intensity;
        const distance = emittance ? emittance.distance : this.distance;

        this.pointOfLight = new THREE.PointLight(appliedColor(color), intensity, distance);
        this.pointOfLight.name = name;
        if (position) { this.pointOfLight.position.set( translateX, translateY, translateZ ); }

        if (this.castShadow === true) {
            this.pointOfLight.castShadow = this.castShadow;
            // TODO: props
            this.pointOfLight.shadow.mapSize.width = 1024;
            this.pointOfLight.shadow.mapSize.height = 1024;
            this.pointOfLight.shadow.camera.near = 0.5;
            this.pointOfLight.shadow.camera.far = 500;
            this.pointOfLight.shadow.bias = -0.001;
            this.pointOfLight.shadow.radius = 1;
        }

        const polPosition = `(${translateX}, ${translateY} , ${translateZ})`;
        const polEmittance = `color: #${this.pointOfLight.color.getHexString()}, ` +
                                `intensity: ${this.pointOfLight.intensity}, ` +
                                `distance: ${this.pointOfLight.distance}`
        ;
        const scalarValues: Array<SomeNgChanges> = [
            { whatProp: 'type', fromValue: null, toValue: this.pointOfLight.type },
            { whatProp: 'uuid', fromValue: null, toValue: this.pointOfLight.uuid },
            { whatProp: 'name', fromValue: null, toValue: this.pointOfLight.name },
            { whatProp: 'position', fromValue: null, toValue: polPosition },
            { whatProp: 'emittance', fromValue: null, toValue: polEmittance }
        ];
        const echoMsg = `PointOfLightComponent, CREATED "${this.pointOfLight.type}"`;
        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('PointOfLightComponent');
        // Logg it:
        const loggEvt: LoggEventData = this.loggerService.logg3Devent(
            'PointOfLightComponent', 'create', echoMsg, this.pointOfLight, scalarValues, hasConsoleLog
        );

        return this.pointOfLight;
    }

}
