import { Component, forwardRef, Input } from '@angular/core';
import * as THREE from 'three';

import { Object3dDirective } from 'projects/ng-threejs-lib/src/lib/modules/scene/object-3d.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

import { appliedColor } from 'projects/ng-threejs-lib/src/lib/util/threejs/applied-color';

@Component({
    selector: 'ng3d-directional-light',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => DirectionalLightComponent) }],
    template: '<ng-content></ng-content>'
})
export class DirectionalLightComponent extends Object3dDirective<THREE.DirectionalLight> {

    @Input() color = 0xffffff;
    @Input() intensity = 1;
    // by default, target is 0,0,0
    @Input() target = new THREE.Object3D();
    @Input() castShadow = true;

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    protected newObject3DInstance(): THREE.DirectionalLight {

        const light = new THREE.DirectionalLight(
            appliedColor(this.color),
            this.intensity
        );

        light.target = this.target;

        if (this.castShadow === true) {
            light.castShadow = this.castShadow;
            // TODO: props
            light.shadow.camera.top = 100;
            light.shadow.camera.bottom = -100;
            light.shadow.camera.left = -100;
            light.shadow.camera.right = 100;
            light.shadow.camera.near = 0.1;
            light.shadow.camera.far = 500;
            light.shadow.mapSize.set(1024, 1024);
            light.shadow.bias = -0.001;

        }
        return light;

    }

}
