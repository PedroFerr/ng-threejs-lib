import { Component, forwardRef, Input } from '@angular/core';
import * as THREE from 'three';

import { Object3dDirective } from '../../scene/object-3d.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

import { SomeNgChanges } from '../../../ng-threejs-lib.interfaces';

@Component({
    selector: 'ng3d-axes-helper',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => AxisHelperComponent) }],
    template: '<ng-content></ng-content>'
})
export class AxisHelperComponent extends Object3dDirective<THREE.AxesHelper> {

    @Input() size: number;

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    protected newObject3DInstance(): THREE.AxesHelper {

        const axisHelper = new THREE.AxesHelper(this.size);

        const echoMsg = `AxisHelperComponent, CREATED XYZ Axis on "${axisHelper.type}"`;
        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('AxisHelperComponent');

        const polPosition = `(${axisHelper.position.x}, ${axisHelper.position.y} , ${axisHelper.position.z})`;
        const scalarValues: Array<SomeNgChanges> = [
            { whatProp: 'type', fromValue: null, toValue: axisHelper.type },
            { whatProp: 'uuid', fromValue: null, toValue: axisHelper.uuid },
            { whatProp: 'name', fromValue: null, toValue: axisHelper.name },
            { whatProp: 'position', fromValue: null, toValue: polPosition },
        ];
        // Logg it:
        this.loggerService.logg3Devent('AxisHelperComponent', 'create', echoMsg, axisHelper, scalarValues, hasConsoleLog);

        return axisHelper;
    }

}
