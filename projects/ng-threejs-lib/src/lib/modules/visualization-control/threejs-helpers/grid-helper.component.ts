import { Component, forwardRef, Input } from '@angular/core';
import * as THREE from 'three';

import { Object3dDirective } from '../../scene/object-3d.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

import { SomeNgChanges } from '../../../ng-threejs-lib.interfaces';

@Component({
    selector: 'ng3d-grid-helper',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => GridHelperComponent) }],
    template: '<ng-content></ng-content>'
})
export class GridHelperComponent extends Object3dDirective<THREE.GridHelper> {

    @Input() size: number;
    @Input() divisions: number;

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    protected newObject3DInstance(): THREE.GridHelper {

        const gridHelper = new THREE.GridHelper(this.size, this.divisions);

        const echoMsg = `GridHelperComponent, CREATED Grid on "${gridHelper.type}"`;
        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('GridHelperComponent');

        const polPosition = `(${gridHelper.position.x}, ${gridHelper.position.y} , ${gridHelper.position.z})`;
        const scalarValues: Array<SomeNgChanges> = [
            { whatProp: 'type', fromValue: null, toValue: gridHelper.type },
            { whatProp: 'uuid', fromValue: null, toValue: gridHelper.uuid },
            { whatProp: 'name', fromValue: null, toValue: gridHelper.name },
            { whatProp: 'position', fromValue: null, toValue: polPosition },
        ];
        // Logg it:
        this.loggerService.logg3Devent('GridHelperComponent', 'create', echoMsg, gridHelper, scalarValues, hasConsoleLog);

        return gridHelper;
    }

}
