import { AfterViewInit, Component, forwardRef } from '@angular/core';
import * as THREE from 'three';

import { Object3dDirective } from '../../scene/object-3d.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

/**
 * This generic holder component is interesting when you want to manipulate
 * TRANSLATION or/and ROTATION of aGROUP of 3D Objects of ANY type - Lights, Scene, etc included.
 * The CONTENTS depend exclusively on your HTML markup, inside any DEMO Component.
 *
 * For instances, in a DEMO component, something like this Template markup
 * -----------------------------------
 * ...
 * <ng3d-perspective-camera />
 * <ng3d-scene
 *     <ng3d-axes-helper />
 *         ...
 *     <ng3d-point-light />
 *     <ng3d-point-light />
 *         ...
 *     <ng3d-object3d-container-helper
 *         [translateX]="translateX"
 *         [translateY]="translateY"
 *         [translateZ]="translateZ"
 *         [rotateX]="rotateX"
 *         [rotateY]="rotateY"
 *         [rotateZ]="rotateZ">
 *             ...
 *         <ng3d-sphere-mesh …>…</ng3d-sphere-mesh-mesh>
 *         <ng3d-torus-mesh …>…</ng3d-torus-mesh>
 *             ...
 *     </ng3d-object3d-container-helper>
 * </ ng3d-scene>
 * ...
 * -----------------------------------
 * would translate/rotate BOTH Sphere and Torus 3D Components, over <ng3d-axes-helper />
 * (whtever they would be), if knoobs would CHANGE any of the [translate_] or [rotate_] @Input()s
 *
 * --------
 * MIND YOU:
 * --------
 * 1) We have opted to put <ng3d-perspective-camera /> OUTSIDE the Stage's 3D Scene <ng3d-scene />
 *     in all the DEMO's Components - could have opted for the other way, but we'd definetely loose independency
 * 2) CameraDirective does NOT extend from an Object3dDirective (extends directly from 'THREE.Camera').
 *     Yet, this 'THREE.Camera' 3D Object has its own's ROTATE and POSITION methods,
 *     through which we can move, and rotate, any Camera around on the 3 Axis
 * 3) Don't forget OrbitControlsDirective, where the CameraDirective is ATTACHED, from where we can also control its POSITION
 *     meaning, by it's (mouse) CHANGEs, a direct TRANSLATION to the (current) Camera position.
 * 4) Axis and Grid helpers, and PointofLight 3D Components, DO extend from Object3dDirective, maning we could, also,
 *     apply a common translation/rotation CHANGE to these 3D Components, as long as wrapped into an <ng3d-object3d-container-helper>
 *
 */
@Component({
    selector: 'ng3d-object3d-container-helper',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => EmptyComponent) }],
    template: '<ng-content></ng-content>'
})
export class EmptyComponent extends Object3dDirective<THREE.Object3D> implements AfterViewInit {

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    protected newObject3DInstance(): THREE.Object3D {
        return new THREE.Object3D();
    }

}
