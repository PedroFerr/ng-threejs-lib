import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MapControls, OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

import { OrbitControlsDirective } from './orbit-controls.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { AnimationService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/animation.service';
import { RaycasterService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/raycaster.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

@Component({
    selector: 'ng3d-map-controls',
    template: `<ng-content></ng-content>`,
    styleUrls: ['./_common-controls.component.scss']
})
export class MapControlsComponent extends OrbitControlsDirective<OrbitControls> implements OnChanges {

    @Input() zoomSpeed = 1.2;

    @Input() rotateSpeed = 1.0;
    @Input() autoRotate = false;
    @Input() autoRotateSpeed = 0.5;

    @Input() enableDamping = false;
    @Input() dampingFactor = 0.1;

    @Input() screenSpacePanning = false;

    @Input() minDistance = 20;
    @Input() maxDistance = 200;
    @Input() maxPolarAngle: number = Math.PI / 2 - 0.1;

    @Input() panSpeed = 1.2;

    constructor(
        protected rendererService: RendererService,
        protected raycasterService: RaycasterService,
        protected animationService: AnimationService,

        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, raycasterService, loggerService, consoleLogService);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (!this.controls) {
            return;
        }
        super.ngOnChanges(changes);

        if (changes.rotateSpeed) {
            this.controls.rotateSpeed = this.rotateSpeed;
        }
        if (changes.zoomSpeed) {
            this.controls.zoomSpeed = this.zoomSpeed;
        }
        // TODO: add others
    }

    protected setUpControls() {
        this.controls = new MapControls(
            this.childCameras.first.camera,
            this.listeningControlElement && this.listeningControlElement.nativeElement
        );
        this.controls.rotateSpeed = this.rotateSpeed;
        this.controls.zoomSpeed = this.zoomSpeed;

        this.controls.panSpeed = this.panSpeed;

        this.controls.autoRotate = this.autoRotate;
        this.controls.autoRotateSpeed = this.autoRotateSpeed;
        this.controls.enableDamping = this.enableDamping; // an animation loop is required when either damping or auto-rotation are enabled
        this.controls.dampingFactor = this.dampingFactor;

        this.controls.screenSpacePanning = this.screenSpacePanning;
        this.controls.minDistance = this.minDistance;
        this.controls.maxDistance = this.maxDistance;
        this.controls.maxPolarAngle = this.maxPolarAngle;

        this.controls.update();

        // Advanced animation:
        if (this.autoRotate || this.enableDamping) {
            this.animationService.animate.subscribe(() => {
                this.controls.update();
            });
            this.controls.addEventListener('change', (evt) => {
                this.rendererService.render('MapControlsComponent CHANGED its Controls by Mouse Event!', evt);
            });
            this.animationService.start();
        }

        this.rendererService.render('MapControlsComponent has its Controls setup!');
    }


}
