import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

import { OrbitControlsDirective } from './orbit-controls.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { RaycasterService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/raycaster.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

import { LoggEventData, SomeNgChanges } from '../../../ng-threejs-lib.interfaces';

@Component({
    selector: 'ng3d-orbit-controls',
    template: `<ng-content></ng-content>`,
    styleUrls: ['./_common-controls.component.scss']
})
export class OrbitControlsComponent extends OrbitControlsDirective<OrbitControls> implements OnChanges {

    @Input() rotateSpeed = 1.0;
    @Input() zoomSpeed = 1.2;

    constructor(
        protected rendererService: RendererService,
        protected raycasterService: RaycasterService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, raycasterService, loggerService, consoleLogService);
    }


    ngOnChanges(changes: SimpleChanges) {

        if (this.consoleLogService.isThisBeingConsoled('OrbitControlsComponent')) { console.log('CHANGES on Orbit control:', changes); }

        if (!this.controls) {
            return;
        }
        super.ngOnChanges(changes);

        if (changes.rotateSpeed) {
            this.controls.rotateSpeed = this.rotateSpeed;
            console.log('Rotate speed changed:', this.rotateSpeed);
        }
        if (changes.zoomSpeed) {
            this.controls.zoomSpeed = this.zoomSpeed;
            console.log('Zoom speed changed:', this.controls.zoomSpeed);
        }
    }

    protected setUpControls(isFirstTime: boolean): void {
        this.controls = new OrbitControls(
            this.childCameras.first.camera,
            this.listeningControlElement && this.listeningControlElement.nativeElement
        );
        this.controls.rotateSpeed = this.rotateSpeed;
        this.controls.zoomSpeed = this.zoomSpeed;

        // tslint:disable-next-line: max-line-length
        const echoMsg = `OrbitControlsComponent, ${isFirstTime ? 'CREATED ' : 'RE-CREATED '} on «${this.controls.domElement.nodeName.toLowerCase()}`;
        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('OrbitControlsComponent');
        // tslint:disable-next-line: no-string-literal
        const orbitControlPosition = `(${this.controls['position0'].x}, ${this.controls['position0'].y}, ${this.controls['position0'].z})`;
        const scalarValues: Array<SomeNgChanges> = [
            { whatProp: 'isFirstTimeCreation', fromValue: isFirstTime.toString(), toValue: this.controls.domElement.nodeName },
            { whatProp: 'orbitPosition', fromValue: null, toValue: orbitControlPosition },
            { whatProp: 'rotateSpeed', fromValue: null, toValue: this.controls.rotateSpeed },
            { whatProp: 'zoomSpeed', fromValue: null, toValue: this.controls.zoomSpeed },
        ];
        // Logg it:
        const loggEvt: LoggEventData = this.loggerService.logg3Devent(
            'OrbitControlsComponent', isFirstTime ? 'create' : 'changed', echoMsg, this.controls, scalarValues, hasConsoleLog
        );
        if (isFirstTime) {
            this.rendererService.render(loggEvt);
        }
    }

}
