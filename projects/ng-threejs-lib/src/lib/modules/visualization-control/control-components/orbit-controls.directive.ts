import {
    Directive, ContentChild, ContentChildren, QueryList, ElementRef,
    Input, OnChanges, SimpleChanges, AfterViewInit, OnDestroy
} from '@angular/core';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

import { CameraDirective } from 'projects/ng-threejs-lib/src/lib/modules/camera/camera.directive';
import { RendererCanvasComponent } from 'projects/ng-threejs-lib/src/lib/components/renderer/renderer-canvas.component';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { RaycasterService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/raycaster.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

import { LoggEventData, SomeNgChanges } from '../../../ng-threejs-lib.interfaces';


@Directive({})
export abstract class OrbitControlsDirective<T extends OrbitControls> implements AfterViewInit, OnChanges, OnDestroy {

    @ContentChildren(CameraDirective, { descendants: true }) childCameras: QueryList<CameraDirective<THREE.Camera>>;
    @ContentChild(RendererCanvasComponent) webGlRenderer: RendererCanvasComponent;
    /**
     * The element on whose native element the orbit control will listen for mouse events.
     *
     * Note that keyboard events are still listened for on the global window object, this is
     * a known issue from Three.js: https://github.com/mrdoob/three.js/pull/10315
     */
    @Input() listeningControlElement: ElementRef;

    protected controls: T;

    private cameraPreviousPosition: string = null;
    private cameraPreviousRotation: THREE.Euler = null;
    private orbitPreviousPosition: string = null;
    private userMouseZoomPreviousValue: number = null;

    constructor(
        protected rendererService: RendererService,
        protected raycasterService: RaycasterService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) { }

    ngOnChanges(changes: SimpleChanges) {
        // If the THREE.js OrbitControls are not set up yet, we do not need to update anything
        // as they will pick the new values from the @Input properties automatically, upon creation.
        // if (!this.controls) { return; }

        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('OrbitControlsDirective');
        if (hasConsoleLog) { console.log('Orbit controls CHANGES:', changes); }

        const controlChanges = changes.listeningControlElement;

        if (controlChanges && controlChanges.currentValue && !controlChanges.firstChange) {
            // The DOM element the OrbitControls listen on cannot be changed once an
            // OrbitControls object is created. We thus need to re-CREATE it again:
            this.controls.dispose();
            this.setUpControls(false);
        }
    }

    ngAfterViewInit(): void {
        // console.log('OrbitControlsComponent.ngAfterViewInit');
        if (this.childCameras === undefined || this.childCameras.first === undefined) {
            throw new Error('Camera is not found');
        }
        if (!this.webGlRenderer) {
            throw new Error('webGlRenderer is not found');
        }
        this.listeningControlElement = this.webGlRenderer.renderPane;
        this.setUpControls(true);
        this.configureListeners();
    }

    ngOnDestroy() {
        if (this.controls) {
            this.controls.dispose();
        }
    }

    protected abstract setUpControls(isFirstTime: boolean): void;


    private configureListeners() {
        // --------------------------------------------
        // User is fiddling around with it's mouse! ;-)
        // --------------------------------------------

        this.controls.addEventListener('change', (evt) => {

            // Light Camera should follow any changes - get it and update 3D Position:
            // console.error(sceneObjects);
            // console.error(ThreeJScamera, userMouseZoom);

            const loggEvt = this.checkChgsAndLoggThem(evt);
            this.rendererService.render(loggEvt);
        });

        // don't raycast during rotation/damping/panning
        if (this.raycasterService.isEnabled) {
            this.controls.addEventListener('start', () => {
                this.raycasterService.pause();
            });
            this.controls.addEventListener('end', () => {
                this.raycasterService.resume();
            });
        }
    }

    private checkChgsAndLoggThem(evt: MouseEvent): LoggEventData {
        const ThreeJScamera = this.controls.object as THREE.PerspectiveCamera;

        const echoMsg = `OrbitControlsDirective, CHANGED Camera #${ThreeJScamera.id} ORBIT Control by Mouse event`;
        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('OrbitControlsDirective');

        // Logg it:
        const propsToLogObj: object = { orbitControls: this.controls, listener: evt };
        const loggEvt: LoggEventData = this.loggerService.logg3Devent(
            'OrbitControlsDirective', 'changed', echoMsg, propsToLogObj, this.makeScalarValues(ThreeJScamera), hasConsoleLog
        );

        // Send back to re-render:
        return loggEvt;
    }

    private makeScalarValues(camera: THREE.PerspectiveCamera): Array<SomeNgChanges> {
        const userMouseZoom = this.controls.target.distanceTo( camera.position );
        // TODO:
        const sceneObjects = this.rendererService.getScene();
        // When...? Will delete it... ;-) Hurry up! Murcão!
        const cameraPosition = `(${parseInt(camera.position.x.toString(), 10)}, ` +
                            `${parseInt(camera.position.y.toString(), 10)} ,` +
                            `${parseInt(camera.position.z.toString(), 10)})`
        ;
        // tslint:disable: no-string-literal
        const orbitControlPosition = `(${this.controls['position0'].x}, ` +
                                    `${this.controls['position0'].y}, ` +
                                    `${this.controls['position0'].z})`
        ;
        return this.filterValuesChanged(camera, cameraPosition, orbitControlPosition, userMouseZoom);
    }


    private filterValuesChanged(
        camera: THREE.PerspectiveCamera,
        cameraPosition: string,
        orbitControlPosition: string,
        userMouseZoom: number
    ): Array<SomeNgChanges>  {

        const changedProps: Array<SomeNgChanges> = [];
        const cameraRotation: THREE.Euler = camera.rotation;

        const camPosChg: SomeNgChanges = { whatProp: 'cameraPosition', fromValue: this.cameraPreviousPosition, toValue: cameraPosition };
        if (camPosChg.fromValue !== camPosChg.toValue) {
            changedProps.push(camPosChg);
        }

        const camRotChg: SomeNgChanges = { whatProp: 'cameraRotation', fromValue: this.cameraPreviousRotation, toValue: cameraRotation };
        if (camRotChg.fromValue !== camRotChg.toValue) {
            changedProps.push(camRotChg);
        }

        // tslint:disable-next-line: max-line-length
        const orbPosChg: SomeNgChanges = { whatProp: 'orbitPosition', fromValue: this.orbitPreviousPosition, toValue: orbitControlPosition };
        if (orbPosChg.fromValue !== orbPosChg.toValue) {
            changedProps.push(orbPosChg);
        }

        const zoomChg: SomeNgChanges = { whatProp: 'userMouseZoom', fromValue: this.userMouseZoomPreviousValue, toValue: userMouseZoom };
        if (zoomChg.fromValue !== zoomChg.toValue) {
            changedProps.push(zoomChg);
        }

        // Keep meorizing mouse, camera position and rotation, to use on NEXT (same) event:
        this.cameraPreviousPosition = cameraPosition;
        this.cameraPreviousRotation = cameraRotation;
        this.orbitPreviousPosition = orbitControlPosition;
        this.userMouseZoomPreviousValue = userMouseZoom;

        return changedProps;
    }
}
