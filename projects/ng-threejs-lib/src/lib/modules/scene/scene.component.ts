import { Component, forwardRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as THREE from 'three';

import { Object3dDirective } from './object-3d.directive';
import { PointOfLightComponent } from '../visualization-control/light-components/point-light.component';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../services/app-demo/console-log.service';

import { appliedColor } from '../../util/threejs/applied-color';

import { LoggEventData, SomeNgChanges, UserObj3D } from '../../ng-threejs-lib.interfaces';

@Component({
    selector: 'ng3d-scene',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => SceneComponent) }],
    template: '<ng-content></ng-content>'
})
export class SceneComponent extends Object3dDirective<THREE.Scene> implements OnChanges {

    scene: THREE.Scene;

    // Used in many components; here we just want to catch it:
    DOMcameraLight: THREE.PointLight;

    // @ContentChildren(PointOfLightComponent) DOMpointsOfLight: QueryList<PointOfLightComponent>;
    // Don't need it! It exists already at the extendend 'Object3dDirective' Class.

    @Input() background = 0xffffff;
    @Input() fog = false;
    @Input() fogColor = 0xa0a0a0;
    @Input() fogNear = 10;
    @Input() fogFar = 500;

    // tslint:disable-next-line: max-line-length
    @Input() newObj: UserObj3D; // UserObj3D.obj3DParams is, in DEMO App, 'ComponentMenuType.New3DObject', an Union of Types (Camera, Actor 3D or PointofLight, so far)

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
        // TODO: directive?
        rendererService.setScene(this);
    }

    ngOnChanges(propsChanged: SimpleChanges) {
        // console.error('CHANGES on the Scene:', propsChanged);

        // Still TODO CHANGES on 'background', 'fog', 'fogColor', 'fogNer' and 'fogFar'

        if (propsChanged.newObj && !propsChanged.newObj.firstChange) {
            const new3DObject: UserObj3D = propsChanged.newObj.currentValue;

            switch (new3DObject.type) {
                case 'PointLight':
                    this.userPointOfLight(new3DObject);
                    break;

                // More will be added here:
                case 'Mesh':
                    this.userMesh(new3DObject);
                    break;

                case 'PerspectiveCamera':
                    this.userCamera(new3DObject);
                    break;

                default:
                    console.error('ERROR: some non identified 3D Object TYPE has been added to the current DEMO\'s 3D Scene...');
                    break;
            }
        }

    }

    protected newObject3DInstance(): THREE.Scene {
        this.scene = new THREE.Scene();

        this.scene.background = new THREE.Color(appliedColor(this.background));
        if (this.fog === true) {
            this.scene.fog = new THREE.Fog(appliedColor(this.fogColor), this.fogNear, this.fogFar);
        }

        // Brightness - not a Scene diretion lighting, so much...
        // scene.add( new THREE.AmbientLight( 0xffffff, 0.9 ) );
        //
        // In order to add a Scene Direction Light,
        // just add a PointLight (must exist, or not, on the HTML to render) with the exact Camera position.
        // Once instantiated, don't forget to update it's position each time - including on the HTML Input(), if there...
        // MIND YOU:
        // The Camera moves dynamically! By the user <input /> number OR by Orbit Control's mouse zoom and movement...
        // ;-)

        const echoMsg = `SceneComponent, CREATED a 3d Object SCENE instantiation`;
        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('SceneComponent');

        const scalarValues: Array<SomeNgChanges> = [
            { whatProp: 'type', fromValue: null, toValue: this.scene.type },
            { whatProp: 'uuid', fromValue: null, toValue: this.scene.uuid },
            { whatProp: 'id', fromValue: null, toValue: this.scene.id }
        ];
        // Logg it:
        this.loggerService.logg3Devent('SceneComponent', 'create', echoMsg, this.scene, scalarValues, hasConsoleLog);

        return this.scene;
    }

    // ---------------------------------------------------------------------------------------------------------------------

    private userPointOfLight(customObj3D: UserObj3D) {
        // console.error('User POINTofLIGHT 3D Object into the Scene:', customObj3D);
        const objParams: any = customObj3D.obj3DParams;
        const uuid = objParams.uuid;
        const position = objParams.position;
        const emittance = objParams.emittance;

        if (customObj3D.operation === 'create') {

            // const userLight = new THREE.PointLight( appliedColor(emittance.color), emittance.intensity, emittance.distance );
            // tslint:disable-next-line: max-line-length
            const PointOfLightInstance: PointOfLightComponent = new PointOfLightComponent(this.rendererService, this.loggerService, this.consoleLogService);
            const creationParams: any = {
                name: objParams.name,
                emittance: {
                    color: appliedColor(emittance.color),
                    intensity: emittance.intensity,
                    distance: emittance.distance
                },
                position: {
                    translateX: position.x,
                    translateY: position.y,
                    translateZ: position.z
                }
            };
            const userLight: THREE.PointLight = PointOfLightInstance.newObject3DInstance(creationParams);
            // Light is created! Just add it to the scene and render it:
            this.scene.add(userLight);

            // Now Logg this creation event - the associated Logged Event was done @ PointOfLightComponent's 'newObject3DInstance()'...
            //
            this.rendererService.render(`CREATED "PointLight" "${creationParams.name}"`);
            // console.error(userLight);
        }

        if (customObj3D.operation === 'update') {
            const objToUpdate = this.scene.getObjectByProperty('uuid', uuid) as THREE.PointLight;
            let mustRerender = {} as SomeNgChanges;

            if (position) {
                if (position.translateX) {
                    mustRerender = Object.assign( {}, {
                        whatProp: 'position_X', fromValue: objToUpdate.position.x, toValue: position.translateX
                    });
                }
                if (position.translateY) {
                    mustRerender = Object.assign( {}, {
                        whatProp: 'position_Y', fromValue: objToUpdate.position.y, toValue: position.translateY
                    });
                }

                if (position.translateZ) {
                    mustRerender = Object.assign( {}, {
                        whatProp: 'position_Z', fromValue: objToUpdate.position.z, toValue: position.translateZ
                    });
                }
                // Update;
                objToUpdate.position.set(
                    position.translateX || objToUpdate.position.x,
                    position.translateY || objToUpdate.position.y,
                    position.translateZ || objToUpdate.position.z
                );
            }

            if (emittance) {
                if (emittance.color) {
                    mustRerender = Object.assign( {}, {
                        whatProp: 'color', fromValue: '#' + objToUpdate.color.getHexString(), toValue: '#' + emittance.color.toString(16)
                    });
                    // Update;
                    objToUpdate.color.setHex( emittance.color );
                }

                if (emittance.intensity) {
                    mustRerender = Object.assign( {}, {
                        whatProp: 'intensity', fromValue: objToUpdate.intensity, toValue: emittance.intensity
                    });
                    // Update;
                    objToUpdate.intensity = emittance.intensity;
                }

                if (emittance.distance) {
                    mustRerender = Object.assign( {}, {
                        whatProp: 'distance', fromValue: objToUpdate.distance, toValue: emittance.distance
                    });
                    // Update;
                    objToUpdate.distance = emittance.distance;
                }
            }

            // console.error(objToUpdate);

            // Logg it and render it:
            // tslint:disable-next-line: max-line-length
            const echoMsg = `PointOfLightComponent, CHANGED the ${mustRerender.whatProp} from "${mustRerender.fromValue}" to "${mustRerender.toValue}"`;
            const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('PointOfLightComponent');
            const loggEvt: LoggEventData = this.loggerService.logg3Devent(
                'PointOfLightComponent', 'changed', echoMsg, objToUpdate, [mustRerender], hasConsoleLog
            );
            this.rendererService.render(loggEvt);
        }

        if (customObj3D.operation === 'remove') {
            const objToRemove = this.scene.getObjectByProperty('uuid', uuid) as THREE.PointLight;
            // console.log(objToRemove);
            objToRemove.parent.remove(objToRemove);

            // Logg it and render it:
            const echoMsg = `PointOfLightComponent, REMOVED "${objToRemove.name}" from current DEMO\'s 3D Stage!`;
            const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('PointOfLightComponent');
            const loggEvt: LoggEventData = this.loggerService.logg3Devent(
                'PointOfLightComponent', 'remove', echoMsg, objToRemove, null, hasConsoleLog
            );
            this.rendererService.render(loggEvt);
        }
    }

    private userMesh(customObj3D: UserObj3D) {
        console.error('New User MESH 3D Object into the Scene:', customObj3D);
    }

    private userCamera(customObj3D: UserObj3D) {
        console.error('New User CAMERA 3D Object into the Scene:', customObj3D);
    }
}
