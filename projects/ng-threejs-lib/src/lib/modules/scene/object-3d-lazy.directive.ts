import { Directive, OnInit, OnDestroy } from '@angular/core';

import { Object3dDirective } from './object-3d.directive';

import * as THREE from 'three';

import { LoggEventData, SomeNgChanges } from '../../ng-threejs-lib.interfaces';

@Directive({})
export abstract class Object3dLazyDirective extends Object3dDirective<THREE.Object3D> implements OnInit, OnDestroy {

    /**
     * Flag to signal whether the parent class instance Object3dDirective called the
     * overwritten method {@link LoaderDirective#afterInit} yet.
     *
     * Unless that method was called, no methods and properties of {@link Object3dDirective}
     * may be safely accessed, especially {@link Object3dDirective#addChild} and
     * {@link Object3dDirective.renderer}.
     */
    private parentInitialized = false;

    /**
     * This is reference to lazy loaded Object3D (async after init)
     */
    protected lazyObject: THREE.Object3D | undefined;

    ngOnInit() {

    }

    /**
     * Abstract method for lazy loading
     *
     */
    protected abstract async loadLazyObject(): Promise<THREE.Object3D>;

    protected afterInit() {
        super.afterInit();
        this.parentInitialized = true;
        // ---------------
        // Are you sure? 'loader' directive is alreay calling it!
        // ---------------
        // this.startLoading();
        // ---------------
    }

    ngOnDestroy(): void {
        if (this.lazyObject) {
            super.removeChild(this.lazyObject);
        }
    }

    protected startLoading() {

        // tslint:disable: variable-name
        const echoMsg_0 = 'Object3dLazyDirective, INITIALIZATION of an Obj3D lazy loading DIRECTIVE';
        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('Object3DLazyDirective');
        // Logg it:
        this.loggerService.logg3Devent('Object3DLazyDirective', 'intialize', echoMsg_0, null, null, hasConsoleLog);

        // Trigger Model acquisition now that the parent has been initialized.
        this.loadLazyObject()
            .then((obj: THREE.Object3D) => {
                // remove old if exists
                if (this.lazyObject) {
                    super.removeChild(this.lazyObject);
                }

                // add lazy object to scene
                this.lazyObject = obj;
                super.addChild(obj);

                const echoMsg_1 = 'Object3DLazyDirective, finish lazy loading => CREATED an Obj3D CHILD';
                const scalarValues: Array<SomeNgChanges> = [
                    { whatProp: 'type', fromValue: null, toValue: obj.type },
                    { whatProp: 'uuid', fromValue: null, toValue: obj.uuid },
                    { whatProp: 'name', fromValue: null, toValue: obj.name }
                ];
                // Logg it:
                const loggEvt: LoggEventData = this.loggerService.logg3Devent(
                    'Object3DLazyDirective', 'create', echoMsg_1, obj, scalarValues, hasConsoleLog
                );

                // And Render it.
                this.rendererService.render(loggEvt);

            })
            .catch(err => {
                console.error(err);
            });
    }

    protected newObject3DInstance(): THREE.Object3D {
        // Just empty object (holder of lazy object)
        return new THREE.Object3D();
    }

}
