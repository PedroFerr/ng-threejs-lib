import {
    Directive, OnInit, ContentChildren, ViewChildren, QueryList, Input, Output, EventEmitter,
    OnChanges, SimpleChanges, AfterViewInit, OnDestroy
} from '@angular/core';
import { Subscription } from 'rxjs';

import * as THREE from 'three';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../services/app-demo/console-log.service';

import { SomeNgChanges } from '../../ng-threejs-lib.interfaces';

@Directive({})
export abstract class Object3dDirective<T extends THREE.Object3D> implements OnInit, OnChanges, AfterViewInit, OnDestroy {

    // Must have @ContentChildren (@ViewChildren not good for all!) because, for some parents, this is in an <ng-content></ng-content>
    // (ViewChildren don’t include elements that exist within the 'ng-content' tag.)
    @ContentChildren(Object3dDirective, { descendants: false }) childNodes: QueryList<Object3dDirective<THREE.Object3D>>;
    // Besides, do remember that the QueryList, of the children within the parent content,
    // is initialized immediately before the ngAfterContentInit() lifecycle hook.
    // So you can manipulate it safely at AfterViewInit() - check Angular life cycle hooks order: https://angular.io/guide/lifecycle-hooks
    // @ContentChildren vs @ViewChildren:
    // check nice demoing in here https://netbasal.com/understanding-viewchildren-contentchildren-and-querylist-in-angular-896b0c689f6e

    @ViewChildren(Object3dDirective) viewChilds: QueryList<Object3dDirective<THREE.Object3D>>;

    protected mustRerender = [] as Array<SomeNgChanges>;

    // ================================================================================
    // Properties ALL 3D Components (Mesh, OBJ loaded, Scene, Points of Light, etc.)
    // HAVE
    // tagged on their Angular's components Template containers, on HTML's [@Input()]
    // ================================================================================

    /**
     * 'monitor-debug', as the most far nested component of this 'obj-loader-demo',
     * has reached 'ngAfterViewInit()' life cycle hook.
     *
     * As so, we can assume all DEMO Angular components have mounted and are now stable.
     * Meaning... only NOW we should trigger
     * the mounting of ANY 3D Actor, lying on the Stage currently being rendered, @ DEMO component
     */
    @Input() afterDEMOisLoaded: boolean;

    @Input() name: string;

    // Rotation of the 3D object, in Euler angles (radians) with order X, Y, Z.
    // Translation of the 3D Object. This is typically done as a ONE TIME operation, and NOT during an animation loop.
    // -----------------------------------------------------------
    // Don't froget to contain any 3D Object inside this DIRECTIVE (<ng3d-object3d-container />), at each DEMO Template,
    // if you want translation, and rotation, to work in a GENERIC way for ANY currently rendering 3D Object (lights, Scene, etc. included)
    @Input() translateX: number;
    @Input() translateY: number;
    @Input() translateZ: number;

    @Input() rotateX: number;
    @Input() rotateY: number;
    @Input() rotateZ: number;

    // ================================================================================

    // @Output() changed = new EventEmitter<void>();
    // changedSubscription: Subscription;
    // What is upper supposed to do...?

    // -----------------------------------------------------------

    private object: T;
    protected abstract newObject3DInstance(): T;

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) { }

    ngOnInit() {
        // this.changedSubscription = this.changed.subscribe(() => {

        //     const echoMsg = 'Object3Ddirective, INITIALIZING an Obj3D instance';
        //     const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('Object3Ddirective');

        //     const scalarValues: Array<SomeNgChanges> = [
        //         { whatProp: 'type', fromValue: null, toValue: this.object.type },
        //         { whatProp: 'uuid', fromValue: null, toValue: this.object.uuid },
        //         { whatProp: 'id', fromValue: null, toValue: this.object.id }
        //     ];
        //     // Logg it:
        //     const loggEvt: LoggEventData = this.loggerService.logg3Devent(
        //         'Object3Ddirective', 'intialize', echoMsg, this.object, scalarValues, hasConsoleLog
        //     );
        //     this.rendererService.render(loggEvt);
        // });
    }

    ngOnChanges(propsChanged: SimpleChanges) {
        // if (!this.object) { return; }
        // Is upper condition needed? thisobject must ALWAYS exist, if we reach 'ngOnChanges()'...
        //
        // console.error('SHOULD NEVER BE SEEN: Object3dDirective changes:', Object.assign({}, propsChanged));
        //
        // In fact, we only detect HERE:
        // 1) the ThreeJS helpers (remember they are also ThreeJS's 3D Objects) changes - Axes and Grid ones
        // 2 ) and 3d Ojects contained (HTML) inside <ng3d-object3d-container />, if any, translation/rotation movements.
        //
        // but NO (other) Lib's Object3D changes.
        // Let's see why:
        //
        // Each Mesh (Cube, Cylinder, Sphere, etc) have their own ngOnChanges() detected on their own '.component.ts',
        // that call common 'ngMeshOnChanges()' method @ Mesh 3D Directive.
        //
        // Other than Mesh 3d Objects, we only have, so far, 'OBJ' loaded file Objects that have detected changes @
        // '../object-loaders/obj-loader.component.ts' that extends LoaderDirective, that extends 'Object3dLazyDirective' Class
        // and NOT this 'Object3dDirective' Class.
        //
        // So, in fact, yes
        // --------------------------------------------------
        // NEXT block of CODE is currently doing NOTHING...?!?!?
        // --------------------------------------------------
        // if we do not put inside <ng3d-object3d-container /> container 3D Objects we want to "group" and treat their transl/rot moves
        // Check it @ './visualization-control/threejs-helpers/object-3D-container-helper.component.ts'
        // --------------------------------------------------

        // let modified = false;

        // if (['rotateX', 'rotateY', 'rotateZ'].some(propName => propName in propsChanged)) {
        //     this.applyRotation();
        //     modified = true;
        // }
        // if (['translateX', 'translateY', 'translateZ'].some(propName => propName in propsChanged)) {
        //     this.applyTranslation();
        //     modified = true;
        // }

        // if (modified) {
        //     // this.changed.emit();
        // }

    }

    ngAfterViewInit() {
        // ===================================================
        // ======================================
        this.object = this.newObject3DInstance();
        // ======================================
        // ===================================================
        // This is the point where ALL ThreeJS 3D OBJECTs,
        // including Lights, Scene, ThreeJS helpers, ANY Lib 3D Objects, etc.
        // are rendered on the screen by Angular.
        // They already have been rendered on WebGL <canvas />
        // ===================================================
        //
        // console.log('Object3dDirective.ngAfterViewInit ' + this.name, this);

        this.applyTranslation();
        this.applyRotation();

        this.collectChilds();

        this.afterInit();
    }

    ngOnDestroy() {
        if (this.object && this.object.parent) {
            this.object.parent.remove(this.object);
        }

        // if (this.changedSubscription) {
        //     this.changedSubscription.unsubscribe();
        // }
    }

    public getObject(): T {
        return this.object;
    }

    public collectChilds() {
        // console.log('Collect childs for', this.name);
        if (this.childNodes !== undefined && this.childNodes.length > 1) {
            this.childNodes.filter(i => i !== this && i.getObject() !== undefined).forEach(i => {
                // console.log('Add childNodes for', this.name, i);
                this.addChild(i.getObject());
            });
        } else {
            // console.log("No child Object3D for: " + this.constructor.label);
        }


        if (this.viewChilds !== undefined && this.viewChilds.length > 0) {
            this.viewChilds.filter(
                i => i !== this
                    && i.getObject() !== undefined
                    && !i.getObject().parent /* direct childs only */
            ).forEach(i => {
                // console.log('Add viewChilds for', this.name, i);
                this.addChild(i.getObject());
            });
        } else {
            // console.log("No child Object3D for: " + this.constructor.label);
        }

    }

    public addChild(object: THREE.Object3D): void {
        this.object.add(object);
    }

    protected removeChild(object: THREE.Object3D): void {
        this.object.remove(object);
    }

    protected applyTranslation(): void {
        this.object.position.set(
            this.translateX || 0,
            this.translateY || 0,
            this.translateZ || 0
        );
    }

    protected applyRotation(): void {
        this.object.rotation.set(
            this.rotateX || 0,
            this.rotateY || 0,
            this.rotateZ || 0,
            'XYZ'
        );
    }

    protected afterInit() {
        // this.changed.emit();
    }

}
