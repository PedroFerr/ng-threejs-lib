export * from './object-loaders/loader.directive';
export * from './object-loaders/obj-loader.component';
export * from './object-loaders/object-loader.component';
export * from './object-loaders/svg-loader.component';

export * from './object-3d-lazy.directive';
export * from './object-3d.directive';
export * from './scene.component';

export * from './z-scene.module';
