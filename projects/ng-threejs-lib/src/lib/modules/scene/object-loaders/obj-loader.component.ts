import { Component, forwardRef, Input, OnChanges, SimpleChanges } from '@angular/core';

import * as THREE from 'three';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader';

import { Object3dDirective } from '../object-3d.directive';
import { LoaderDirective } from './loader.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

import { SomeNgChanges } from '../../../ng-threejs-lib.interfaces';

import { waitForFinalEvent } from '../../../util/js/timeout-controller';
import { returnNowsTime } from '../../../util/js/returnNowsTime';

/**
 * Component for employing THREE.OBJLoader to load [Wavefront *.obj files][1].
 *
 * [1]: https://en.wikipedia.org/wiki/Wavefront_.obj_file
 */
@Component({
    selector: 'ng3d-obj-loader',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => ObjLoaderComponent) }],
    template: '<ng-content></ng-content>'
})
export class ObjLoaderComponent extends LoaderDirective implements OnChanges {

    private loader = new OBJLoader();
    private mtlLoader = new MTLLoader();

    // /**
    //  * 'monitor-debug', as the most far nested component of this 'obj-loader-demo',
    //  * has reached 'ngAfterViewInit()' life cycle hook.
    //  *
    //  * As so, we can assume all DEMO Angular components have mounted and are now stable
    //  * Meaning... only NOW we should trigger the mounting of 'obj-loader'.
    //  */
    // @Input() afterDEMOisLoaded: boolean;

    // /**
    //  * The model data source (usually a URI).
    //  * Settings this property only hides the previous model upon successful
    //  * loading of the new one. This especially means that if the new data source
    //  * is invalid, the old model will *not* be removed from the scene.
    //  */
    // @Input() public set model(newModelUrl: string) {
    //     if (this.thisModel !== newModelUrl) { this.thisModel = newModelUrl; }
    // }
    // public get model(): string {
    //     return this.thisModel;
    // }


    /**
     * The material for the Model to load via OBJ file:
     */
    @Input() material: string;

    /**
     * Path relative to which resources and textures within the loaded obj file are loaded.
     * @deprecated Rather use resourcePath
     */
    @Input() public set texturePath(newTexturePath: string) {
        this.resourcePath = newTexturePath;
    }
    /**
     * Path relative to which resources and textures within the loaded obj file are loaded.
     */
    @Input() resourcePath: string;

    constructor(
        public rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    ngOnChanges(propsChanged: SimpleChanges) {
        // console.warn('propsChanged:', Object.assign( {}, propsChanged));

        const readyToStart3Dloading = propsChanged.afterDEMOisLoaded;

        if (readyToStart3Dloading && readyToStart3Dloading.currentValue && !readyToStart3Dloading.firstChange) {

            const echoMsg = `ObjLoaderComponent, INITIALIZATION of an Obj3D loading, by URL: "${this.thisModel}"`;
            const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('ObjLoaderComponent');

            // We think this is a VERY important point to outstand:
            if (hasConsoleLog) {
                console.log('%%%%%%%__startLoading()_3D_actor(s)_MODEL(s)__%%%%%%%');
                console.log(this.thisModel);
            }


            // Start counting for thr brginning of games:
            waitForFinalEvent ( () => {
                // Logg it:
                this.loggerService.logg3Devent('ObjLoaderComponent', 'intialize', echoMsg, this.loader, null, hasConsoleLog);

                // ---------------
                this.startLoading();    // @ Object3dLazyDirective THAT extends Object3dDirective
                // ---------------
                // Upper wil come this one, "placed" on this OBJ file loader - exclusively....?
                // ---------------
                // this.loadLazyObject();
                // ---------------
            }, 0, `Unique Id #${returnNowsTime()}` );

        }
    }

    protected async loadLazyObject(): Promise<THREE.Object3D> {
        // console.log('###################### Iniside "protected async loadLazyObject() {...}"');

        // Preloading step for the material
        const preloadingStep = new Promise<THREE.Object3D>((resolve, reject) => {
            if (this.material === undefined) {
                // No preloading necessary
                resolve();
            } else {
                // TODO: If typings of mtlLoader are included in the Three.js NPM
                // package, remove this 'any' cast.
                (this.mtlLoader as any).setResourcePath(this.resourcePath);

                this.mtlLoader.load(this.material, materialCreator => {
                    materialCreator.preload();
                    this.loader.setMaterials(materialCreator);

                    // ---------------
                    resolve();
                    // ---------------

                }, undefined, reject);
            }
        });

        // Await preloading and load final model
        return preloadingStep
            .then(() => {
                if (this.material !== undefined) {

                    const echoMsg = `ObjLoaderComponent, INITIALIZATION of OBJ file to load MODEL - MATERIAL pre loading: ${this.material}`;
                    const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('ObjLoaderComponent');
                    // Logg it:
                    this.loggerService.logg3Devent('ObjLoaderComponent', 'intialize', echoMsg, null, null, hasConsoleLog);
                }
            })
            .then(() => {
                return new Promise<THREE.Object3D>((resolve, reject) => {
                    this.loader
                        .load(this.model, model => {    // this.model is a getter @ LoaderDirective

                            // ---------------
                            resolve(model);
                            // ---------------

                            const echoMsg = `ObjLoaderComponent, CREATED a MODEL from OBJ file Loader`;
                            const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('ObjLoaderComponent');

                            const scalarValues: Array<SomeNgChanges> = [
                                { whatProp: 'type', fromValue: null, toValue: model.type },
                                { whatProp: 'uuid', fromValue: null, toValue: model.uuid },
                                { whatProp: 'id', fromValue: null, toValue: model.id }
                            ];
                            // Logg it:
                            this.loggerService.logg3Devent('ObjLoaderComponent', 'create', echoMsg, model, scalarValues, hasConsoleLog);

                        }, undefined, reject)
                    ;
                });
            })
        ;
    }
}
