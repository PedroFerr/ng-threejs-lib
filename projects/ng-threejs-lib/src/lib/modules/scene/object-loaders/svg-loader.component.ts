import { Component, forwardRef, Input } from '@angular/core';
import * as THREE from 'three';
import { SVGLoader } from 'three/examples/jsm/loaders/SVGLoader';

import { Object3dDirective } from '../object-3d.directive';
import { LoaderDirective } from './loader.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

import { appliedColor } from 'projects/ng-threejs-lib/src/lib/util/threejs/applied-color';
import { appliedMaterial } from 'projects/ng-threejs-lib/src/lib/util/threejs/applied-material';
import { fixCenter } from 'projects/ng-threejs-lib/src/lib/util/threejs/fix-center';
import { scaleToFit } from 'projects/ng-threejs-lib/src/lib/util/threejs/scale-to-fit';

import { SomeNgChanges } from '../../../ng-threejs-lib.interfaces';

@Component({
    selector: 'ng3d-svg-loader',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => SVGLoaderComponent) }],
    template: '<ng-content></ng-content>'
})
export class SVGLoaderComponent extends LoaderDirective {

    private thisMaterialColor: number = undefined;

    @Input()
    get overrideMaterialColor(): number {
        return this.thisMaterialColor;
    }

    set overrideMaterialColor(value: number) {
        this.thisMaterialColor = value;
        // ---------------
        // Are you sure? 'loader' directive is alreay calling it!
        // CHECK id it's needed, once we have an SVG in Stage...
        // ---------------
        this.startLoading();
        // ---------------
    }

    @Input() material = 'basic';

    @Input() depthWrite = true;

    @Input() maxX: number;
    @Input() maxY: number;

    @Input() centered = true;

    private loader = new SVGLoader();

    constructor(
        public rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    protected async loadLazyObject(): Promise<THREE.Object3D> {
        // console.log('ObjectLoaderComponent.loadLazyObject');

        return new Promise<THREE.Object3D>((resolve, reject) => {
            this.loader
                .load(this.model, data => {    // this.model is a getter @ LoaderDirective
                    const paths = data.paths;
                    const group = new THREE.Group();

                    // tslint:disable-next-line: prefer-for-of
                    for (let i = 0; i < paths.length; i++) {
                        // NOTE: It seems that ShapePath does not includes typed color, cast to any as workaround
                        const path: any = paths[i];
                        const color = (this.thisMaterialColor ? appliedColor(this.thisMaterialColor) : path.color);
                        const material = appliedMaterial(color, this.material, this.depthWrite);
                        const shapes = path.toShapes(true, {});

                        // tslint:disable-next-line: prefer-for-of
                        for (let j = 0; j < shapes.length; j++) {
                            const shape = shapes[j];
                            const geometry = new THREE.ShapeBufferGeometry(shape);
                            const mesh = new THREE.Mesh(geometry, material);
                            group.add(mesh);
                        }
                    }

                    if (this.maxX || this.maxY) { scaleToFit(group, new THREE.Vector3(this.maxX, this.maxY, 0)); }
                    if (this.centered) { fixCenter(group); }

                    // ---------------
                    resolve(group);
                    // ---------------

                    const echoMsg = `SVGLoaderComponent, CREATED a MODEL from SVG (group) Loader`;
                    const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('SVGLoaderComponent');

                    const scalarValues: Array<SomeNgChanges> = [
                        { whatProp: 'type', fromValue: null, toValue: group.type },
                        { whatProp: 'uuid', fromValue: null, toValue: group.uuid },
                        { whatProp: 'name', fromValue: null, toValue: group.name },
                        { whatProp: 'model', fromValue: null, toValue: this.model }
                    ];
                    // Logg it:
                    this.loggerService.logg3Devent('SVGLoaderComponent', 'create', echoMsg, group, scalarValues, hasConsoleLog);

                }, undefined, reject)
            ;
        });
    }


}
