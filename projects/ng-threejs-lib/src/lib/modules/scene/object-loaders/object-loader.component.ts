import { Component, forwardRef } from '@angular/core';
import * as THREE from 'three';

import { Object3dDirective } from '../object-3d.directive';
import { LoaderDirective } from './loader.directive';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../../services/app-demo/console-log.service';

import { SomeNgChanges } from '../../../ng-threejs-lib.interfaces';

@Component({
    selector: 'ng3d-object-loader',
    providers: [{ provide: Object3dDirective, useExisting: forwardRef(() => ObjectLoaderComponent) }],
    template: '<ng-content></ng-content>'
})
export class ObjectLoaderComponent extends LoaderDirective {

    private loader = new THREE.ObjectLoader();

    constructor(
        public rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    protected async loadLazyObject() {
        // console.log('ObjectLoaderComponent.loadLazyObject');

        return new Promise<THREE.Object3D>((resolve, reject) => {
            this.loader
                .load(this.model, model => {    // this.model is a getter @ LoaderDirective
                    // BUG #95: it seems that some textures loaded after last render (and model has black texture instead)
                    //
                    // Maybe 'texture.needsUpdate = true;' is missing....?
                    // https://threejs.org/docs/index.html#manual/en/introduction/How-to-update-things

                    // ---------------
                    resolve(model);
                    // ---------------

                    const echoMsg = `ObjLoaderComponent, CREATED a MODEL from Object Loader`;
                    const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('ObjectLoaderComponent');

                    const scalarValues: Array<SomeNgChanges> = [
                        { whatProp: 'type', fromValue: null, toValue: model.type },
                        { whatProp: 'uuid', fromValue: null, toValue: model.uuid },
                        { whatProp: 'name', fromValue: null, toValue: model.name }
                    ];
                    // Logg it:
                    this.loggerService.logg3Devent('ObjectLoaderComponent', 'create', echoMsg, model, scalarValues, hasConsoleLog);

                }, undefined, reject)
            ;
        });
    }

}
