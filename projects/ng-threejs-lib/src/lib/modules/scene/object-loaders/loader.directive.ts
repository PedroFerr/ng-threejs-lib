import { OnInit, Input, Directive, OnChanges } from '@angular/core';

import { Object3dLazyDirective } from '../object-3d-lazy.directive';

import { LoggEventData } from '../../../ng-threejs-lib.interfaces';

/**
 * Helper parent class for model loader.
 *
 * @see ObjLoaderComponent
 */
@Directive({})
export abstract class LoaderDirective extends Object3dLazyDirective implements OnInit, OnChanges {

    protected thisModel: string;
    protected loggEvt: LoggEventData;

    /**
     * 'monitor-debug', as the most far nested component of this 'obj-loader-demo',
     * has reached 'ngAfterViewInit()' life cycle hook.
     *
     * As so, we can assume all DEMO Angular components have mounted and are now stable
     * Meaning... only NOW we should trigger the mounting of 'obj-loader'.
     */
    @Input() afterDEMOisLoaded: boolean;

    /**
     * The model data source (usually a URI).
     * Settings this property only hides the previous model upon successful
     * loading of the new one. This especially means that if the new data source
     * is invalid, the old model will *not* be removed from the scene.
     */
    @Input() public set model(newModelUrl: string) {
        if (this.thisModel !== newModelUrl) { this.thisModel = newModelUrl; }
    }
    public get model(): string {
        return this.thisModel;
    }

    // ngOnChanges(propsChanged: SimpleChanges) {
    //     console.warn('propsChanged:', propsChanged);

    //     const readyToStart3Dloading = propsChanged.afterDEMOisLoaded;

    //     if (readyToStart3Dloading && readyToStart3Dloading.currentValue && !readyToStart3Dloading.firstChange) {
    //         // No 'if's. Next is ALWAYS consoled:
    //         console.error('%%%%%%%__startLoading()_3D_actor(s)_MODEL(s)__%%%%%%%');

    //         // ---------------
    //         // super.startLoading();    // NOPES! Make it SYNCRONOUS (below), with 3D Angular affairs.
    //         // ---------------

    //         this.loggEvt = {
    //             msg: `LoaderDirective, INITIALIZATION of an Obj3D loading, by URL: "${this.thisModel}"`,
    //             data: {
    //                 // consoleLogIt: this.consoleLogService.loaderDirective,
    //                 where: 'LoaderDirective',
    //                 operation: 'intialize',
    //                 scalarValues: null,
    //                 onWhat: {url: this.thisModel}
    //             }
    //         };
    //         if (this.consoleLogService.loaderDirective) {
    //             console.error('3D STAGE main ACTOR(s) LOADING event WILL start @ ' + this.loggEvt.data.when, this.loggEvt.data);
    //         }

    //         // Give it some time so '../scene/object-3d-lazy.directive.ts' doesn't START erroneously...
    //         // setTimeout(() => super.startLoading(), 5000);
    //         // Better: make it SYNCHRONOUS, delaying it till 'startLoading()' is called @ '../scene/object-3d-lazy.directive.ts'

    //         // This is where the main OBJ loaded Actor (3D) starts loading
    //         // - when all the other 3D (axis, camera, orbit, etc.) and Angular components are supposed to already be there.
    //         // Angular has, itself, a lot to load - 1st mounting of ALL 3D Components!
    //         // So give time for Angular to mount the INIT of everything, and only then fire 3D loading/modeling/rendering.
    //         waitForFinalEvent ( () => {
    //             if (this.consoleLogService.loaderDirective) {
    //                 console.error('****************************************************');
    //                 console.warn(
    //                     `3D STAGE main ACTOR BEGINNING OF TIMES, after 'waitForFinalEvent'` +
    //                     `-  STAGE and ANGULAR Components HAVE, now, been loaded and rendered!`); // , this.loggEvt.data);
    //                 console.error('****************************************************');

    //                 console.error('====== START loading a MODEL, by URL:', this.thisModel);
    //             }
    //             // Logg it:
    //             this.loggerService.addLog(this.loggEvt);

    //             // ---------------
    //             super.startLoading();
    //             // ---------------

    //         }, 50000, `Unique Id #${this.loggEvt.data.when}` );
    //         // ---------------
    //     }
    // }


}
