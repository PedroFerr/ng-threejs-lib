import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SceneComponent } from './scene.component';

import { ObjectLoaderComponent } from './object-loaders/object-loader.component';
import { ObjLoaderComponent } from './object-loaders/obj-loader.component';
import { SVGLoaderComponent } from './object-loaders/svg-loader.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        SceneComponent,
        ObjectLoaderComponent,
        ObjLoaderComponent,
        SVGLoaderComponent
    ],
    exports: [
        SceneComponent,
        ObjectLoaderComponent,
        ObjLoaderComponent,
        SVGLoaderComponent
    ]
})
export class SceneModule {}
