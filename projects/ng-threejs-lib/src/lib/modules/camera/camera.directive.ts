import { Directive, AfterViewInit, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import * as THREE from 'three';

import { SceneComponent } from '../scene/scene.component';

import { RendererService } from '../..//services/lib-threejs/renderer.service';
import { LoggerService } from '../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../services/app-demo/console-log.service';

import { SomeNgChanges, LoggEventData, AxisType, AxisTypeInit } from '../../ng-threejs-lib.interfaces';

import { CheckWhatChangedOn, CheckWhatMOVEchanged } from '../../util/threejs/check-ng-changes';

@Directive({})
export abstract class CameraDirective<T extends THREE.Camera> implements OnChanges, AfterViewInit {

    camera: T;
    currentLookAtCenter: boolean;
    currentAxisUp: AxisType;

    scene: SceneComponent;

    protected mustRerender = [] as Array<SomeNgChanges>;

    @Input() name: string;

    // We need 'lookAtCenter' to be true/false, even before changes (by default, should be set initially to 'true').
    // And it's not guaranteed this param is set @ current DEMO's 'xxxxx--demo.stage-params' - if not, set the default one:
    @Input('lookAtCenter')  set setCurrentLookAtCenter(bool: boolean) { this.currentLookAtCenter = bool !== undefined ? bool : true; }
    @Input() positionX: number;
    @Input() positionY: number;
    @Input() positionZ: number;

    // We need some initial 'axisUp' (AxisTypeInit), even before changes.
    // And it's not guaranteed this param is set @ current DEMO's 'xxxxx--demo.stage-params' - if not, set the default one:
    @Input('axisUp') set setCurrentAxisUp(axis: AxisType) { this.currentAxisUp = axis || AxisTypeInit; }
    @Input() rotateX: number;
    @Input() rotateY: number;
    @Input() rotateZ: number;

    @Input() fov: number;
    @Input() near: number;
    @Input() far: number;

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) { }

    ngOnChanges(propsChanged: SimpleChanges) {
        // console.error('SimpleChanges @ camera.directive:', Object.assign({}, propsChanged));
        // console.error('currentLookAtCenter:', this.currentLookAtCenter);
        // console.error('currentAxisUp:', this.currentAxisUp);

        // ----------------------------------------------------------------------------------------

        if (CheckWhatChangedOn(this, propsChanged, 'setCurrentLookAtCenter')) {
            const echoMsg = `CameraDirective, CHANGED the "lookAtCenter" param to '${propsChanged.setCurrentLookAtCenter.currentValue}'`;
            const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('CameraDirective');
            // Logg it:
            const loggEvt: LoggEventData = this.loggerService.logg3Devent(
                'CameraDirective', 'changed', echoMsg, propsChanged, null, hasConsoleLog
            );
            // Render it, for user to see it:
            this.applyAllInputData();
            this.rendererService.render(loggEvt);

        }

        if (['positionX', 'positionY', 'positionZ'].some(propName => propName in propsChanged)) {
            CheckWhatMOVEchanged(this, propsChanged, 'position', 'CameraDirective', this.camera);
        }

        // ----------------------------------------------------------------------------------------

        if (CheckWhatChangedOn(this, propsChanged, 'setCurrentAxisUp')) {

            // tslint:disable-next-line: max-line-length
            const echoMsg = `CameraDirective, CHANGED the "axisUp" param from "${propsChanged.setCurrentAxisUp.previousValue}" to "${propsChanged.setCurrentAxisUp.currentValue}"`;
            const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('CameraDirective');
            // Logg it:
            const loggEvt: LoggEventData = this.loggerService.logg3Devent(
                'CameraDirective', 'changed', echoMsg, propsChanged, null, hasConsoleLog
            );

            // this.applyAxisUp();
            // OK. We have now set a NEW 'axisUp', but user does not see any changes...
            //
            // New up axis and some rotation movement will be seen on Front End as soon as the User types
            // a new value for this axis <ng3d-input-block /> Input...
            // But would be cool if, right away, we show the User a new axis is efectively up, preserving transl/rot coords:
            //
            this.applyAllInputData();

            // Render it, for user to see it:
            this.rendererService.render(loggEvt);
        }

        if (['rotateX', 'rotateY', 'rotateZ'].some(propName => propName in propsChanged)) {
            CheckWhatMOVEchanged(this, propsChanged, 'rotate', 'CameraDirective', this.camera);
        }

        // ----------------------------------------------------------------------------------------

        if (CheckWhatChangedOn(this, propsChanged, 'fov', 'near', 'far')) {
            this.updateFrustum(this.fov, this.near, this.far);

            const echoMsg = `PerspectiveCameraComponent, CHANGED the "frustum" params`;
            const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('CameraDirective');
            // Logg it:
            const loggEvt: LoggEventData = this.loggerService.logg3Devent(
                'PerspectiveCameraComponent', 'changed', echoMsg, propsChanged, null, hasConsoleLog
            );
            // Render it:
            this.rendererService.render(loggEvt);
        }

    }

    ngAfterViewInit() {
        // console.log('CameraDirective.ngAfterViewInit');
        this.createCamera();

        this.applyAllInputData();

        this.rendererService.setCamera(this);
        // We have NOW a REAL camera on the screen! ;-)

        // Logg it:
        const cameraPosition = `(${parseInt(this.camera.position.x.toString(), 10)}, ` +
                `${parseInt(this.camera.position.y.toString(), 10)} ,` +
                `${parseInt(this.camera.position.z.toString(), 10)})`
        ;
        const cameraFrustum = `fov: ${this.fov}, near: ${this.near}, far: ${this.far}`;
        // tslint:disable: no-string-literal
        const cameraRotation = `(${Number(this.camera.rotation.x.toString()).toFixed(2)}, ` +
                `${Number(this.camera.rotation.y.toString()).toFixed(2)} ,` +
                `${Number(this.camera.rotation.z.toString()).toFixed(2)})`
        ;
        // Finally:
        const scalarValues: Array<SomeNgChanges> = [
            { whatProp: 'type', fromValue: null, toValue: this.camera.type },
            { whatProp: 'uuid', fromValue: null, toValue: this.camera.uuid },
            { whatProp: 'name', fromValue: null, toValue: this.camera.name },
            { whatProp: 'position', fromValue: null, toValue: cameraPosition },
            { whatProp: 'frustum', fromValue: null, toValue: cameraFrustum },
            { whatProp: 'rotation', fromValue: null, toValue: cameraRotation }
        ];
        const echoMsg = `CameraDirective, CREATED "${this.camera.type}"`;
        const hasConsoleLog: boolean = this.consoleLogService.isThisBeingConsoled('CameraDirective');
        this.loggerService.logg3Devent('CameraDirective', 'create', echoMsg, this.camera, scalarValues, hasConsoleLog);
    }

    private applyAllInputData() {
        this.applyAxisUp();
        this.applyTranslation();
        // this.applyRotation(this.currentAxisUp === 'x', this.currentAxisUp === 'y', this.currentAxisUp === 'z');
        //
        // NOPES!
        // Will change some x,y,z pair of camera (translation) position!
        //
        // Apply, instead, a (difference of) 0º rotation, just to reflect current user input changes:
        // this.camera.rotation.set(0, 0, 0, 'XYZ');
        // this.camera.rotation.set(this.rotateX, this.rotateY, this.rotateZ, 'XYZ');
        // this.camera.lookAt(0, 0, 0);
    }

    protected abstract createCamera(): void;

    protected applyTranslation(): void {
        if (this.camera) {
            this.camera.position.set(
                this.positionX || 0,
                this.positionY || 0,
                this.positionZ || 0,
            );
        }
        // "Always" focus on the center Stage - otherwise mouse/TRANSL moves will "change" what User is seeing at each change
        // It's up to the User to set it...
        if (this.currentLookAtCenter === true) {
            this.camera.lookAt(0, 0, 0);
        }

    }

    protected applyAxisUp(): void {
        if (this.camera) {
            // console.error(`Axis UP: ${this.currentAxisUp} one`);
            // Y one...?
            // this.camera.up.set(0, 1, 0);
            this.camera.up.set(
                this.currentAxisUp === 'x' ? 1 : 0,
                this.currentAxisUp === 'y' ? 1 : 0,
                this.currentAxisUp === 'z' ? 1 : 0,
            );
        }
    }

    protected applyRotation(xChgs: boolean, yChgs: boolean, zChgs: boolean): void {

        // this.camera.rotation.set(
        //     this.rotateX || 0,
        //     this.rotateY || 0,
        //     this.rotateZ || 0,
        //     'XYZ'
        // );

        // If we just rotate the camera, we'll stop seing, very soon (if we keep incrementing any axis value), the entire Stage.
        // The camera is just turning "away" from the Stage Scene (imagine a head's person knoding up/down, left/right or closer/far)
        //
        // Say we want to rotate "the current view" over the Y axis.
        //
        // We have to do a compose movement of:
        //     1) translation, over 2 axis (X and Z), to get "around" the whole Stage
        //     2) but, at the same time, we have to keep the camera sort of looking back, turning it (rotating over Y)
        // to always look at the "center" of the Stage, and never lost it's line of sight.
        //
        // On the other hand, this 2 axis increase/decrease movement bit by bit, have to be "circular",
        // so we keep the same distance AROUND the Stage.
        // Îf they were "linear", we would get far and far away from the Stage - it would be like as if we were drawing a diagonal line,
        // from the place were we've started... so, all we have to do is BENDING it, curve it, over a same RADIUS perimeter.
        // And keep looking (facing the camera) to that same Stage "center" point spot - not necessarily (0,0,0),
        // it must be the one we were lookng to, once the Stage finished it's rendering, or after some change on it.

        // ---------------------------------------------------------
        // Say we want to rotate "the current view" over the Y axis.
        // ---------------------------------------------------------
        // console.error(this.camera.rotation);
        // const centerPoint = Object.assign( {}, this.camera.position);
        // // The received INPUT, from the User - a numeric value, representing "degrees":
        // const angle = this.rotateY;
        // // The perimeter we want to rotate along:
        // const bendedPathRadius = this.camera.position.y;

        // this.camera.position.set(
        //     // x NEW position - the tiny bit increment over one (X) of the 2 other axis (from Y)
        //     0 + bendedPathRadius * Math.cos(angle),
        //     // y NEW position - in fact is the same quota! We must - rotation occurs on translation OVER the OTHER 2 axis
        //     bendedPathRadius,
        //     // z NEW position - the tiny bit SAME increment over the other one (Z) of the 2 other axis (from Y)
        //     0 + bendedPathRadius * Math.sin(angle)
        // );
        // // And never stop "looking back", to the Stage Scene center point
        // console.error(this.camera.position);
        // console.error(this.camera.rotation);
        // // this.camera.lookAt(centerPoint.x, bendedPathRadius, centerPoint.z);
        // this.camera.lookAt(0, 0, 0);
        // ---------------------------------------------------------

        if (xChgs) { this.performRotationOver('x'); }
        if (yChgs) { this.performRotationOver('y'); }
        if (zChgs) { this.performRotationOver('z'); }
    }

    /**
     * MIND YOU:
     * You ALWAYS have to define as being 'up' tha Axis over you want to do a Rotation on.
     *
     * With the new FE UI/UX, some axis is already set, by @Input('axisUp') set setCurrentAxisUp(){};
     * you now can only set/input rotation movements ON the 'this.currentAxisUp' axis
     * (that's why, for each axis, 'this.camera.up.set()' is now commented)
     *
     * Check it here: https://github.com/mrdoob/three.js/issues/1953#issuecomment-5820658
     */
    private performRotationOver(thisAxis: AxisType) {
        // console.error('Axis is:', thisAxis);
        const newAngle = this['rotate' + thisAxis.toUpperCase()];
        const newRadius = this.camera.position[thisAxis];

        switch (thisAxis) {
            case 'x':
                // this.camera.up.set(1, 0, 0);
                this.camera.position.set(
                    newRadius,
                    newRadius * Math.cos(newAngle),
                    newRadius * Math.sin(newAngle)
                );
                break;

            case 'y':
                // this.camera.up.set(0, 1, 0);
                this.camera.position.set(
                    newRadius * Math.cos(newAngle),
                    newRadius,
                    newRadius * Math.sin(newAngle)
                );
                break;

            case 'z':
                // this.camera.up.set(0, 0, 1);
                this.camera.position.set(
                    newRadius * Math.cos(newAngle),
                    newRadius * Math.sin(newAngle),
                    newRadius
                );
                break;

            default:
                break;
        }

        // "Always" focus on the center Stage - otherwise mouse/TRANSL moves will "change" what User is seeing at each change
        // It's up to the User to set it...
        if (this.currentLookAtCenter === true) {
            this.camera.lookAt(0, 0, 0);
        }
    }

    public abstract updateAspectRatio(aspect: number): void;
    public abstract updateFrustum(fov: number, near: number, far: number): void;
    public reApplyAllInputData() { this.applyAllInputData(); }
    /**
     * It's Public because different Components/Directives come here to get this value.
     *
     * We've place it here, on Camera Directive, since it's a light CAMERA, exclusively to be attached to the Camera,
     * and not anywhere around the scene.
     * Eventualy can obviously be User customized (controled on any DEMO 'settings' Tab <input />)
     * on params like color, distance, intensity, etc.
     *
     * We don't need it HERE anymore - attach the light coordinates to the camera ones, on the HTML Input() vars.
     * But we'll need it at Orbit Controls Directive - when user moves the Scene with the mouse...
     */
    public updateCameraLightPosition(px = this.positionX, py = this.positionY, pz = this.positionZ) {
        // const frontLightCamera = this.scene.cameraLight;
        // Upper is the DOM camera tag/object - does code still exists (maybe 'DOMcameraLight'...?); didn't make too much sense.
        // Next is the ThreeJS Object 'Camera' type (Directive):

        // Is it marked up on the same <canvas /> HTML...?
        // if (frontLightCamera) {
        //     frontLightCamera.position.set(px || 0, py || 0, pz || 0);
        // }
    }
}
