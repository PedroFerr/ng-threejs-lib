import { Component, forwardRef, Input } from '@angular/core';
import { CameraDirective } from './camera.directive';
import * as THREE from 'three';

import { RendererService } from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
import { LoggerService } from '../../services/app-demo/logger.service';
import { ConsoleLogService } from '../../services/app-demo/console-log.service';

@Component({
    selector: 'ng3d-perspective-camera',
    providers: [{ provide: CameraDirective, useExisting: forwardRef(() => PerspectiveCameraComponent) }],
    template: '<ng-content></ng-content>'
})
export class PerspectiveCameraComponent extends CameraDirective<THREE.PerspectiveCamera> {

    constructor(
        protected rendererService: RendererService,
        protected loggerService: LoggerService,
        protected consoleLogService: ConsoleLogService
    ) {
        super(rendererService, loggerService, consoleLogService);
    }

    public updateAspectRatio(aspect: number): void {
        // console.log('updateAspectRatio: ' + aspect, this.camera);
        this.camera.aspect = aspect;
        this.updateProjectionMatrix();
    }

    public updateFrustum(thisFov: number, thisNear: number, thisFar: number): void {
        if (this.camera) {
            this.camera.fov = thisFov;
            this.camera.near = thisNear;
            this.camera.far = thisFar;
        }

        this.updateProjectionMatrix();

        if (this.consoleLogService.isThisBeingConsoled('PerspectiveCameraComponent')) {
            console.warn('CHANGES on Camera Viewing Frustum:', thisFov, thisNear, thisFar, this.camera);
        }
    }

    /**
     * Updates the camera projection matrix.
     * NB: MUST be called AFTER any change of parameters (https://threejs.org/docs/#api/en/cameras/PerspectiveCamera.updateProjectionMatrix)
     */
    public updateProjectionMatrix(): void {
        if (this.camera) {
            this.camera.updateProjectionMatrix();
        }
    }

    protected createCamera(): void {
        // let aspectRatio = undefined; // Updated later
        this.camera = new THREE.PerspectiveCamera(
            this.fov,
            undefined,
            this.near,
            this.far
        );
    }
}
