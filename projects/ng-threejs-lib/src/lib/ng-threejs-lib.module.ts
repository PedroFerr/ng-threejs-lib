import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';    // <= for <ng3d-input-block />

// ---------------------------------------------------
import { NgThreejsLibComponent } from './ng-threejs-lib.component';
// ---------------------------------------------------

// Common:

// ThreeJS Modules that have "same function" components:
import { CameraModule } from './modules/camera/z-camera.module';
import { SceneModule } from './modules/scene/z-scene.module';
import { VisualizationControlModule } from './modules/visualization-control/z-visualization-control.module';
// or Components by themselfes:
import { RendererCanvasComponent } from './components/renderer/renderer-canvas.component';

// Angular Modules
import { PipesModule } from './pipes/z-pipes.module';

// UI/UX (2D) components, used at the DEMO App:
import { ViewportControlerMenuComponent } from './components/app-demo-used/viewport-controler-menu/viewport-controler-menu.component';
import { LedLightComponent } from './components/app-demo-used/led-light/led-light.component';
import { InputBlockComponent } from './components/app-demo-used/input-block/input-block.component';
import { MonitorDebugComponent } from './components/app-demo-used/monitor-debug/monitor-debug.component';
import { LoadingWheelComponent } from './components/app-demo-used/loading-wheel/loading-wheel.component';
import { SwitchButtonComponent } from './components/app-demo-used/switch/switch.component';

// UI/UX 3D Components:
import { SphereMeshComponent } from './components/mesh/sphere-mesh.component';
import { CubeMeshComponent } from './components/mesh/cube-mesh.component';
import { CylinderMeshComponent } from './components/mesh/cylinder-mesh.component';
import { TorusMeshComponent } from './components/mesh/torus-mesh-component';


@NgModule({
    declarations: [
        NgThreejsLibComponent,

        RendererCanvasComponent,

        // UI/UX (2D) components, used at the DEMO App:
        ViewportControlerMenuComponent,
        LedLightComponent,
        InputBlockComponent,
        MonitorDebugComponent,
        LoadingWheelComponent,
        SwitchButtonComponent,

        // UI/UX 3D Components:
        SphereMeshComponent,
        CubeMeshComponent,
        CylinderMeshComponent,
        TorusMeshComponent

        // -------------------

    ],

    imports: [
        CommonModule,
        FormsModule,

        CameraModule,
        SceneModule,
        VisualizationControlModule,
        PipesModule
    ],


    exports: [
        NgThreejsLibComponent,

        RendererCanvasComponent,

        // UI/UX (2D) components, used at the DEMO App:
        ViewportControlerMenuComponent,
        LedLightComponent,
        InputBlockComponent,
        MonitorDebugComponent,
        LoadingWheelComponent,
        SwitchButtonComponent,

        // UI/UX 3D Components:
        SphereMeshComponent,
        CubeMeshComponent,
        CylinderMeshComponent,
        TorusMeshComponent,

        // -------------------

        CameraModule,
        SceneModule,
        VisualizationControlModule,
        // PipesModule    // Already have it on App's DEMO
    ]
})
export class NgThreejsLibModule { }
