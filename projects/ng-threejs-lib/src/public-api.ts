/*
 * Public API Surface of ng-threejs-lib
 */

// Very weird I can not use './lib/...' relative path:
// ------------------------------------------------------------------------------------------------------
// ERROR in ./projects/ng-threejs-lib/src/lib/util/threejs/fix-center.ts.. and all the others!
// Module build failed | Error: C:\wamp\www\_...\fix-center.ts is missing from the TypeScript compilation.
// Please make sure it is in your tsconfig via the 'files' or 'include' property.
// ------------------------------------------------------------------------------------------------------
// Happens on 'ng serve' compile time, but the error is "done" on the 'ng build ng-threejs-lib' of the library...

/* *

export * from './lib/ng-threejs-lib.service';
export * from './lib/ng-threejs-lib.component';
export * from './lib/ng-threejs-lib.module';

// ThreeJS Modules that have "same function" components:
export * from './lib/modules/camera';
export * from './lib/modules/scene';
export * from './lib/modules/visualization-control';

// Angular App Modules
export * from './lib/pipes';

export * from './lib/services-threejs/animation.service';
export * from './lib/services-threejs/raycaster.service';
export * from './lib/services-threejs/renderer.service';
export * from './lib/services-threejs/stats.service';

export * from './lib/util';


// UI/UX 3D Components:
export * from './lib/components/renderer/renderer-canvas.component';
export * from './lib/components/mesh/sphere-mesh.component';

/* */
export * from 'projects/ng-threejs-lib/src/lib/ng-threejs-lib.service';
export * from 'projects/ng-threejs-lib/src/lib/ng-threejs-lib.component';
export * from 'projects/ng-threejs-lib/src/lib/ng-threejs-lib.module';
export * from 'projects/ng-threejs-lib/src/lib/ng-threejs-lib.interfaces';

// ThreeJS Modules that have "same function" components:
export * from 'projects/ng-threejs-lib/src/lib/modules/camera';
export * from 'projects/ng-threejs-lib/src/lib/modules/scene';
export * from 'projects/ng-threejs-lib/src/lib/modules/visualization-control';
// or Components by themselfes:
export * from 'projects/ng-threejs-lib/src/lib/components/renderer/renderer-canvas.component';


// Angular App Modules, Services and Utils
// 3D relative:
export * from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/animation.service';
export * from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/raycaster.service';
export * from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/renderer.service';
export * from 'projects/ng-threejs-lib/src/lib/services/lib-threejs/stats.service';
// General angular componentization purpose:
export * from 'projects/ng-threejs-lib/src/lib/services/app-demo/logger.service';
export * from 'projects/ng-threejs-lib/src/lib/services/app-demo/console-log.service';
export * from 'projects/ng-threejs-lib/src/lib/services/app-demo/cookie-handler.service';
// export * from 'projects/ng-threejs-lib/src/lib/pipes';        // Already have it on App's DEMO

// UI/UX (2D) components, used at the DEMO App:
export * from 'projects/ng-threejs-lib/src/lib/components/app-demo-used/viewport-controler-menu/viewport-controler-menu.component';
export * from 'projects/ng-threejs-lib/src/lib/components/app-demo-used/led-light/led-light.component';
export * from 'projects/ng-threejs-lib/src/lib/components/app-demo-used/input-block/input-block.component';
export * from 'projects/ng-threejs-lib/src/lib/components/app-demo-used/monitor-debug/monitor-debug.component';
export * from 'projects/ng-threejs-lib/src/lib/components/app-demo-used/loading-wheel/loading-wheel.component';
export * from 'projects/ng-threejs-lib/src/lib/components/app-demo-used/switch/switch.component';

// UI/UX 3D Components:
export * from 'projects/ng-threejs-lib/src/lib/components/mesh/sphere-mesh.component';
export * from 'projects/ng-threejs-lib/src/lib/components/mesh/cube-mesh.component';
export * from 'projects/ng-threejs-lib/src/lib/components/mesh/cylinder-mesh.component';
export * from 'projects/ng-threejs-lib/src/lib/components/mesh/torus-mesh-component';

/* */
